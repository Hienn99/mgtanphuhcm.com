<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'mgtanphu' );

/** Username của database */
define( 'DB_USER', 'root' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', '' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '(13]#}d:&nQx-A{~4<Lz/~bix:M%e$}KSBE-HSLu$ 2lM@fEQS`+tXuHbJ8}NMOM' );
define( 'SECURE_AUTH_KEY',  ')7*0:QO8VOc3e6hx~OH_4V;M+Go[pzM|u4HL?}A*QGE6*x Lg,1AT/A~6*d0M9!w' );
define( 'LOGGED_IN_KEY',    'q+*u+q /_Qb:%0/O|d3y# 6,3]~6CU*CK*%Hx6U!PLYPXP5i<|(dG^,XBaN#@w# ' );
define( 'NONCE_KEY',        '#f*]qNZ@z-DMFt_eiqr)-CBR%h}MFAl(y4^mLKPu^g5,%*q~w#u|x87)O$w0Q Ky' );
define( 'AUTH_SALT',        'd|KT?WhQk0F.{*Yw^09G]XgrPqn([@o6h#ZEx$w(!;lMF+*;25e~h&oY@*[[f^dp' );
define( 'SECURE_AUTH_SALT', 'We+42]&Hr;BCA`C_HH3;`Q7RiSQG,.f6hwN[YbS&_345WD]/{l]Rr(QouWiP1$9C' );
define( 'LOGGED_IN_SALT',   'CBLWIOfS$6?pWiM_JSZ(_Ag]U6eKrlp#vF~UzyU/sgcCY8{}5I)d&=p<aaur!#DX' );
define( 'NONCE_SALT',       '0O_j(  +@|c}N<UFbZZd)tx}e7TgUgyxVR4%R^oTvpF&4o0GQ|vK:x.&1ur$;jjv' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
