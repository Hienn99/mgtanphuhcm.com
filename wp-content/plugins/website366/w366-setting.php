<?php
/*
Plugin Name: Website366
Plugin URI: http://website366.com/
Description: Config Website366
Author: LeAV
Author URI: http://website366.com/
Text Domain: website366
Domain Path: /languages/
Version: 1.0
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if( !class_exists('Website366') ):
	define( 'WEBSITE366_PLUGIN', __FILE__ );
	define( 'WEBSITE366_PLUGIN_DIR', untrailingslashit( dirname( WEBSITE366_PLUGIN ) ) );
	require_once(WEBSITE366_PLUGIN_DIR .'/inc/define.php');
	
	class Website366{
		
		/**
		 * Constructor
		 */
		function __construct() {
			add_action( 'admin_head', array( $this, 'register_css_js' ) );
			
			//Load module
			$this->change_admin_bar_logo();
			$this->custom_dashboard();
			$this->clean_menu();
			$this->hide_alert_box();
			$this->redirect_after_login();
			$this->admin_thankyou();
			$this->admin_bar_remove_logo();
			$this->admin_remove_wp_title();
			$this->admin_remove_help_tabs();
		}
		
		/**
		 * Register Admin Css & Js (Use for all mudules)
		 */
		function register_css_js() {
			//Css
			wp_register_style( 'website366_admin_style', WEBSITE366_CSS_URL . '/admin-style.css', false, '1.0.0' );
			wp_enqueue_style( 'website366_admin_style' );
			
			//Js
			wp_enqueue_script( 'website366_admin_js', WEBSITE366_CSS_JS . '/admin-js.js' , array(), '1.0', true );
		}
		
		//Module: Change Admin Bar Logo
			public function render_css_change_admin_bar_logo(){
			?>
				<!-- change_admin_bar_logo -->
				<style>
					#wpadminbar #wp-admin-bar-wp-logo>.ab-item .ab-icon:before{
						content:url(<?php echo W366_IMGAGES_URL;?>/logo.png);
					}
					
					.wp-list-table tr[data-slug="website366"] .deactivate{ display:none; }
				</style>
			<?php
			}
			public function change_admin_bar_logo(){
				add_action( 'admin_head', array( $this, 'render_css_change_admin_bar_logo' ) );
				add_action('login_head', array($this, 'w366_custom_login_logo'));
			}
			
			public function w366_custom_login_logo(){
			?>
            <style type="text/css">                                                                                   
				body.login div#login h1 a {
					background-image: url("<?php echo W366_IMGAGES_URL;?>/logo-admin-website366.png");
					padding: 0;
					background-size: cover;
					background-position: center center;
					background-repeat: no-repeat;
					background-color: #fff;
					width: 100%;
					height: 60px;
				}
			</style>
            <?php
			}
		
		//Module: Custom Dashboard
			public function render_css_custom_dashboard(){
			?>
            	<!-- custom_dashboard -->
            	<style>
                	#w366_intro_website366{
						background: #179bd7;
					}
					#w366_intro_website366, #w366_intro_website366 .hndle, #w366_intro_website366 a{
						color:#fff;
					}
					ul.list-actions{ 
						text-align: center;
						border-top: 1px solid;
						padding-top: 6px;
						margin-bottom:0;
					}
					ul.list-actions li{
						display: inline-block;
						position: relative;
						padding: 0 10px;
					}
					ul.list-actions li:not(:last-child):after{ 
						position: absolute;
						content: '';
						width: 1px;
						height: 12px;
						background: #fff;
						top: 50%;
						transform: translateY(-50%);
						right: 0;
					}
                </style>
            <?php 
			}
			
			public function w366_intro_website366_function( $post, $callback_args ) {
				?>
				<ul class="w366-list">
                	<li><strong><u>Website được thiết kế bởi:</u></strong> </li>
					<li>Công ty thiết kế web <a href="http://website366.com" target="_blank">Website366</a></li>
					<li>Điện thoại: <a href="tel:0792.922.321">0792.922.321</a> – <a href="tel:0979217783">0979.217783</a></li>
					<li>Email: <a href="mailto:website366@gmail.com">website366@gmail.com</a> – <a href="mailto:website366@gmail.com">webmau366@gmail.com</a></li>
					<li>Địa chỉ: 28 Đường số 5 Phước Kiển, Nhà Bè TpHCM</li>
					<li>MXH: <a target="_blank" title="Liên kết Facebook" href="https://www.facebook.com/website366com"><span class="dashicons dashicons-facebook-alt"></span></a> <a target="_blank" title="Liên kết Google Plus" href="https://plus.google.com/communities/100805533198135254144"><span class="dashicons dashicons-googleplus"></span></a></li>
                    <li>Website: <a target="_blank" href="http://website366.com">Website366.com</a></li>
				</ul>
                
                <ul class="list-actions">
                	<li><a target="_blank" href="http://website366.com/mau-web/" title="Xem mẫu web có sẳn">Kho giao diện</a></li>
                    <li><a target="_blank" href="http://website366.com/mau-website-noi-bat/" title="Xem giao diện nổi bật">Giao diện nổi bật</a></li>
                    <li><a target="_blank" href="http://website366.com/mau-website-dang-giam-gia/">Giao diện giảm giá</a></li>
                    <li><a target="_blank" href="http://website366.com/ban-theme-wordpress/">Giao diện free</a></li>
                </ul>
				<?php
			}
	
			public function w366_add_dashboard_widgets(){
				$this->w366_remove_all_dashboard();

				//Add Dashboard------------------------------------------------------------------------------
				$widget_id = 'w366_intro_website366';
				$widget_name = 'Website366.com - Thiết kế website giá rẻ - chuẩn seo';
				$callback = array($this, 'w366_intro_website366_function');
				$control_callback = '';
				wp_add_dashboard_widget( $widget_id, $widget_name, $callback, $control_callback, $callback_args );
				
				// Your new add dashboard here..............
				

			}
			
			public function w366_remove_all_dashboard(){
				global $wp_meta_boxes;
				
				//Remove Gutenberg panel
				remove_action( 'try_gutenberg_panel', 'wp_try_gutenberg_panel' );
				
				//Remove Dashboard Left
				$arr_left_dashboard = $wp_meta_boxes['dashboard']['normal'];
				if($arr_left_dashboard){
					foreach($arr_left_dashboard as $key => $arr_core_or_high){
						if($arr_core_or_high){
							foreach($arr_core_or_high as $item){
								$dashboard_id = $item['id'];
								remove_meta_box( $dashboard_id, 'dashboard', 'normal' );
							}
						}
					}
				}
				
				//Remove Dashboard Right
				$arr_right_dashboard = $wp_meta_boxes['dashboard']['side']['core'];
				if($arr_right_dashboard){
					foreach($arr_right_dashboard as $item){
						$dashboard_id = $item['id'];
						remove_meta_box( $dashboard_id, 'dashboard', 'side' );
					}
				}
			}
			
			public function custom_dashboard(){
				add_action( 'admin_head', array( $this, 'render_css_custom_dashboard' ) );
				add_action( 'wp_dashboard_setup', array($this, 'w366_add_dashboard_widgets'), 100 );
				remove_action( 'try_gutenberg_panel', 'wp_try_gutenberg_panel' );
			}
			
		//Module: Clean Menu
			public function clean_menu(){
				add_action( 'admin_init', array($this, 'w366_remove_admin_menu'), 100 );
			}
			public function w366_remove_admin_menu(){
				remove_menu_page( 'edit-comments.php' );
				//remove_menu_page( 'duplicator' );
				remove_menu_page( 'wphpuw' ); 
				remove_menu_page( 'wpseo_dashboard' ); 
				remove_menu_page( 'edit.php?post_type=acf' );
				remove_submenu_page('themes.php', 'theme-editor.php' ); 
			}
		
		//Module: Hide all notice alert message box
			public function hide_alert_box(){
				add_action( 'admin_head', array( $this, 'render_css_hide_alert_box' ) );
			}
			
			public function render_css_hide_alert_box(){
			?>
				<!-- render_css_hide_alert_box -->
				<style>
					#welcome-panel,
					#message,
					.notice,
					div[class$="notice"]{ display:none !important;}
				</style>
			<?php
			}
		
		//Module: redirect list posts after login
			public function redirect_after_login(){
				add_filter( 'login_redirect', array( $this, 'w366_login_redirect' ), 10, 3 );
			}
			public function w366_login_redirect( $redirect_to, $request, $user ){
				return admin_url( 'edit.php' );
			}
		
		//Module: Admin edit thank you footer
			public function admin_thankyou(){
				add_filter('admin_footer_text', array($this, 'edit_footer_admin') );
			}
			public function edit_footer_admin(){
				echo '<span id="footer-thankyou">Developed by <a href="http://website366.com" target="_blank">website366.com</a></span>';
			}
		
		//Module:  Admin remove wordpress logo admin bar
			public function admin_bar_remove_logo(){
				add_action( 'wp_before_admin_bar_render', array($this, 'bar_remove_logo') , 0 );
			}
			public function bar_remove_logo(){
				global $wp_admin_bar;
    			$wp_admin_bar->remove_menu( 'wp-logo' );
			}
			
		//Module:  Admin 'wordpress' admin title
			public function admin_remove_wp_title(){
				add_filter('admin_title', array($this, 'remove_wp_title'), 10, 2);
			}
			public function remove_wp_title($t){
				return str_replace("&#8212; WordPress"," ",$t);
			}
		
		//Module: Admin Help Tabs
			public function admin_remove_help_tabs(){
				add_action('admin_head',  array($this, 'remove_help_tabs') );
			}
			public function remove_help_tabs(){
				 $screen = get_current_screen();
   				 $screen->remove_help_tabs();
			}
		
	//End class--------------	
	}
	$Website366 = new Website366();
endif;
