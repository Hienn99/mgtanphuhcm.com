<?php
/* Template Name: Phụ kiện */
get_header(); ?>


<?php 
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$per_page = 12;
$args_filter = array(
					'post_type' => 'w366_acc',
					'posts_per_page' => $per_page,
					'paged' => $paged,
					'orderby'   => 'date',
					'order' => 'desc'
				);
$the_query = new WP_query($args_filter);
?>

<div class="wrap">
	<?php
		get_template_part( 'template-parts/header/header', 'bottom' );
		
		while ( have_posts() ) : the_post();
		?>
       <div class="container">
            <div class="wrapper_new">
                <div class="box_mid">
                    <div class="mid-title ">
                        <div class="titleL">
                            <h1><?php the_title(); ?></h1>
                        </div>
                        <div class="titleR"></div>
                    </div>
                    <div class="mid-content">
                        <div class="the-content">
                       		<?php 
								get_template_part( 'template-parts/accessory/tax', 'list' );
							?>
                            
                            <div class="vnt-phukien">
                            	<div class="row">
									<?php 
                                    while ( $the_query->have_posts() ) { $the_query->the_post();
                                    ?>
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <?php 
                                                get_template_part( 'template-parts/accessory/content', 'loop' );
                                            ?>
                                        </div>
                                     <?php
                                    }
                                    wp_reset_postdata();
                                    ?>
                            	</div>
                                <div class="w366-pagination row">
                                    <div class="col-md-12">
                                        <?php 
                                        $GLOBALS['wp_query']->max_num_pages = $the_query->max_num_pages;
                                        the_posts_pagination( array(
                                                    'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
                                                    'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
                                                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
                                                ) );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>          
                </div>
            </div>
        </div>
        <?php
		endwhile; // End of the loop.
	?>
</div><!-- .wrap -->

<?php get_footer();
