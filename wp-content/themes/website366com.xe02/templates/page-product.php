<?php
/* Template Name: Sản phẩm */
get_header(); ?>

<div class="wrap">
	<?php
		get_template_part( 'template-parts/header/header', 'bottom' );
		
		while ( have_posts() ) : the_post();
		?>
       <div class="container">
            <div class="wrapper_new">
                <div class="box_mid">
                    <div class="mid-title ">
                        <div class="titleL">
                            <h1><?php the_title(); ?></h1>
                        </div>
                        <div class="titleR"></div>
                    </div>
                    <div class="mid-content">
                        <div class="the-content">
                        	 <div class="row">
                             	<?php 
									$arr_car_term = w366_all_car_tax();
									if ( $arr_car_term ) {
										foreach($arr_car_term as $obj_car_term){
											set_query_var( 'obj_car_term', $obj_car_term );
										?>
											<div class="col-md-4">
												<?php 
													get_template_part( 'template-parts/product/tax', 'loop' );
												?>
											</div>
										 <?php
										}
									}
									?>
                             </div>
                        </div>
                    </div>          
                </div>
            </div>
        </div>
        <?php
		endwhile; // End of the loop.
	?>
</div><!-- .wrap -->

<?php get_footer();
