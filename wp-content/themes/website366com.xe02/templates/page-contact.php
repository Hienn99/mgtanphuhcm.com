<?php
/* Template Name: Liên hệ */
get_header(); ?>

<div class="wrap">
	<?php
		get_template_part( 'template-parts/header/header', 'bottom' );
		
		while ( have_posts() ) : the_post();
			$post_id = get_the_ID(); 
			$w366_contact_title = get_post_meta($post_id, 'w366_contact_title', true);
			$w366_contact_address = get_post_meta($post_id, 'w366_contact_address', true);
			$w366_contact_phone_1 = get_post_meta($post_id, 'w366_contact_phone_1', true);
			$w366_contact_phone_2 = get_post_meta($post_id, 'w366_contact_phone_2', true);
			$w366_contact_email = get_post_meta($post_id, 'w366_contact_email', true);
			$cf7_id = get_post_meta(get_the_ID(), 'w366_contact_cf7_id', true);
		?>
        	<div class="container">
                <div class="wrapper_new">
                    <div class="box_mid">
                        <div class="mid-title ">
                            <div class="titleL">
                                <h1><?php the_title(); ?></h1>
                            </div>
                            <div class="titleR"></div>
                        </div>
                        <div class="mid-content">
                            <div class="the-content-desc">
                            	<div class="info-contact">
                                    <div class="over">
                                        <div class="name"><?php if($w366_contact_title) echo $w366_contact_title; else bloginfo('name'); ?></div>
                                        <ul>
                                            <li class="fa-home"><?=$w366_contact_address?></li>
                                            <li class="fa-phone">CSKH: <?=$w366_contact_phone_1?></li>
                                            <li class="fa-phone"><?=$w366_contact_phone_2?></li>
                                            <li class="fa-envelope-o"><a href="mailto:<?=$w366_contact_email?>"><?=$w366_contact_email?></a></li>
                                            <li class="fa-globe"><a href="<?php echo get_site_url();?>"><?php echo get_site_url();?></a></li>
                                        </ul>
                                        <div class="view-map-contact"><a href="#maps1" onclick="load_maps(1,450,450);"><span>Xem bản đồ</span></a></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
								<div class="form-contact v2">
									<?php if($cf7_id) echo do_shortcode('[contact-form-7 id="'.$cf7_id.'" title="Form liên hệ 1"]'); ?>
								</div>
                                <div class="map-contact"><?php the_content(); ?></div>
                            </div>
                        </div>          
                    </div>
                </div>
            </div>
        <?php
		endwhile; // End of the loop.
	?>
</div><!-- .wrap -->

<?php get_footer();