<?php
get_header(); ?>

<div class="wrap">
	<?php
		get_template_part( 'template-parts/header/header', 'bottom' );
	?>
	<div class="container">
        <div class="wrapper_new">
            <div class="box_mid">
                <div class="mid-title ">
                    <div class="titleL">
                        <h1><?php single_term_title(); ?></h1>
                    </div>
                    <div class="titleR"></div>
                </div>
                <div class="mid-content">
                    <div class="the-content">
                         <div class="row">
                            <?php 
                                while ( have_posts() ) : the_post();
                                ?>
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <?php 
                                            get_template_part( 'template-parts/service/content', 'loop' );
                                        ?>
                                    </div>
                                 <?php
                                endwhile;
                                wp_reset_postdata();
                                ?>
                         </div>
                         <div class="w366-pagination row">
                            <div class="col-md-12">
                                <?php 
                                the_posts_pagination( array(
                                        'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'website366com' ) . '</span>',
                                        'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'website366com' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
                                        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'website366com' ) . ' </span>',
                                    ) );

                                ?>
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
        </div>
    </div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();
