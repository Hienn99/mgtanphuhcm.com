<?php
/*
Create: new Meta Box
	Step 1: Show Metabox for Custom Posttype
	Step 2: Save data for post
*/
class W366_Ajax_Metabox {
	function __construct() {
		add_action( 'wp_ajax_w366_ajax_load_demo', array( $this, 'w366_ajax_load_demo' ) );
	}
	
	//Ajax Process
	public function w366_ajax_load_demo(){
		$html = ''; 
		
		//code here
		
		echo $html;
		exit;
	}
}

new W366_Ajax_Metabox();

//Step 1:
function w366_custom_metabox_post_type() {
	//---------------------------------------------------------------
	$id = 'meta_box_w366_car';
	$title = __( 'Thông tin xe', 'website366com' );
	$func_callback = 'w366_callback_display_content_w366_car';
	$arr_post_type = array('w366_car');
	$context = 'side'; // advanced ; side ; normal
	$priority = 'low'; // low ; high; default
	//$callback_args =  array( 'foo' => $var1, 'bar' => $var2 );
	
	//Metabox;
	add_meta_box( $id, $title, $func_callback, $arr_post_type, $context, $priority, $callback_args);
	
	//---------------------------------------------------------------
	$id = 'meta_box_w366_acc';
	$title = __( 'Thông tin phụ kiện', 'website366com' );
	$func_callback = 'w366_callback_display_content_w366_acc';
	$arr_post_type = array('w366_acc');
	$context = 'normal'; // advanced ; side ; normal
	$priority = 'high'; // low ; high; default
	//$callback_args =  array( 'foo' => $var1, 'bar' => $var2 );
	
	//Metabox;
	add_meta_box( $id, $title, $func_callback, $arr_post_type, $context, $priority, $callback_args);
	
	
	//Check is contact template add metabox
	$post_id = get_the_ID();
	$_wp_page_template = get_post_meta($post_id, '_wp_page_template', true);
	if (strpos($_wp_page_template, 'page-contact') !== false) {
		$id = 'meta_box_page-contact';
		$title = __( 'Thông tin liên hệ', 'website366com' );
		$func_callback = 'w366_callback_display_content_contact_page';
		$arr_post_type = array('page');
		$context = 'normal'; // advanced ; side ; normal
		$priority = 'high'; // low ; high; default
		//$callback_args =  array( 'foo' => $var1, 'bar' => $var2 );
		
		//Metabox;
		add_meta_box( $id, $title, $func_callback, $arr_post_type, $context, $priority, $callback_args);
	}
	
}
add_action( 'add_meta_boxes', 'w366_custom_metabox_post_type' );

function w366_callback_display_content_w366_car($post){
	$post_id = $post->ID;
	
	$w366_car_price = get_post_meta($post_id, 'w366_car_price', true);
?>
	<div class="meta-box">
        <div class="row">
        	<!-- item -->
            <div class="item">
                <h3>Giá</h3>
                <div class="w366-field">
                    <input class="widefat" name="w366_car_price" value="<?php echo $w366_car_price; ?>" placeholder="150000000" />
                </div>
            </div>
            
      	</div>
    </div> 
<?php
}

function w366_callback_display_content_w366_acc($post){
	$post_id = $post->ID;
	
	$w366_acc_price = get_post_meta($post_id, 'w366_acc_price', true);
?>
	<div class="meta-box">
        <div class="row">
        	<!-- item -->
            <div class="item">
                <h3>Giá</h3>
                <div class="w366-field">
                    <input class="widefat" name="w366_acc_price" value="<?php echo $w366_acc_price; ?>" placeholder="5000000" />
                </div>
            </div>
            
      	</div>
    </div> 
<?php
}

function w366_callback_display_content_contact_page($post){
	$post_id = $post->ID;
	
	$w366_contact_title = get_post_meta($post_id, 'w366_contact_title', true);
	$w366_contact_address = get_post_meta($post_id, 'w366_contact_address', true);
	$w366_contact_phone_1 = get_post_meta($post_id, 'w366_contact_phone_1', true);
	$w366_contact_phone_2 = get_post_meta($post_id, 'w366_contact_phone_2', true);
	$w366_contact_email = get_post_meta($post_id, 'w366_contact_email', true);
?>
	<div class="meta-box">
        <div class="row">
        	<!-- item -->
            <div class="item">
                <h3>Tiêu đề</h3>
                <div class="w366-field">
                    <input class="widefat" name="w366_contact_title" value="<?php echo $w366_contact_title; ?>" placeholder="" />
                </div>
            </div>
            
        	<!-- item -->
            <div class="item">
                <h3>Địa chỉ</h3>
                <div class="w366-field">
                    <input class="widefat" name="w366_contact_address" value="<?php echo $w366_contact_address; ?>" placeholder="" />
                </div>
            </div>
            
            <!-- item -->
            <div class="item">
                <h3>Điện thoại 1</h3>
                <div class="w366-field">
                    <input class="widefat" name="w366_contact_phone_1" value="<?php echo $w366_contact_phone_1; ?>" placeholder="" />
                </div>
            </div>
            
            <!-- item -->
            <div class="item">
                <h3>Điện thoại 2</h3>
                <div class="w366-field">
                    <input class="widefat" name="w366_contact_phone_2" value="<?php echo $w366_contact_phone_2; ?>" placeholder="" />
                </div>
            </div>
            
            <!-- item -->
            <div class="item">
                <h3>Email</h3>
                <div class="w366-field">
                    <input class="widefat" name="w366_contact_email" value="<?php echo $w366_contact_email; ?>" placeholder="" />
                </div>
            </div>
            
            <!-- item -->
            <?php 
			$w366_contact_cf7_id = get_post_meta($post_id, 'w366_contact_cf7_id', true);
			$args = array( 'post_type' => 'wpcf7_contact_form', 'order' => 'DESC','posts_per_page' => '-1');
			$posts_array = get_posts( $args );
			?>
            <div class="item">
                <h3>Form liên hệ</h3>
                <div class="w366-field">
                    <select name="w366_contact_cf7_id">
                    	<option>--Chọn Form--</option>
                        <?php 
						foreach($posts_array as $p){
						?>
                        	<option <?php if($w366_contact_cf7_id==$p->ID) echo 'selected'; ?> value="<?=$p->ID?>"><?=$p->post_title?></option>
                        <?php
						}
						?>
                    </select>
                </div>
            </div>
            
      	</div>
    </div> 
<?php
}
// End step 1

// Step 2:
function w366_save_w366_car( $post_id ) {
	if($_POST){
		update_post_meta( $post_id, 'w366_car_price', $_POST['w366_car_price'] );
	}
}
add_action( 'save_post_w366_car', 'w366_save_w366_car' );

function w366_save_w366_acc( $post_id ) {
	if($_POST){
		update_post_meta( $post_id, 'w366_acc_price', $_POST['w366_acc_price'] );
	}
}
add_action( 'save_post_w366_acc', 'w366_save_w366_acc' );

function w366_save_page( $post_id ) {
	if($_POST){ 
		update_post_meta( $post_id, 'w366_contact_title', $_POST['w366_contact_title'] );
		update_post_meta( $post_id, 'w366_contact_address', $_POST['w366_contact_address'] );
		update_post_meta( $post_id, 'w366_contact_phone_1', $_POST['w366_contact_phone_1'] );
		update_post_meta( $post_id, 'w366_contact_phone_2', $_POST['w366_contact_phone_2'] );
		update_post_meta( $post_id, 'w366_contact_email', $_POST['w366_contact_email'] );
		update_post_meta( $post_id, 'w366_contact_cf7_id', $_POST['w366_contact_cf7_id'] );
	}
}
add_action( 'save_post_page', 'w366_save_page' );
// End Step 2