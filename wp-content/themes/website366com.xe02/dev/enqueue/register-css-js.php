<?php
define('IS_BOOTSTRAP', true);
define('IS_BOOTSTRAP_4', false);
define('IS_DATEPICKER', false);
define('IS_AWESOME_FONT', true);

//Register CSS & JS
if(!function_exists('w366_register_scripts')):
	function w366_register_scripts() {
		//CSS CONTROL--------------------------------------------------------------------------------------------
			$a_css = array(
							//Site 1--------------hondaotophattien.com.vn
							'0cp-jquery.alerts.css',
							'2cp-global.css',
							'3cp-box.css',
							'1cp-style.css',
							//'4cp-menumobile.css',
							//'5cp-lazyloading.css',
							'6cp-main.css',
							//'7cp-mnpopup.css',
							'animate.css',
							//'8cp-MODALit.css',
							//'9cp-accessories.css',
							'10cp-custom.css',
							'21-news.css',
							'22-contact.css',
							
							//Site 2--------------hondasaigonquan7.vn
							'11cp-style.css',
							
							'custom.css',
							'responsive.css',
							//'add-new-file.css'
						);
						
			//Css bootstrap
			if(IS_BOOTSTRAP):
				if(IS_BOOTSTRAP_4){
					wp_enqueue_style( W366_PREFIX.'bootstrap.min.css_4', get_theme_file_uri( '/dev/enqueue/js/bootstrap-4.0.0/dist/css/bootstrap.min.css' ), array(), '4.0.0' );
					wp_enqueue_script( W366_PREFIX.'bootstrap.min.js_4', get_theme_file_uri( '/dev/enqueue/js/bootstrap-4.0.0/dist/js/bootstrap.min.js' ), array( 'jquery' ), '4.0.0', true );
					
				}else{
					wp_enqueue_style( W366_PREFIX.'bootstrap.3.3.7.min.css', get_theme_file_uri( '/dev/enqueue/js/bootstrap-3.3.7/css/bootstrap.min.css' ), array(), '3.3.7' );
					wp_enqueue_style( W366_PREFIX.'bootstrap.3.3.7.theme.min.css', get_theme_file_uri( '/dev/enqueue/js/bootstrap-3.3.7/css/bootstrap-theme.min.css' ), array(), '3.3.7' );
					wp_enqueue_script( W366_PREFIX.'bootstrap-3.3.7.min.js', get_theme_file_uri( '/dev/enqueue/js/bootstrap-3.3.7/js/bootstrap.min.js' ), array( 'jquery' ), '3.3.7', true );
				}
				
				//----
				if(IS_DATEPICKER){
					wp_enqueue_style( W366_PREFIX.'bootstrap.datepicker.css', get_theme_file_uri( '/dev/enqueue/js/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css' ), array(), '3.3.7' );
					wp_enqueue_script( W366_PREFIX.'moment.js', get_theme_file_uri( '/dev/enqueue/js/bootstrap-datetimepicker/build/js/moment.js' ), array( 'jquery' ), '3.3.7', true );
					wp_enqueue_script( W366_PREFIX.'datetimepicker.js', get_theme_file_uri( '/dev/enqueue/js/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js' ), array( 'jquery' ), '3.3.7', true );
				}
			endif;
			
			//Css font-awesome-4.7.0
			if(IS_AWESOME_FONT):
				wp_enqueue_style( W366_PREFIX.'font-awesome.min.css', get_theme_file_uri( '/dev/enqueue/css/font-awesome-4.7.0/css/font-awesome.min.css' ), array( 'twentyseventeen-style' ), '1.0' );
			endif;
			
			//malihu-custom-scrollbar
			wp_enqueue_style( W366_PREFIX.'jquery.mCustomScrollbar.css', get_theme_file_uri( '/dev/enqueue/js/malihu-custom-scrollbar/jquery.mCustomScrollbar.css' ), array( 'twentyseventeen-style' ), '1.0' );
			wp_enqueue_script( W366_PREFIX.'jquery.mCustomScrollbar.concat.min.js', get_theme_file_uri( '/dev/enqueue/js/malihu-custom-scrollbar/jquery.mCustomScrollbar.concat.min.js' ), array( 'jquery' ), '3.3.7', true );
			
			//fancybox-2.1.7
			wp_enqueue_style( W366_PREFIX.'jquery.fancybox.css', get_theme_file_uri( '/dev/enqueue/js/fancybox-2.1.7/source/jquery.fancybox.css' ), array( 'twentyseventeen-style' ), '2.1.7' );
			wp_enqueue_style( W366_PREFIX.'jquery.fancybox-buttons.css', get_theme_file_uri( '/dev/enqueue/js/fancybox-2.1.7/source/helpers/jquery.fancybox-buttons.css' ), array( 'twentyseventeen-style' ), '2.1.7' );
			wp_enqueue_style( W366_PREFIX.'jquery.fancybox-thumbs.css', get_theme_file_uri( '/dev/enqueue/js/fancybox-2.1.7/source/helpers/jquery.fancybox-thumbs.css' ), array( 'twentyseventeen-style' ), '2.1.7' );
			wp_enqueue_script( W366_PREFIX.'jquery.mousewheel.pack.js', get_theme_file_uri( '/dev/enqueue/js/fancybox-2.1.7/lib/jquery.mousewheel.pack.js' ), array( 'jquery' ), '2.1.7', true );
			wp_enqueue_script( W366_PREFIX.'jquery.fancybox.pack.js', get_theme_file_uri( '/dev/enqueue/js/fancybox-2.1.7/source/jquery.fancybox.pack.js' ), array( 'jquery' ), '2.1.7', true );
			wp_enqueue_script( W366_PREFIX.'jquery.fancybox-buttons.js', get_theme_file_uri( '/dev/enqueue/js/fancybox-2.1.7/source/helpers/jquery.fancybox-buttons.js' ), array( 'jquery' ), '2.1.7', true );
			wp_enqueue_script( W366_PREFIX.'jquery.fancybox-media.js', get_theme_file_uri( '/dev/enqueue/js/fancybox-2.1.7/source/helpers/jquery.fancybox-media.js' ), array( 'jquery' ), '2.1.7', true );
			wp_enqueue_script( W366_PREFIX.'jquery.fancybox-thumbs.js', get_theme_file_uri( '/dev/enqueue/js/fancybox-2.1.7/source/helpers/jquery.fancybox-thumbs.js' ), array( 'jquery' ), '2.1.7', true );
			
			//Css themes	
			if($a_css){
				foreach( $a_css as $css_file){
					$file_css_dir = get_parent_theme_file_path( '/dev/enqueue/css/'.$css_file ); 
					if( file_exists($file_css_dir) ){
						wp_enqueue_style( W366_PREFIX.$css_file, get_theme_file_uri( '/dev/enqueue/css/'.$css_file ), array( 'twentyseventeen-style' ), '1.0' );
					}
				}
			}
		//CSS CONTROL END--------------------------------------------------------------------------------------------
		
		//slick-1.8.1
		wp_enqueue_style( W366_PREFIX.'slick.css', get_theme_file_uri( '/dev/enqueue/js/slick-1.8.1/slick/slick.css' ), array( 'twentyseventeen-style' ), '1.8.1' );
		wp_enqueue_style( W366_PREFIX.'slick-theme.css', get_theme_file_uri( '/dev/enqueue/js/slick-1.8.1/slick/slick-theme.css' ), array( 'twentyseventeen-style' ), '1.8.1' );
		wp_enqueue_script( W366_PREFIX.'slick.min.js', get_theme_file_uri( '/dev/enqueue/js/slick-1.8.1/slick/slick.min.js' ) , array(), '1.8.1', true );
		
		//CSS JS ----------------------------------------------------------------------------------------------------
			$a_js = array(
							'custom.js',
							'ajax.js',
							//'add-new-file.js'
						);
						
			
			if($a_js){
				foreach( $a_js as $js_file){
					$file_js_dir = get_parent_theme_file_path( '/dev/enqueue/js/'.$js_file ); 
					if( file_exists($file_js_dir) ){
						wp_enqueue_script( W366_PREFIX.$js_file, get_theme_file_uri( '/dev/enqueue/js/'.$js_file ) , array(), '1.0', true );
					}
				}
			}
		//CSS JS END---------------------------------------------------------------------------------------------------
		
	}// end w366_register_scripts
	add_action( 'wp_enqueue_scripts', 'w366_register_scripts' );
endif;


