<?php
/*Define*/
$devDir = dirname(__FILE__);
define('DEV_DIR',$devDir);
define('REGISTER_POSTTYPE_DIR', DEV_DIR . '/posttype' );
define('REGISTER_SCRIPT_DIR', DEV_DIR . '/enqueue' );
define('REGISTER_WIDGET_DIR', DEV_DIR . '/widgets' );
define('REGISTER_SHORTCODE_DIR', DEV_DIR . '/shortcode' );
define('REGISTER_NAV_WALKER_DIR', DEV_DIR . '/walker' );
define('REGISTER_CLASSES_DIR', DEV_DIR . '/classes' );
define('REGISTER_METABOX', DEV_DIR . '/metabox' );
define('REGISTER_THEME_OPTIONS', DEV_DIR . '/theme-options' );
define('REGISTER_THEME_W366_OPTIONS', DEV_DIR . '/w366-options' );
define('REGISTER_W366_PAYPAL', DEV_DIR . '/paypal' );
define('REGISTER_W366_CUSTOM_SIDEBAR_AREA', DEV_DIR . '/sidebar-area' );

/*@Include DEV*/
//THEME OPTIONS redux-framework---------------------
/*$classes_file = REGISTER_THEME_OPTIONS . '/index.php';
if( file_exists($classes_file) ){
	require_once ( $classes_file );
}*/

//CLASS---------------------
/*$classes_file = REGISTER_CLASSES_DIR . '/index.php';
if( file_exists($classes_file) ){
	require_once ( $classes_file );
}*/

//THEME W366 OPTIONS---------------------
$classes_file = REGISTER_THEME_W366_OPTIONS . '/index.php';
if( file_exists($classes_file) ){
	require_once ( $classes_file );
}

//Functions
$functions_file = DEV_DIR . '/functions.php';
if( file_exists($functions_file) ){
	require_once ( $functions_file );
}

//Ajax
/*$ajax_file = DEV_DIR . '/ajax.php';
if( file_exists($ajax_file) ){
	require_once ( $ajax_file );
}*/

//Nav Walker
/*$walker_file = REGISTER_NAV_WALKER_DIR . '/index.php';
if( file_exists($walker_file) ){
	require_once ( $walker_file );
}*/

//CUSTOM POST TYPE
$posttype_file = REGISTER_POSTTYPE_DIR . '/posttype-custom.php';
if( file_exists($posttype_file) ){
	require_once ( $posttype_file );
}

//CSS & JS
$css_js_file = REGISTER_SCRIPT_DIR . '/register-css-js.php';
if( file_exists($css_js_file) ){
	require_once ( $css_js_file );
}

//WIDGET
$widget_file = REGISTER_WIDGET_DIR . '/index.php';
if( file_exists($widget_file) ){
	require_once ( $widget_file );
}


//SHORTCODE
/*$shortcode_file = REGISTER_SHORTCODE_DIR . '/index.php';
if( file_exists($shortcode_file) ){
	require_once ( $shortcode_file );
}*/

//Metabox
$metabox_file = REGISTER_METABOX . '/index.php';
if( file_exists($metabox_file) ){
	require_once ( $metabox_file );
}

//Paypal
/*$paypal_file = REGISTER_W366_PAYPAL . '/index.php';
if( file_exists($paypal_file) ){
	require_once ( $paypal_file );
}*/

//Custom sidebar area
$sb_area_file = REGISTER_W366_CUSTOM_SIDEBAR_AREA . '/index.php';
if( file_exists($sb_area_file) ){
	require_once ( $sb_area_file );
}


