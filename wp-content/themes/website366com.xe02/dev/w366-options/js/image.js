jQuery(document).ready(function($) {
	imageMultipleInit();
	removeItem();
});

function removeItem(){
	jQuery(".removeit").click(function(){
		var divIt = jQuery(this).parents('.attachment');
		var eContainer = jQuery(divIt).parent(); 
		
		jQuery(divIt).remove();
		saveAttachmentIds(eContainer);
		
		var count_li = jQuery(jQuery(eContainer).find("li")).length;
		if(count_li==0){
			jQuery(eContainer).html('<span class="w366-no-photo">'+jQuery(eContainer).data("nophoto")+'</span>');
		}
	});
}

function imageMultipleInit(){
	
	jQuery('.w366-image-type .btn-img-field').on('click',function(e){
		
		var eContainer = jQuery(jQuery(this).parent()).find(".images").get(0);
		
		e.preventDefault();
		var frame_multiple = false;
		
		if (frame_multiple) {
			frame_multiple.open();
			return;
		}
		
		//Select many photos
		var i_number = jQuery(this).attr("data-number");
		if(i_number=='*'){
			var is_multiple = true;
		}else{
			var is_multiple = false;
		}
		
		var media_upload_title = jQuery(this).data('title');
		if(media_upload_title==''){
			media_upload_title = 'Select or Upload Media Of Your Chosen Persuasion';
		}
		
		var media_upload_btn_text = jQuery(this).data('btntext');
		if(media_upload_btn_text==''){
			media_upload_btn_text = 'Use this media';
		}
		
		frame_multiple = wp.media({
			title: media_upload_title,
			button: {
				text: media_upload_btn_text
			},
			multiple: is_multiple
		});
		
		
		// Register Event
		frame_multiple.on( "select", function() {
			var selection = frame_multiple.state().get('selection');
				selection.map( function( attachment ) {
					attachment = attachment.toJSON();
					//console.log(attachment);
					
					// append image
					createImgBlockHtml(attachment, eContainer, is_multiple)
				});
				
				removeItem();
				
				// Close the media frame_multiple
				frame_multiple.close();
		});
		
		// Show media frame_multiple
		frame_multiple.open();
	});//------------------------------------------------------------------------
	
	return false;
///////////////////////////////////////////////////////
}

function createImgBlockHtml(attachment, eContainer, is_multiple){
	// 1 ----------------------------
	if((attachment.sizes).hasOwnProperty('thumbnail')){
		var src_attachment = attachment.sizes.thumbnail.url;
	}else{
		var src_attachment = attachment.url;
	}
	var id_attachment = attachment.id;
	
	var sitem = '<li id="'+id_attachment+'" data-id="'+id_attachment+'" class="attachment save-ready"><a class="dashicons dashicons-no removeit"></a><div class="attachment-preview js--select-attachment type-image subtype-png landscape"><div class="thumbnail"><div class="centered"><img src="'+src_attachment+'" draggable="false"></div></div></div></li>';
	
	if(is_multiple){
		var count_li = jQuery(jQuery(eContainer).find("li")).length;
		if(count_li>0){
			jQuery(eContainer).append(sitem);
		}else{
			jQuery(eContainer).html(sitem);
		}
	}else{
		jQuery(eContainer).html(sitem);
	}
	
	// 2 ----------------------
	saveAttachmentIds(eContainer);
}

function saveAttachmentIds(eContainer){
	var aItems = jQuery(eContainer).find('li.attachment');
	var arrAttachment = new Array();
	var s_id_attachment = '';
	jQuery(aItems).each(function(index, element) {
		var attachment_id = jQuery(this).attr("id");
        arrAttachment.push(attachment_id);
    });
	var s_id_attachment = arrAttachment.join(",");
	var eIntHidden = jQuery(eContainer).prev();
	
	//-----------
	if(s_id_attachment){
		s_id_attachment = 'image:'+s_id_attachment;
	}
	jQuery(eIntHidden).val(s_id_attachment);
}