<?php 
$id = $field['id'];
$is_multiple = $field['is_multiple'];
$btn_text = ($field['btn_text']) ? $field['btn_text'] : 'Chọn hình 1';
$list_attachment_id = get_option($id);
$note = $field['note'];
?>
<div id="container-<?php echo $id; ?>" class="w366-image-type w366-item-8">
	<input type="hidden" class="hdn" value='<?php echo $list_attachment_id;?>' name="<?php echo $id; ?>" />
	<ul id="out-<?php echo $id; ?>" data-image="1" class="attachments ui-sortable ui-sortable-disabled images w366-sortable" data-nophoto="<?php echo $field['no_photo_text']; ?>">
    	<?php 
		if($list_attachment_id){
			$arr_attachment_id = explode(",", str_replace("image:","", $list_attachment_id));
			foreach($arr_attachment_id as $attachment_id){
				$src_attachment = wp_get_attachment_image_url($attachment_id, 'thumbnail');
		?>
        		<li id="<?php echo $attachment_id;?>" data-id="<?php echo $attachment_id;?>" class="attachment save-ready">
                	<a class="dashicons dashicons-no removeit"></a>
                    <div class="attachment-preview js--select-attachment type-image subtype-png landscape">
                        <div class="thumbnail">
                            <div class="centered">
                                <img src="<?php echo $src_attachment; ?>" draggable="false">
                            </div>
                        </div>
                     </div>
            	</li>
        <?php
			}
		}else{
			echo '<span class="w366-no-photo">'.$field['no_photo_text'].'</span>';
		}
		?>
    </ul>
    <?php 
	if($note){
	?>
    <span class="wnote"><?php echo $note; ?></span>
    <?php
	}
	?>
	<button data-title="<?php echo $field['media_upload_title'];?>" data-btntext="<?php echo $field['media_upload_btn_text'];?>" type="button" class="button btn-img-field" data-number="<?php if($is_multiple)echo '*'; else echo '1'; ?>"><?php echo $btn_text; ?></button>
</div>