<?php
define('OPTIONS_PREFIX', 'w366_');
function w366_get_all_slider_option(){
	$post_type = 'wp_igsp_gallery';
	$arr_q = array(
				  'post_type' => $post_type,
				  'post_status' => 'publish',
				  'numberposts' => -1
				  // 'order'    => 'ASC'
				);
	$posts = get_posts($arr_q);
	$option = array('0' => 'Chọn');
	if($posts){
		foreach($posts as $p){
			$option[$p->ID] = $p->post_title; 
		}
	}
	return $option;
}
function w366_get_all_cf7_option(){
	$args = array( 'post_type' => 'wpcf7_contact_form', 'order' => 'DESC','posts_per_page' => '-1');
	$posts = get_posts($args);
	$option = array('0' => 'Chọn');
	if($posts){
		foreach($posts as $p){
			$option[$p->ID] = $p->post_title; 
		}
	}
	return $option;
}


/*
Type: text, select, image, editor, color-picker, textarea, checkbox, checkbox_many, radio => 
*/
//------------------------------------------------ LIST FIELDS -------------------------------------------
$arr_section = array();
// Section 1-----------------------------------------------------------------------------------------------
$arr_section['section-gr-1'] =  
	array(
			'title' => 'Cấu hình web',
			'fields' => array(
								array(  'id' => OPTIONS_PREFIX.'stt_logo',
										'type' => 'image',
										'is_multiple' => false,
										'label' => 'Hình logo',
										'default' => '',
										'note' => '',
										'btn_text' => 'Chọn hình',
										'media_upload_title' => 'Select or Upload Media Of Your Chosen Persuasion',
										'media_upload_btn_text' => 'Use this media',
										'no_photo_text' => 'No photos. Please click below button to upload photo'
									),
								array(  'id' => OPTIONS_PREFIX.'stt_hotline',
										'type' => 'text',
										'label' => 'Hotline',
										'default' => '',
										'placeholder' => '',
										'note' => ''
									),
								array(  'id' => OPTIONS_PREFIX.'setting_hp_slider',
										'type' => 'select',
										'label' => 'Slider trang chủ',
										'options' => w366_get_all_slider_option(),
										'default' => '1',
										'note' => 'Your note here......'
									),
								
								array(  'id' => OPTIONS_PREFIX.'setting_try_cf7_id',
										'type' => 'select',
										'label' => 'Form liên hệ',
										'options' => w366_get_all_cf7_option(),
										'default' => '1',
										'note' => 'Your note here......'
									),
									
								array(  'id' => OPTIONS_PREFIX.'ft_cr_lelf',
										'type' => 'text',
										'label' => 'Footer copy right trái',
										'default' => '',
										'placeholder' => '',
										'note' => ''
									),
								array(  'id' => OPTIONS_PREFIX.'ft_cr_right',
										'type' => 'text',
										'label' => 'Footer Copy Right right',
										'default' => '',
										'placeholder' => '',
										'note' => ''
									),
	
							)
		);

		
//ALL Type Field Demo----------------------------------------------------------------------------------------------
$arr_section_bk['section-gr-4'] =  
	array(
			'title' => 'Demo All Fields',
			'fields' => array(
								// Text
								array(  'id' => OPTIONS_PREFIX.'demo_text',
										'type' => 'text',
										'label' => 'Text box',
										'default' => '',
										'placeholder' => 'Enter your value here',
										'note' => 'Your note here......'
									),
								
								// Select
								array(  'id' => OPTIONS_PREFIX.'demo_select',
										'type' => 'select',
										'label' => 'Select',
										'options' => array('0'=>'--Select--', '1'=>'Val 1', '2'=>'Val 2'),
										'default' => '1',
										'note' => 'Your note here......'
									),
								
								// Textarea
								array(  'id' => OPTIONS_PREFIX.'demo_textarea',
										'type' => 'textarea',
										'rows' => 5,
										'label' => 'Textarea',
										'default' => 'Your default',
										'placeholder' => 'Enter your value here',
										'note' => 'Your note here......'
									),
								
								// Check Box 1
								array(  'id' => OPTIONS_PREFIX.'demo_check_one',
										'type' => 'checkbox',
										'label' => 'Checkbox',
										'default' => '1'
									),
								
								// Check Box Many
								array(  'id' => OPTIONS_PREFIX.'demo_check_many',
										'type' => 'checkbox_many',
										'label' => 'Checkbox (Many)',
										'options' => array(	//'Checkbox 1' => 1, { 1 =>checked ; else 0 }
															'Checkbox 1' => 1, 
															'Checkbox 8' => 1, 
															'Checkbox 6' => 1, 
															'Checkbox 22' => 1, 
															'Checkbox 4' => 0,
															'Checkbox 400' => 1,
															'Checkbox 100' => 1,
															'Checkbox 7' => 0, 
															'Checkbox 105' => 1, 
															'Checkbox 9' => 1,
															'Checkbox 12' => 0, 
															'Checkbox 20' => 0, 
															'Checkbox 5' => 0,
															)
									),
									
									array(  'id' => OPTIONS_PREFIX.'demo_check_many_2',
										'type' => 'checkbox_many',
										'label' => 'Do you like country?',
										'options' => array(	//'Checkbox 1' => 1, { 1 =>checked ; else 0 }
															'Việt Nam' => 1, 
															'Thái Lan' => 0,
															'Ấn độ' => 0, 
															'Hồng Kong' => 1 
															)
									),
								
								// Radio
								array(  'id' => OPTIONS_PREFIX.'demo_radio',
										'type' => 'radio',
										'label' => 'Radio',
										'options' => array(	'Việt Nam' => 0, 
															'Thái Lan' => 0,
															'Ấn độ' => 1, 
															'Hồng Kong' => 0  
															)
									),
								
								// Color Picker
								array(  'id' => OPTIONS_PREFIX.'demo_color_picker',
										'type' => 'color-picker',
										'label' => 'Color Picker',
										'default' => '#fff',
										'note' => 'Your note here......'
									),
								
								// Image: is_multiple = true 
								array(  'id' => OPTIONS_PREFIX.'demo_image_single',
										'type' => 'image',
										'is_multiple' => false,
										'label' => 'Images (Single)',
										'default' => '',
										'note' => 'Your note here......',
										'btn_text' => 'Chọn hình',
										'media_upload_title' => 'Select or Upload Media Of Your Chosen Persuasion',
										'media_upload_btn_text' => 'Use this media',
										'no_photo_text' => 'No photos. Please click below button to upload photo'
									),
									
								// Image: is_multiple = true 
								array(  'id' => OPTIONS_PREFIX.'demo_image',
										'type' => 'image',
										'is_multiple' => true,
										'label' => 'Images (Multiple)',
										'default' => '',
										'note' => 'Your note here......',
										'btn_text' => 'Chọn hình',
										'media_upload_title' => 'Select or Upload Media Of Your Chosen Persuasion',
										'media_upload_btn_text' => 'Use this media',
										'no_photo_text' => 'No photos. Please click below button to upload photo'
									),
									
								// Editor
								array(  'id' => OPTIONS_PREFIX.'demo_editor',
										'type' => 'editor',
										'label' => 'Editor',
										'height' => '',
										'default' => 'Your default content',
										'note' => 'Your note here......'
									),
									
								
							)
		);
		