<?php
/*
Create: Theme-options
//Docs: https://www.sitepoint.com/create-a-wordpress-theme-settings-page-with-the-settings-api/
*/

require_once ( dirname(__FILE__) .'/options.php' );

class W366_theme_option {
	private $page_title = 'Theme Options';
	private $menu_title = 'Theme Options';
	private $menu_slug = 'theme-options-366';
	private $position = 1;
	private $settings_fields = 'myoption-page';
	private $is_tab = 'vertical'; // '' | vertical | horizontal
	
	
	//Construct
	function __construct() {
		add_action("admin_menu", array( $this, 'createMenuThemeOptionPage' ));
		add_action("admin_init", array( $this, 'displayThemePanelFields' ));
		add_action( 'admin_enqueue_scripts', array( $this, 'registerAdminCssJs' ));
   	}
	
	public function getCssUri(){
		$str_path = str_replace("\\","/", dirname(__FILE__) );
		$arr_path = explode("/themes/", $str_path); 
		$uri_css = get_theme_root_uri() . '/'. $arr_path[1] . '/css';
		return $uri_css;
	}
	
	public function getJsUri(){
		$str_path = str_replace("\\","/", dirname(__FILE__) );
		$arr_path = explode("/themes/", $str_path); 
		$uri_js = get_theme_root_uri() . '/'. $arr_path[1] . '/js';
		return $uri_js;
	}
	
	public function getFieldsPath(){
		return dirname(__FILE__) .'/fields';
	}
	
	public function getTemplateField(){
		$str_path = str_replace("\\","/", dirname(__FILE__) );
		$arr_path = explode("/".get_template()."/", $str_path); 
		return $arr_path[1].'/fields';
	}
	
	public function registerAdminCssJs() { 
		// Css ---------------
		wp_register_style( 'w366_wp_admin_css', $this->getCssUri() . '/admin-styles.css', false, '1.0.0' );
		wp_enqueue_style( 'w366_wp_admin_css' );
		
		// Js ---------------
		wp_enqueue_style( 'wp-color-picker' );
    	wp_enqueue_script( 'wp-color-picker');
		wp_enqueue_media();
		wp_enqueue_script( 'w366_type_image', $this->getJsUri() . '/image.js' , array(), '1.0', true );
		wp_enqueue_script( 'w366_wp_admin_script', $this->getJsUri() . '/admin-script.js' , array(), '1.0', true );
	}

	
	public function createMenuThemeOptionPage(){
		$page_title = $this->page_title;
		$menu_title = $this->menu_title;
		$capability = 'manage_options';
		$menu_slug = $this->menu_slug;
		$function_callback = array($this, 'theContentThemeOptionPage');
		$icon_url = '';
		$position = $this->position;
		add_menu_page($page_title, $menu_title, $capability, $menu_slug , $function_callback, $icon_url, $position);
	}
	
	public function theFormOptions(){
		$is_tab = $this->is_tab;
		$clss = '';
		if($is_tab){
			$clss = 'is_tab';
		}
	?>
        <form method="post" action="options.php" <?php if($is_tab){?> class="has-tabs" <?php } ?>>
            <?php
                settings_fields( $this->settings_fields );
    
                global $arr_section;
                if($arr_section){
					$i = 0;
                    foreach($arr_section as $section => $arr_fields){
						$s_clss = '';
						if(!$i){
							$s_clss = 'first active';
						}
						echo '<section id="'.$section.'" class="section '.$clss.' '.$s_clss.'">';
                        do_settings_sections( $section.'-page' ); 
						echo '</section>';
						$i++;
                    }
                } 
                submit_button(); 
            ?>          
        </form>
    <?php
	}
	
	public function theContentThemeOptionPage(){ //var_dump( w366_get_option('demo_text') ); 
		?>
		<div id="poststuff" class="w366-options-area">
			<div class="postbox-container">
				<div class="meta-box-sortables ui-sortable">
					<div class="postbox ">
						<h2 class="hndle ui-sortable-handle">Theme Options</h2>
						<div class="inside <?php if($this->is_tab){?>options-tabs <?php echo $this->is_tab; ?> <?php } ?>">
                        	<?php $this->theCreateTabs(); ?>
							<?php $this->theFormOptions(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	
	
	public function displayThemePanelFields(){
		global $arr_section;
		if($arr_section){
			foreach($arr_section as $section => $arr_item){
				$section_title = $arr_item['title'];
				$arr_fields = $arr_item['fields'];
				if($arr_fields){
					add_settings_section($this->settings_fields, $section_title, null, $section.'-page');
					foreach($arr_fields as $field){
						$label = $field['label']; 
						$description = $field['description'];
						if($description){
							$label .= '<br/><small class="wdescription"><i>('.$description.')</i></small>';
						}
						add_settings_field($field['id'], $label, array($this, 'theW366DisplayField'), $section.'-page', $this->settings_fields, $field);
						register_setting($this->settings_fields, $field['id']);
					}
				}
			}
		}
	}
	
	public function theCreateTabs(){
		if($this->is_tab){
			global $arr_section;
			if($arr_section){
				echo '<h2 class="nav-tab-wrapper wp-clearfix">';
				$i = 0;
				foreach($arr_section as $section => $arr_item){
					$s_clss = '';
					if(!$i){
						$s_clss = 'nav-tab-active';
					}
					$section_title = $arr_item['title'];
					echo '<a id="tab_'.$section.'" class="nav-tab w366-tab-btn '.$s_clss.'" href="#'.$section.'">'.$section_title.'</a>';
					$i++;
				}
				echo '</h2>';
			} 
		}
	}
	
	public function theW366DisplayField($field){
		$type = $field['type'];
		set_query_var( 'field', $field );
		get_template_part( $this->getTemplateField() . '/' .$type, '' );
	}

//---------------End Class-------------------------------------	
}

// Run----------
new W366_theme_option();

// Test: w366_get_option('twitter_url');
function w366_get_option($field_id){
	$val = get_option(OPTIONS_PREFIX . $field_id);
	if( strpos($val, "image:") !== false ){
		$val = str_replace("image:","", $val);
	}
	$arr_json = json_decode($val, true);
	if(is_array($arr_json)){
		$type = $arr_json['type'];
		$arr_val = $arr_json['val'];
		return $arr_json;
	}
	//return apply_filters('the_content', $val);
	return $val;
}

function w366_get_full_src($field_id){
	$attachment_id = w366_get_option( $field_id );
	$arr_attachment_attr =  wp_get_attachment_image_src($attachment_id, '');
	$src_logo = $arr_attachment_attr[0];
	return $src_logo;
}

function w366_the_content_field($field_id){
	echo apply_filters('the_content', w366_get_option($field_id) );
}
