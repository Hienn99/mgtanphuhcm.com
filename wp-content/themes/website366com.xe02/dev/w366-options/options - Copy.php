<?php
define('OPTIONS_PREFIX', 'w366_');
/*
Type: text, select, image, editor, color-picker, textarea, checkbox, checkbox_many, radio => 
*/
//------------------------------------------------ LIST FIELDS -------------------------------------------
$arr_section = array();
// Section 1-----------------------------------------------------------------------------------------------
$arr_section['section-gr-1'] =  
	array(
			'title' => 'All Settings',
			'fields' => array(
								array(  'id' => OPTIONS_PREFIX.'exchange_rate_vnd',
										'type' => 'text',
										'label' => 'Exchange Rate VND',
										'default' => 23000,
										'placeholder' => '23000',
										'note' => 'Exchange rate from USD to VND'
									),
								array(  'id' => OPTIONS_PREFIX.'hotline',
										'type' => 'text',
										'label' => 'Hotline',
										'default' => '+84.983.042.096',
										'placeholder' => '+84.983.042.096'
									),
								array(  'id' => OPTIONS_PREFIX.'cc_email',
										'type' => 'text',
										'label' => 'Visa Apply Receive Email',
										'description' => 'email_1@email.com,email_2@email.com...',
										'default' => '',
										'placeholder' => 'email_1@email.com,email_2@email.com,email_3@email.com....'
									),
							)
		);

// Section 2-----------------------------------------------------------------------------------------------
$arr_section['section-gr-2'] =  
	array(
			'title' => 'Banner Slider Settings',
			'fields' => array(
								// Banner Slider
								array(  'id' => OPTIONS_PREFIX.'banner_img',
										'type' => 'image',
										'is_multiple' => false,
										'label' => 'Banner Image',
										'default' => '',
										'note' => 'Your banner will show on homepage',
										'btn_text' => 'Select your banner',
										'media_upload_title' => 'Select or Upload Media Of Your Chosen Persuasion',
										'media_upload_btn_text' => 'Use this media',
										'no_photo_text' => 'No photos. Please click below button to upload photo'
									),
								array(  'id' => OPTIONS_PREFIX.'banner_title',
										'type' => 'text',
										'label' => 'Banner Title',
										'default' => 'ENTRY VIETNAM VISA',
										'placeholder' => 'ENTRY VIETNAM VISA'
									),
								array(  'id' => OPTIONS_PREFIX.'banner_description',
										'type' => 'text',
										'label' => 'Banner Description',
										'default' => 'Providing the Vietnam visa for 190+ countries and 5,000,000+ travellers every years.',
										'placeholder' => 'Providing the Vietnam visa for 190+ countries and 5,000,000+ travellers every years.'
									),
							)
		);
		
// Section 3-----------------------------------------------------------------------------------------------
$arr_section['section-gr-3'] =  
	array(
			'title' => 'Paypal Setting',
			'fields' => array(
								array(  'id' => OPTIONS_PREFIX.'paypal_sandbox',
										'type' => 'checkbox',
										'label' => 'Sandbox',
										'default' => '',
										'note' => 'If Checked => Mode TEST enable'
									),
								array(  'id' => OPTIONS_PREFIX.'paypal_business',
										'type' => 'text',
										'label' => 'Business Email',
										'default' => '',
										'placeholder' => '',
										'note' => 'Email of account paypal (Receive email)'
									),
									
								array(  'id' => OPTIONS_PREFIX.'paypal_return',
										'type' => 'text',
										'label' => 'Return Url',
										'default' => '',
										'placeholder' => get_site_url().'/thank-you',
										'note' => 'Url page after payment successful'
									),
								array(  'id' => OPTIONS_PREFIX.'paypal_cancel_return',
										'type' => 'text',
										'label' => 'Cancel Return Url',
										'default' => '',
										'placeholder' => get_site_url().'/cancel-return',
										'note' => 'Url page comeback if user cancel payment'
									),
								array(  'id' => OPTIONS_PREFIX.'paypal_currency_code',
										'type' => 'text',
										'label' => 'Currency code',
										'default' => 'USD',
										'placeholder' => 'Currency code: USD',
										'note' => 'Paypal Currency code'
									),
								
							)
		);
		
//ALL Type Field Demo----------------------------------------------------------------------------------------------
$arr_section['section-gr-4'] =  
	array(
			'title' => 'Demo All Fields',
			'fields' => array(
								// Text
								array(  'id' => OPTIONS_PREFIX.'demo_text',
										'type' => 'text',
										'label' => 'Text box',
										'default' => '',
										'placeholder' => 'Enter your value here',
										'note' => 'Your note here......'
									),
								
								// Select
								array(  'id' => OPTIONS_PREFIX.'demo_select',
										'type' => 'select',
										'label' => 'Select',
										'options' => array('0'=>'--Select--', '1'=>'Val 1', '2'=>'Val 2'),
										'default' => '1',
										'note' => 'Your note here......'
									),
								
								// Textarea
								array(  'id' => OPTIONS_PREFIX.'demo_textarea',
										'type' => 'textarea',
										'rows' => 5,
										'label' => 'Textarea',
										'default' => 'Your default',
										'placeholder' => 'Enter your value here',
										'note' => 'Your note here......'
									),
								
								// Check Box 1
								array(  'id' => OPTIONS_PREFIX.'demo_check_one',
										'type' => 'checkbox',
										'label' => 'Checkbox',
										'default' => '1'
									),
								
								// Check Box Many
								array(  'id' => OPTIONS_PREFIX.'demo_check_many',
										'type' => 'checkbox_many',
										'label' => 'Checkbox (Many)',
										'options' => array(	//'Checkbox 1' => 1, { 1 =>checked ; else 0 }
															'Checkbox 1' => 1, 
															'Checkbox 8' => 1, 
															'Checkbox 6' => 1, 
															'Checkbox 22' => 1, 
															'Checkbox 4' => 0,
															'Checkbox 400' => 1,
															'Checkbox 100' => 1,
															'Checkbox 7' => 0, 
															'Checkbox 105' => 1, 
															'Checkbox 9' => 1,
															'Checkbox 12' => 0, 
															'Checkbox 20' => 0, 
															'Checkbox 5' => 0,
															)
									),
									
									array(  'id' => OPTIONS_PREFIX.'demo_check_many_2',
										'type' => 'checkbox_many',
										'label' => 'Do you like country?',
										'options' => array(	//'Checkbox 1' => 1, { 1 =>checked ; else 0 }
															'Việt Nam' => 1, 
															'Thái Lan' => 0,
															'Ấn độ' => 0, 
															'Hồng Kong' => 1 
															)
									),
								
								// Radio
								array(  'id' => OPTIONS_PREFIX.'demo_radio',
										'type' => 'radio',
										'label' => 'Radio',
										'options' => array(	'Việt Nam' => 0, 
															'Thái Lan' => 0,
															'Ấn độ' => 1, 
															'Hồng Kong' => 0  
															)
									),
								
								// Color Picker
								array(  'id' => OPTIONS_PREFIX.'demo_color_picker',
										'type' => 'color-picker',
										'label' => 'Color Picker',
										'default' => '#fff',
										'note' => 'Your note here......'
									),
								
								// Image: is_multiple = true 
								array(  'id' => OPTIONS_PREFIX.'demo_image_single',
										'type' => 'image',
										'is_multiple' => false,
										'label' => 'Images (Single)',
										'default' => '',
										'note' => 'Your note here......',
										'btn_text' => 'Chọn hình',
										'media_upload_title' => 'Select or Upload Media Of Your Chosen Persuasion',
										'media_upload_btn_text' => 'Use this media',
										'no_photo_text' => 'No photos. Please click below button to upload photo'
									),
									
								// Image: is_multiple = true 
								array(  'id' => OPTIONS_PREFIX.'demo_image',
										'type' => 'image',
										'is_multiple' => true,
										'label' => 'Images (Multiple)',
										'default' => '',
										'note' => 'Your note here......',
										'btn_text' => 'Chọn hình',
										'media_upload_title' => 'Select or Upload Media Of Your Chosen Persuasion',
										'media_upload_btn_text' => 'Use this media',
										'no_photo_text' => 'No photos. Please click below button to upload photo'
									),
									
								// Editor
								array(  'id' => OPTIONS_PREFIX.'demo_editor',
										'type' => 'editor',
										'label' => 'Editor',
										'height' => '',
										'default' => 'Your default content',
										'note' => 'Your note here......'
									),
									
								
							)
		);
		