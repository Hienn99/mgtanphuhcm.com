<?php
//Register posttype
$posttype = 'w366_car';
$posttype_slug_use_frontend = 'dong-xe';
$labels = array(
				'name' => __( 'Xe', 'website366com' ),
				'singular_name' => __( 'Wp_w366_car' ),
				'add_new' => __( 'Thêm', 'website366com' ), //.....xem them : https://codex.wordpress.org/Function_Reference/register_post_type
			  );
$posttypes = array(
			  'labels' => $labels,
			  'public' => true,
			  'publicly_queryable' => true,
			  'show_ui' => true,
			  'show_in_menu' => true,
			  'query_var' => false,
			  'capability_type' => 'page',
			  'has_archive' => false,
			  'rewrite' => array( 'slug' => $posttype_slug_use_frontend, 'with_front' => false  ), 
			  'menu_icon'=>'dashicons-images-alt2', //https://developer.wordpress.org/resource/dashicons/#media-text
			  'supports' => array('title','editor','thumbnail') //'title', 'editor', 'comments', 'revisions', 'trackbacks', 'author', 'excerpt', 'page-attributes', 'thumbnail', 'custom-fields', and 'post-formats'
			);
/*############ 2 ################*/			
$slug_tax_use_admin = 'car_tax';
$slug_tax_use_frontend = 'danh-muc-xe';			
$taxonomy = array(	$slug_tax_use_admin,
					array(
					'hierarchical' => true,
					'label' =>  __('Danh mục xe', 'website366com'),
					'query_var' => true,
					'rewrite' => array( 'slug' => $slug_tax_use_frontend, 'with_front' => false ),
					'show_ui' => true,
					'singular_name' => 'W366_car_tax'
					)
			);
			
/*############ 3 ################*/
$columns = array();
$columns['title'] = 'Tên xe';
$columns['featured_image'] = 'Hình';
$columns['date'] = 'Ngày';

/**--------------------**/
$arr_post_type[$posttype] = array(	'post-type' => $posttypes, 
									'taxonomy' => $taxonomy, 
									'columns' =>$columns,
									//'sort_columns' => array('total_amount'),
									'tag'=>$arrTags);


/**********************************
** Posttype Item end
***********************************/

//Register posttype
$posttype = 'w366_acc';
$posttype_slug_use_frontend = 'phu-kien';
$labels = array(
				'name' => __( 'Phụ kiện', 'website366com' ),
				'singular_name' => __( 'Wp_w366_acc' ),
				'add_new' => __( 'Thêm', 'website366com' ), //.....xem them : https://codex.wordpress.org/Function_Reference/register_post_type
			  );
$posttypes = array(
			  'labels' => $labels,
			  'public' => true,
			  'publicly_queryable' => false,
			  'show_ui' => true,
			  'show_in_menu' => true,
			  'query_var' => false,
			  'capability_type' => 'page',
			  'has_archive' => false,
			  'rewrite' => array( 'slug' => $posttype_slug_use_frontend, 'with_front' => false  ), 
			  'menu_icon'=>'dashicons-images-alt2', //https://developer.wordpress.org/resource/dashicons/#media-text
			  'supports' => array('title','thumbnail') //'title', 'editor', 'comments', 'revisions', 'trackbacks', 'author', 'excerpt', 'page-attributes', 'thumbnail', 'custom-fields', and 'post-formats'
			);
/*############ 2 ################*/
$slug_tax_use_admin = 'acc_tax';
$slug_tax_use_frontend = 'danh-muc-phu-kien';			
$taxonomy = array(	$slug_tax_use_admin,
					array(
					'hierarchical' => true,
					'label' =>  __('Danh mục phụ kiện', 'website366com'),
					'query_var' => true,
					'rewrite' => array( 'slug' => $slug_tax_use_frontend, 'with_front' => false ),
					'show_ui' => true,
					'singular_name' => 'W366_acc_tax'
					)
			);
			
			
/*############ 3 ################*/
$columns = array();
$columns['title'] = 'Tên';
$columns['featured_image'] = 'Hình';
$columns['date'] = 'Ngày';

/**--------------------**/
$arr_post_type[$posttype] = array(	'post-type' => $posttypes, 
									'taxonomy' => $taxonomy, 
									'columns' =>$columns,
									//'sort_columns' => array('total_amount'),
									'tag'=>$arrTags);


/**********************************
** Posttype Item end
***********************************/

//Register posttype
$posttype = 'w366_service';
$posttype_slug_use_frontend = 'dich-vu';
$labels = array(
				'name' => __( 'Dịch vụ', 'website366com' ),
				'singular_name' => __( 'Wp_w366_service' ),
				'add_new' => __( 'Thêm', 'website366com' ), //.....xem them : https://codex.wordpress.org/Function_Reference/register_post_type
			  );
$posttypes = array(
			  'labels' => $labels,
			  'public' => true,
			  'publicly_queryable' => true,
			  'show_ui' => true,
			  'show_in_menu' => true,
			  'query_var' => false,
			  'capability_type' => 'page',
			  'has_archive' => false,
			  'rewrite' => array( 'slug' => $posttype_slug_use_frontend, 'with_front' => false  ), 
			  'menu_icon'=>'dashicons-images-alt2', //https://developer.wordpress.org/resource/dashicons/#media-text
			  'supports' => array('title','editor','thumbnail') //'title', 'editor', 'comments', 'revisions', 'trackbacks', 'author', 'excerpt', 'page-attributes', 'thumbnail', 'custom-fields', and 'post-formats'
			);
/*############ 2 ################*/		
$taxonomy = NULL;
			
/*############ 3 ################*/
$columns = array();
$columns['title'] = 'Tên';
$columns['featured_image'] = 'Hình';
$columns['date'] = 'Ngày';

/**--------------------**/
$arr_post_type[$posttype] = array(	'post-type' => $posttypes, 
									'taxonomy' => $taxonomy, 
									'columns' =>$columns,
									//'sort_columns' => array('total_amount'),
									'tag'=>$arrTags);


/**********************************
** Posttype Item end
***********************************/