<?php
/***********************************************************************************************************
** W366 CUSTOM WIDGETS SIDEBAR
***********************************************************************************************************/
//[w366_dynamic_sidebar id="sidebar-homepage"]
add_action( 'init', function(){ add_shortcode( 'w366_dynamic_sidebar', 'w366_dynamic_sidebar_func' ); } );
function w366_dynamic_sidebar_func($atts){
	extract(shortcode_atts(array(
		'id'=>''
	), $atts));

	if ( is_active_sidebar( $id ) ) { 
		ob_start();
			dynamic_sidebar( $id );
			$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
}

if( is_admin() ){
	$current_page = get_site_url(). $_SERVER['REQUEST_URI'];
	if( strpos($current_page, 'widgets.php') !== false){
		//-----------------------------------------------------------------
		add_filter( 'admin_body_class', function($classes){ return "$classes w366-custom-widget"; } );
		
		//-----------------------------------------------------------------
		add_action('admin_footer', 'w366_custom_widget_css_js');
		function w366_custom_widget_css_js() {
			?>
            <style>
            	.w366-custom-widget div.widget-liquid-left {
					display:none;
				}
				.w366-custom-widget div.widget-liquid-right{
					width:100%;
				}
				.w366-custom-widget div#widgets-right .sidebars-column-1, 
				.w366-custom-widget div#widgets-right .sidebars-column-2{
					width:50%;
					max-width:49%;
				}
				.w366-custom-wg-actions{
					float:left;
					width:100%;
				}
				.w366-custom-widget .widget-control-actions .alignleft{
					display:none;
				}
				.sidebar-shortcode .shortcode{
					font-weight:bold;
				}
            </style>
            
            <script>
            	jQuery( document ).ready(function() {
					// append action
					var btn_show_widgets = '<input type="button" id="w366-custom-wg-show" class="button button-primary widget-control-save right" value="Hiển thị Widgets">';
					jQuery(".widget-liquid-right").prepend('<div class="w366-custom-wg-actions">'+btn_show_widgets+'</div>');
					
					// click button
					jQuery("#w366-custom-wg-show").click(function(){
						if( jQuery('body').hasClass('w366-custom-widget') ){
							jQuery('body').removeClass('w366-custom-widget');
							jQuery(this).val('Ẩn Widgets');
						}else{
							jQuery('body').addClass('w366-custom-widget');
							jQuery(this).val('Hiển thị Widgets');
						}
					});
					
					// Show shortcde
					jQuery("#widgets-right .widgets-sortables").each(function(index, element) {
                        var sidebar_id = jQuery(this).attr("id");
						var shortcode = '[w366_dynamic_sidebar id="'+sidebar_id+'"]';
						var div_contain_shortcode = '<div class="sidebar-shortcode"><p class="description">Shortcode: <span class="shortcode">'+shortcode+'</span></p></div>';
						jQuery("#"+sidebar_id+" .sidebar-description").after(div_contain_shortcode);
                    });
				});
            </script>
            <?php
		}
	}
}