<?php
session_start();

/*Functions Dev*/
function w366_dev_setup() {
	//Add New Size Media
	add_image_size( 'thumb_360x260', 360, 260, true );
	add_image_size( 'thumb_90x60', 90, 60, true );
	add_image_size( 'thumb_600x400', 600, 400, true );
	
}
add_action( 'after_setup_theme', 'w366_dev_setup' );


//Active do_shortcode for Text Widget
add_filter('widget_text','do_shortcode');

/*Your function here....*/
//show_admin_bar( false );

function w366_format_money($amount, $currency=false){
	if($currency){
		//return number_format_i18n($amount, 0) . ' '. getCsCurrency();
		return number_format($amount, 0, '.', ','). ' '. getCsCurrency();
	}
	//return number_format_i18n($amount, 0) ;
	return number_format($amount, 0, '.', ',');
}

function getCsCurrency(){
	return '₫';
}

function w366_all_accessory_tax(){
	$terms = get_terms( array(
		'taxonomy' => 'acc_tax',
		'hide_empty' => false,
		'parent' => 0
	) );
	return $terms;
}

function w366_all_car_tax(){
	$terms = get_terms( array(
		'taxonomy' => 'car_tax',
		'hide_empty' => false,
		'parent' => 0
	) );
	return $terms;
}

