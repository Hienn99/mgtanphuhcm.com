<?php
class w366_home_contact_info extends WP_Widget {
    function __construct() {
		parent::__construct(
			'w366_home_contact_info', esc_html_x('* Thông tin liên hệ', 'widget name', 'website366com'),
			array(
				'classname' => 'w366_home_contact_info',
				'description' => esc_html__('Thông tin gần footer', 'website366com'),
				'customize_selective_refresh' => true
			)
		);
		add_action( 'admin_head', array( $this, 'register_js' ) );
	}
	
	function register_js(){
	?>
    <script>
		function initClickMedia( id ){ //alert(id); return false;
			jQuery('#'+id).on('click',function(e){
				var input_id = jQuery(this).data('input'); 
				var div_id = 'div_'+id;
				
				e.preventDefault();
				var frame_multiple_wop = false;
				
				if (frame_multiple_wop) {
					frame_multiple_wop.open();
					return;
				}
				
				//Select many photos
				var is_multiple = false;
				
				frame_multiple_wop = wp.media({
					multiple: is_multiple
				});

				// Register Event
				frame_multiple_wop.on( "select", function() {
					var selection = frame_multiple_wop.state().get('selection');
						selection.map( function( attachment ) {
							attachment = attachment.toJSON();
							//console.log(attachment);
							
							var bg_url = attachment.url;
							jQuery("#"+input_id).val(bg_url);
							if(attachment.sizes.thumbnail==undefined){
								var thumb_url = attachment.url;
							}else{	
								var thumb_url = attachment.sizes.thumbnail.url;
							}
							
							var div_id_count = 1*jQuery('#'+div_id).length;
							if(div_id_count==0){
								jQuery("#"+input_id).before('<div id="'+div_id+'"><img src="'+thumb_url+'" /></div>');
							}else{
								jQuery('#'+div_id + ' img').attr('src', thumb_url);
							}
							jQuery("input.widget-control-save").removeAttr('disabled');
							
						});
						
						// Close the media frame_multiple_wop
						frame_multiple_wop.close();
				});
				
				// Show media frame_multiple_wop
				frame_multiple_wop.open();
			});//------------------------------------------------------------------------
			
			return false;
		}
		
		
		
    </script>
    <?php
	}
	
    function widget($args, $instance) {
	    $defaults = array('title' => '');
		$title = $instance['title'];  
		$bg_url = $instance['bg_url'];
		
		$fb_link = $instance['fb_link'];
		$fb_title =  $instance['fb_title'];
		$yt_link =  $instance['yt_link'];
		$yt_title =  $instance['yt_title'];
		$twitter_link =  $instance['twitter_link'];
		$twitter_title =  $instance['twitter_title'];
		$video_link =  $instance['video_link'];
		
		$video_link_arr = explode("?v=", $video_link);
		$code = $video_link_arr[1];
		
		echo $args['before_widget'];
		?>
        	<div class="home-social">
            	<div class="container">
                	<div class="row">
                    	<div class="col col-3">
                            <img alt="<?=$title?>" src="<?=$bg_url?>">
                        </div>
                        <div class="col col-3 feeds">
                            <div class="item" style=" padding: 0; ">
                                <h3><a href="/" style=" text-align: center; width: 100%; display: block; margin-bottom: 5px; "><?=$title?></a></h3>
                            </div>
                            <div class="item facebook">
                                <a rel="nofollow" class="favicon" target="_blank" href="<?=$fb_link?>">Facebook</a>
                                <h3><a rel="nofollow" href="<?=$fb_link?>" target="blank">Fanpage</a></h3>
                                <p><?=$fb_title?></p>
                            </div>
                            <div class="item youtube">
                                <a rel="nofollow" class="favicon" target="_blank" href="<?=$yt_link?>">YouTube</a>
                                <h3><a rel="nofollow" href="<?=$yt_link?>" target="blank">Channel</a></h3>
                                <p><?=$yt_title?></p>
                            </div>
            
                            <div class="item twitter">
                                <a rel="nofollow" class="favicon" target="_blank" href="<?=$twitter_link?>">Twitter</a>
                                <h3><a rel="nofollow" href="<?=$twitter_link?>" target="blank">Twitter</a></h3>
                                <p><?=$twitter_title?></p>
                            </div>
                        </div>
                        <div class="col col-6">
                            <div class="video-wrapper">
                                <iframe width="100%" height="270" src="//www.youtube.com/embed/<?=$code?>" frameborder="0" allowfullscreen=""></iframe>					
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        echo $args['after_widget'];
    }
    function update($new_instance, $old_instance) {
        $instance = array();
		if (!empty($new_instance['title'])) {
			$instance['title'] = ($new_instance['title']);
		}
		
		$instance['bg_url'] = ($new_instance['bg_url']);
		
		$instance['fb_link'] = ($new_instance['fb_link']);
		$instance['fb_title'] = ($new_instance['fb_title']);
		$instance['yt_link'] = ($new_instance['yt_link']);
		$instance['yt_title'] = ($new_instance['yt_title']);
		$instance['twitter_link'] = ($new_instance['twitter_link']);
		$instance['twitter_title'] = ($new_instance['twitter_title']);
		$instance['video_link'] = ($new_instance['video_link']);
		
        return $instance;
    }
	
	function get_circle_limit(){ return 6; }
	
	
    function form($instance) {	
	    $defaults = array('title' => '');
	   
        $instance = wp_parse_args($instance, $defaults); ?>
        
		
        <p>
			<?php 
                $id = 'w366_click_img_'.uniqid();
                $bg_url = 'bg_url'
            ?>
            <label for="<?php echo esc_attr($this->get_field_id($bg_url)); ?>"><?php esc_html_e('Hình', 'website366com'); ?></label>
            <?php 
                $url_img = $instance[$bg_url];
                if($url_img){
                    $id_attachment = attachment_url_to_postid($url_img);
                    $arr_thumb_url = wp_get_attachment_image_src($id_attachment, 'thumbnail');
                ?>
                    <div id="div_<?php echo $id; ?>">
                        <img src="<?php echo $arr_thumb_url[0]; ?>">
                    </div>
                <?php
                }
            ?>
            <input class="widefat url-hide" type="text" value="<?php echo esc_attr($instance[$bg_url]); ?>" name="<?php echo esc_attr($this->get_field_name($bg_url)); ?>" id="<?php echo esc_attr($this->get_field_id($bg_url)); ?>" />
            <br/>
            <button id="<?php echo $id; ?>" data-input='<?php echo esc_attr($this->get_field_id($bg_url)); ?>' class="button w366_click_img">Chọn hình</button>
            <script>
                initClickMedia('<?php echo $id; ?>');
            </script>
        </p>
        
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Tiêu đề', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['title']); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" id="<?php echo esc_attr($this->get_field_id('title')); ?>" />
        </p>
        
         <p>
        	<label for="<?php echo esc_attr($this->get_field_id('fb_link')); ?>"><?php esc_html_e('Link facebook', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['fb_link']); ?>" name="<?php echo esc_attr($this->get_field_name('fb_link')); ?>" id="<?php echo esc_attr($this->get_field_id('fb_link')); ?>" />
        </p>
        
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('fb_title')); ?>"><?php esc_html_e('Mô tả facebook', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['fb_title']); ?>" name="<?php echo esc_attr($this->get_field_name('fb_title')); ?>" id="<?php echo esc_attr($this->get_field_id('fb_title')); ?>" />
        </p>
        
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('yt_link')); ?>"><?php esc_html_e('Link youtube', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['yt_link']); ?>" name="<?php echo esc_attr($this->get_field_name('yt_link')); ?>" id="<?php echo esc_attr($this->get_field_id('yt_link')); ?>" />
        </p>
        
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('yt_title')); ?>"><?php esc_html_e('Mô tả youtube', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['yt_title']); ?>" name="<?php echo esc_attr($this->get_field_name('yt_title')); ?>" id="<?php echo esc_attr($this->get_field_id('yt_title')); ?>" />
        </p>
        
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('twitter_link')); ?>"><?php esc_html_e('Link twitter', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['twitter_link']); ?>" name="<?php echo esc_attr($this->get_field_name('twitter_link')); ?>" id="<?php echo esc_attr($this->get_field_id('twitter_link')); ?>" />
        </p>
        
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('twitter_title')); ?>"><?php esc_html_e('Mô tả twitter', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['twitter_title']); ?>" name="<?php echo esc_attr($this->get_field_name('twitter_title')); ?>" id="<?php echo esc_attr($this->get_field_id('twitter_title')); ?>" />
        </p>
        
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('video_link')); ?>"><?php esc_html_e('Link video', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['video_link']); ?>" name="<?php echo esc_attr($this->get_field_name('video_link')); ?>" id="<?php echo esc_attr($this->get_field_id('video_link')); ?>" />
        </p>
        
   	<?php
    }
}

?>