<?php
class w366_home_car extends WP_Widget {
    function __construct() {
		parent::__construct(
			'w366_home_car', esc_html_x('* Các dòng xe', 'widget name', 'website366com'),
			array(
				'classname' => 'w366_home_car',
				'description' => esc_html__('các dòng xe ở trang chủ', 'website366com'),
				'customize_selective_refresh' => true
			)
		);
	}
    function widget($args, $instance) {
	    $defaults = array('title' => '');
		$title = $instance['title'];    
		$per_page = $instance['per_page'];
		
		$arr_car_term = w366_all_car_tax();
		
		if ( $arr_car_term ) {
			echo $args['before_widget'];
			?>
            <div class="cars">
                <div class="container">
                    <div class="box_mid">
                        <div class="mid-title ">
                            <div class="titleL">
                                <h1><?=$title?></h1>
                            </div>
                            <div class="titleR"></div>
                        </div>
                     </div>
                    
                    <div class="row">
                    <?php 
					foreach($arr_car_term as $obj_car_term){
						set_query_var( 'obj_car_term', $obj_car_term );
					?>
                    	<div class="col-md-4">
                            <?php 
								get_template_part( 'template-parts/product/tax', 'loop' );
							?>
                        </div>
                     <?php
					}
					?>
                    </div>
                </div>
            </div>
			<?php
			echo $args['after_widget'];
		}
    }
    function update($new_instance, $old_instance) {
        $instance = array();
		if (!empty($new_instance['title'])) {
			$instance['title'] = sanitize_text_field($new_instance['title']);
		}
		
		$instance['per_page'] = ($new_instance['per_page']);  
        return $instance;
    }
    function form($instance) {
		
	    $defaults = array('title' => '', 'per_page' => 6);
	   
        $instance = wp_parse_args($instance, $defaults); ?>
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Tiêu đề', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['title']); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" id="<?php echo esc_attr($this->get_field_id('title')); ?>" />
        </p>

        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('per_page')); ?>"><?php esc_html_e('Số lượng', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['per_page']); ?>" name="<?php echo esc_attr($this->get_field_name('per_page')); ?>" id="<?php echo esc_attr($this->get_field_id('per_page')); ?>" />
        </p>
        
   	<?php
    }
}

?>