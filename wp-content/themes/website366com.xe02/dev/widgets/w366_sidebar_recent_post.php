<?php
class w366_sidebar_recent_post extends WP_Widget {
    function __construct() {
		parent::__construct(
			'w366_sidebar_recent_post', esc_html_x('* Tin tức có hình', 'widget name', 'website366com'),
			array(
				'classname' => 'w366_sidebar_recent_post',
				'description' => esc_html__('Tin tức bên cột', 'website366com'),
				'customize_selective_refresh' => true
			)
		);
	}
    function widget($args, $instance) {
	    $defaults = array('title' => '');
		$title = $instance['title'];    
		$cat_id = $instance['category'];
		$per_page = $instance['per_page'];
		
		$args_q = array(  'post_type' => 'post',
						'posts_per_page' =>  $per_page ,
						'order'     => 'desc'
		);
		if($cat_id){
			$objCatCurrent = get_term_by('id', $cat_id, 'category', ''); 
			$args_q['tax_query'] = array(
											array(
												'taxonomy'  => 'category',
												'field'     => 'id', 
												'terms'     => $cat_id
											)
										);
		
		}
		$the_query = new WP_Query( $args_q );
		
		if ( $the_query->have_posts() ) {
			echo $args['before_widget'];
				echo $args['before_title'] . $title . $args['after_title'];
			?>
				<div class="vnt-news-most">
                	<?php 
					while ( $the_query->have_posts() ) { $the_query->the_post();
					?>
                        <div class="item">
                            <div class="img" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
                            	<a href="<?php the_permalink();?>"><?php the_post_thumbnail('thumb_90x60'); ?></a>
                            </div>
                            <div class="tend">
                            	<h3 itemprop="headline"><a href="<?php the_permalink();?>" tabindex="0"><?php the_title();?></a></h3>
                            </div>
                            <div class="clear"></div>
                        </div>	
                    <?php
					}
					wp_reset_postdata();
					?>
                </div>
			<?php
			echo $args['after_widget'];
		}
    }
    function update($new_instance, $old_instance) {
        $instance = array();
		if (!empty($new_instance['title'])) {
			$instance['title'] = sanitize_text_field($new_instance['title']);
		}
		
		$instance['category'] = (!empty($new_instance['category'])) ? $new_instance['category'] : '';
		
		$instance['per_page'] = ($new_instance['per_page']);  
        return $instance;
    }
    function form($instance) {
		
	    $defaults = array('title' => '', 'per_page' => 6);
	   
        $instance = wp_parse_args($instance, $defaults); ?>
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Tiêu đề', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['title']); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" id="<?php echo esc_attr($this->get_field_id('title')); ?>" />
        </p>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('category')); ?>"><?php esc_html_e('Chuyên mục', 'website366com'); ?></label>
            <select id="<?php echo esc_attr($this->get_field_id('category')); ?>" class="widefat" name="<?php echo esc_attr($this->get_field_name('category')); ?>">
                <option value="" <?php selected("", $instance['category']); ?>><?php esc_html_e('Mới nhất', 'website366com'); ?></option>
				<?php 
					$args = array(
						 'taxonomy'   => 'category',
						 'hide_empty' => false,
					);
					$cats = get_terms( $args );
					if($cats):
						foreach($cats as $objCat): ?>
                        	 <option value="<?php echo $objCat->term_id; ?>" <?php selected($objCat->term_id, $instance['category']); ?>><?php echo $objCat->name; ?> (<?php echo $objCat->count;?>)</option>
				<?php
						endforeach;
					endif;
				?>
            </select>
            <small><?php esc_html_e('Chọn chuyên mục hoặc Tin tức mới nhất', 'website366com'); ?></small>
		</p>
		
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('per_page')); ?>"><?php esc_html_e('Số lượng', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['per_page']); ?>" name="<?php echo esc_attr($this->get_field_name('per_page')); ?>" id="<?php echo esc_attr($this->get_field_id('per_page')); ?>" />
        </p>
        
   	<?php
    }
}

?>