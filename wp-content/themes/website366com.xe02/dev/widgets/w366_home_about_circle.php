<?php
class w366_home_about_circle extends WP_Widget {
    function __construct() {
		parent::__construct(
			'w366_home_about_circle', esc_html_x('* Giới thiệu (Vòng tròn)', 'widget name', 'website366com'),
			array(
				'classname' => 'w366_home_about_circle',
				'description' => esc_html__('Giới thiệu ở trang chủ có các vòng tròn', 'website366com'),
				'customize_selective_refresh' => true
			)
		);
		add_action( 'admin_head', array( $this, 'register_js' ) );
	}
	
	function register_js(){
	?>
    <script>
		function initClickMedia( id ){ //alert(id); return false;
			jQuery('#'+id).on('click',function(e){
				var input_id = jQuery(this).data('input'); 
				var div_id = 'div_'+id;
				
				e.preventDefault();
				var frame_multiple_wop = false;
				
				if (frame_multiple_wop) {
					frame_multiple_wop.open();
					return;
				}
				
				//Select many photos
				var is_multiple = false;
				
				frame_multiple_wop = wp.media({
					multiple: is_multiple
				});

				// Register Event
				frame_multiple_wop.on( "select", function() {
					var selection = frame_multiple_wop.state().get('selection');
						selection.map( function( attachment ) {
							attachment = attachment.toJSON();
							//console.log(attachment);
							
							var bg_url = attachment.url;
							jQuery("#"+input_id).val(bg_url);
							if(attachment.sizes.thumbnail==undefined){
								var thumb_url = attachment.url;
							}else{	
								var thumb_url = attachment.sizes.thumbnail.url;
							}
							
							var div_id_count = 1*jQuery('#'+div_id).length;
							if(div_id_count==0){
								jQuery("#"+input_id).before('<div id="'+div_id+'"><img src="'+thumb_url+'" /></div>');
							}else{
								jQuery('#'+div_id + ' img').attr('src', thumb_url);
							}
							jQuery("input.widget-control-save").removeAttr('disabled');
							
						});
						
						// Close the media frame_multiple_wop
						frame_multiple_wop.close();
				});
				
				// Show media frame_multiple_wop
				frame_multiple_wop.open();
			});//------------------------------------------------------------------------
			
			return false;
		}
		
		
		
    </script>
    <?php
	}
	
    function widget($args, $instance) {
	    $defaults = array('title' => '');
		$title = $instance['title'];  
		$description = $instance['description']; 
		$title2 = $instance['title2'];
		
		echo $args['before_widget'];
		?>
           <div class="advHome">
                <div class="momCircle">
                    <div class="grid">
                    	<?php 
						$limit = $this->get_circle_limit();
						if($limit){
							for($i=1; $i<=$limit; $i++){
								$title_i = $instance['block_title_'.$i];
								$link_i = $instance['block_link_'.$i];
								$bg_url = $instance['block_img_'.$i];
						?>
                        	<div class="col">
                                <div class="circle v<?=$i?> <?php if($i==1) echo 'effect'; ?>">
                                    <div class="img">
                                        <a href="<?=$link_i?>" target="_blank">
                                            <img src="<?=$bg_url?>" alt="<?=$title_i?>">
                                        </a>
                                    </div>
                                    <div class="txt"><?=$title_i?></div>
                                </div>
                            </div>
                        <?php

							}
						}
						?>
                    </div>
                </div>
                
                <div class="infoCircle">
                    <div class="t1"><?php echo $title; ?></div>
                    <div class="t2"><?php echo $description; ?></div>
                    <div class="t3"><?php echo $title2; ?></div>
                </div>
           		
           </div>
        <?php
        echo $args['after_widget'];
    }
    function update($new_instance, $old_instance) {
        $instance = array();
		if (!empty($new_instance['title'])) {
			$instance['title'] = sanitize_text_field($new_instance['title']);
		}
		
		$instance['description'] = ($new_instance['description']);
		
		if (!empty($new_instance['title2'])) {
			$instance['title2'] = sanitize_text_field($new_instance['title2']);
		}
		
		$limit = $this->get_circle_limit();
		if($limit){
			for($i=1; $i<=$limit; $i++){
				$title_i = 'block_title_'.$i;
				$link_i = 'block_link_'.$i;
				$bg_url = 'block_img_'.$i;
			
				$instance[$title_i] = ($new_instance[$title_i]);
				$instance[$link_i] = ($new_instance[$link_i]);
				$instance[$bg_url] = ($new_instance[$bg_url]);
			}
		}
		
        return $instance;
    }
	
	function get_circle_limit(){ return 6; }
	
	
    function form($instance) {	
	    $defaults = array('title' => '');
	   
        $instance = wp_parse_args($instance, $defaults); ?>
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Tiêu đề', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['title']); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" id="<?php echo esc_attr($this->get_field_id('title')); ?>" />
        </p>
        
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('description')); ?>"><?php esc_html_e('Mô tả', 'website366com'); ?></label>
			<textarea rows="6" class="widefat" name="<?php echo esc_attr($this->get_field_name('description')); ?>" id="<?php echo esc_attr($this->get_field_id('description')); ?>"><?php echo esc_attr($instance['description']); ?></textarea>
        	<small><?php _e('Mô tả', 'website366com')?></small>
        </p>
        
         <p>
        	<label for="<?php echo esc_attr($this->get_field_id('title2')); ?>"><?php esc_html_e('Tiêu đề dưới', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['title2']); ?>" name="<?php echo esc_attr($this->get_field_name('title2')); ?>" id="<?php echo esc_attr($this->get_field_id('title2')); ?>" />
        </p>
        
        <style>
        	.block{
				background: #f1f1f1;
				padding: 10px;
				border: 1px solid #e5e5e5;
				box-shadow: 0 1px 1px rgba(0,0,0,.04);
				margin-bottom:20px;
			}
			.url-hide{ display:none; }
        </style>
        
        <?php 
		$limit = $this->get_circle_limit();
		if($limit){
		echo '<div class="block_all">';
			for($i=1; $i<=$limit; $i++){
				$title_i = 'block_title_'.$i;
				$link_i = 'block_link_'.$i;
		?>
        	<div class="block">
            	<p><strong>Nhóm <?php echo $i; ?></strong></p>
            	 <p>
                    <label for="<?php echo esc_attr($this->get_field_id($title_i)); ?>"><?php esc_html_e('Tiêu đề', 'website366com'); ?></label>
                    <input class="widefat" type="text" value="<?php echo esc_attr($instance[$title_i]); ?>" name="<?php echo esc_attr($this->get_field_name($title_i)); ?>" id="<?php echo esc_attr($this->get_field_id($title_i)); ?>" />
                </p>
                
                <p>
					<?php 
                        $id = 'w366_click_img_'.uniqid();
                        $bg_url = 'block_img_'.$i;
                    ?>
                	<label for="<?php echo esc_attr($this->get_field_id($bg_url)); ?>"><?php esc_html_e('Hình icon', 'website366com'); ?></label>
                    <?php 
						$url_img = $instance[$bg_url];
						if($url_img){
							$id_attachment = attachment_url_to_postid($url_img);
							$arr_thumb_url = wp_get_attachment_image_src($id_attachment, 'thumbnail');
						?>
							<div id="div_<?php echo $id; ?>">
								<img src="<?php echo $arr_thumb_url[0]; ?>">
							</div>
						<?php
						}
					?>
                    <input class="widefat url-hide" type="text" value="<?php echo esc_attr($instance[$bg_url]); ?>" name="<?php echo esc_attr($this->get_field_name($bg_url)); ?>" id="<?php echo esc_attr($this->get_field_id($bg_url)); ?>" />
                    <br/>
                    <button id="<?php echo $id; ?>" data-input='<?php echo esc_attr($this->get_field_id($bg_url)); ?>' class="button w366_click_img">Chọn hình</button>
                    <script>
                        initClickMedia('<?php echo $id; ?>');
                    </script>
                </p>
                
                <p>
                    <label for="<?php echo esc_attr($this->get_field_id($link_i)); ?>"><?php esc_html_e('Link', 'website366com'); ?></label>
                    <input class="widefat" type="text" value="<?php echo esc_attr($instance[$link_i]); ?>" name="<?php echo esc_attr($this->get_field_name($link_i)); ?>" id="<?php echo esc_attr($this->get_field_id($link_i)); ?>" />
                    <small>http://</small>
                </p>
            </div>
        <?php
			}
		echo '</div>';
		}
		?>
        
   	<?php
    }
}

?>