<?php
if( !class_exists('W_SQLMaster') ):
	class W_SQLMaster {
		const TABLE = 'null';
		const ID = 'null';
		
		private $_data = array();
		
		//__construct
		public function __construct($id = NULL) {
			if($id){
				$class = get_called_class();
				$table = $class::TABLE;
				$ID = $class::ID;
				
				global $wpdb;
				$sql = "SELECT 	* 
						FROM 	`".$table."` 
						WHERE 	`".$ID."` = '".$id."' ";
				$rows = $wpdb->get_results($sql,ARRAY_A);
				if($rows[0]){
					foreach($rows[0] as $k=>$val){
						$this->$k = $val;
					}
				}
			}
		}
		// ./__construct
		public function getClass(){
			return get_called_class();
		}
		
		public function getId(){
			$id = $this->getIDCol(); 
			return $this->$id;
		}
		
		public function getIDCol(){
			$class = $this->getClass();
			$ID = $class::ID;
			return $ID;
		}
		
		public function getTable(){
			$class = $this->getClass();
			$table = $class::TABLE;
			return $table;
		}
		
		/**
		** Get All Property
		**/
		public function  __getAllProperties(){
			return array_keys($this->_data);
		}
		
		/**
		** Setter
		**/
		public function __set ($key, $value) {
			if (is_object($value)) {
				$this->_data[$key] = $value;
			} else {
				$this->_data[$key] = addslashes($value);
			}
		}
		
		/**
		** Get Property
		**/
		public function __get($property) {
			switch ($property) {
				default :
					if (array_key_exists ( $property, $this->_data )) {
						if (is_object ( $this->_data [$property] ))
							return $this->_data [$property];
						return stripslashes ( htmlspecialchars_decode ( $this->_data [$property], ENT_NOQUOTES ) );
					}
					return null;
					break;
			}
		}
		
		public function getData(){
			$aData = array();
			$properties = $this->__getAllProperties();
			if($properties){
				foreach($properties as $key){
					$val = $this->__get($key);
					if($key=='created' && $val=='' || $key=='edited' && $val==''){
						$aData[$key] = current_time('Y/m/d H:i:s', 0);
					}else{
						$aData[$key] = $val;
					}
				}
			}
			return $aData;
		}
		
		
		//Insert; Update; Delete
		public function save(){
			global $wpdb;
			$class = get_called_class();
			$table = $class::TABLE;
			$aData = $this->getData();
			$bool = $wpdb->insert($table, $aData);
			if($bool){
				$lastid = $wpdb->insert_id;
				$objClass = new $class($lastid);
			}
			return $objClass;
		}
		
		public function update(){
			global $wpdb;
			$class = get_called_class();
			$table = $class::TABLE;
			$id = $class::ID;
			$aData = $this->getData();
			$bool = $wpdb->update($table, $aData, array('id'=>$this->$id));
			if($bool){
				$lastid = $wpdb->insert_id;
				$objClass = new $class($lastid);
			}
			return $objClass;
		}
		
		public function delete(){
			global $wpdb;
			$class = get_called_class();
			$table = $class::TABLE;
			$id = $class::ID;
			return $wpdb->delete( $table, array('id'=>$this->$id));
		}
		
		
		
	//End Class
	}
endif;