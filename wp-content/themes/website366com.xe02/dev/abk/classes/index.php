<?php
$arr_init = array(	
	'W_SQLMaster',
	'W366Demo',
	//'add_new_here', //file exist : add_new_here.php
);

if($arr_init):
	foreach($arr_init as $file_name){
		$file = realpath(dirname(__FILE__)).'/'.$file_name.'.php';
		if( file_exists($file) ){
			require_once($file);
		}
	}
endif;	//end if arr_init

/*
Use to call single class if you dont want require_once everywhere
=>E.g:  callClass('W366Demo');
*/
function callClass($class_file_name){
	$file = realpath(dirname(__FILE__)).'/'.$class_file_name.'.php';
	if( file_exists($file) ){
		require_once($file);
	}
}