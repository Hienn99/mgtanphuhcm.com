<?php 
//https://code.tutsplus.com/vi/tutorials/creating-a-paypal-payment-form--net-6
//http://phpcoban.com/thanh-toan-bang-credit-card-qua-paypal/
//https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/formbasics/
?>

<!-- 
https://www.paypal.com/cgi-bin/webscr
-->
<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="leauvuong-facilitator@gmail.com">
    <input type="hidden" name="item_name" value="Entry Vietnam Apply Visa"> <!-- product Name-->
    <input type="hidden" name="item_number" value="VS-621254"> <!-- product ID-->
    <input type="hidden" name="amount" value="215"> <!-- Total -->
    <input type="hidden" name="quantity" value="1"> <!-- product quantity-->
    <input type="hidden" name="currency_code" value="USD">
    
    <input type="hidden" name="return" value="#url_return">
    <input type="hidden" name="cancel_return" value="#cancel_return">
    
    <!-- Customer -->
    <!--
    <input type="hidden" name="first_name" value="">
    <input type="hidden" name="last_name" value="">
    <input type="hidden" name="address" value="">
    <input type="hidden" name="country" value="">
    -->
    <input type="hidden" name="email" value="jdoe@zyzzyu.com">
    
    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
	<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
