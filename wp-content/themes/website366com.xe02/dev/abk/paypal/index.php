<?php 
//https://code.tutsplus.com/vi/tutorials/creating-a-paypal-payment-form--net-6
//http://phpcoban.com/thanh-toan-bang-credit-card-qua-paypal/
//https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/formbasics/

/*$arr_options = array(
						'sandbox' => false,
						'cmd' => '_xclick',
						'rm' => '2', // 0, 1, 2 ( post method ) 
						'business' => 'leauvuong-facilitator@gmail.com',
						'currency_code' => 'USD',
						'return' => '',
						'cancel_return' => '',
						'arr_cmd' => array(
												'item_name' => 'Entry Vietnam Apply Visa',
												'item_number' => 'VS-621254',
												'amount' => '215',
												'quantity' => '1'
											),
						// type shopting card
						//'arr_cmd' => array(
//												array(
//													'item_name_1' => 'Product name 1',
//													'item_number_1' => 'sku_1',
//													'amount_1' => '215',
//													'quantity_1' => '1'
//												),
//												array(
//													'item_name_2' => 'Product name 2',
//													'item_number_2' => 'sku_2',
//													'amount_2' => '215',
//													'quantity_2' => '1'
//												),
//											),
						'arr_customer' => array(
												'first_name' => '',
												'last_name' => '',
												'country' => '',
												'email' => '',
												'address1' => '',
												'address2' => ''
											),
						
					);*/
class W366_Paypal{
	private $sandbox_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
	private $paypal_url = 'https://www.paypal.com/cgi-bin/webscr';
	const encrypt_salt = 'cvdsa9';
	const encrypt_num = '3';
	
	function __construct($arr_options) {
		$this->setOptions($arr_options);
   	}
	
	public function setOptions($new){
		$this->arr_options = $new;
	}
	
	public function getOptions(){
		return $this->arr_options;
	}
	
	public function isSandbox(){
		$options = $this->getOptions();
		return $options['sandbox'];
	}
	
	public function createCustomerHidden(){
		$options = $this->getOptions();
		$customer_options = $options['arr_customer'];
		$s = '';
		if($customer_options){
			foreach($customer_options as $name=>$val){
				if($val){
					$s .= '<input type="hidden" name="'.$name.'" value="'.$val.'">';	
				}
			}
		}
		return $s;
	}
	
	public function createItemHidden(){
		$options = $this->getOptions();
		$arr_cmd = $options['arr_cmd'];
		if(!empty($arr_cmd)){
			if($arr_cmd[0]){
				foreach($arr_cmd as $arr_item){
					if(is_array($arr_item)){
						foreach($arr_item as $name=>$val){
							$s .= '<input type="hidden" name="'.$name.'" value="'.$val.'">';		
						}
					}	
				}
			}else{
				foreach($arr_cmd as $name=>$val){
					$s .= '<input type="hidden" name="'.$name.'" value="'.$val.'">';		
				}
			}
		}
		return $s;
	}
	
	
	public function createEncryptTransaction(){
		$options = $this->getOptions();
		$arr_transaction = array();
		$arr_transaction['cart_total'] = $options['cart_total'];
		$arr_transaction['currency_code'] = $options['currency_code'];
		$arr_transaction['business'] = $options['business'];
		$arr_transaction['date'] = date("Y-m-d");
		$arr_transaction['time'] = time();
		$arr_transaction['sandbox'] = ($this->isSandbox()) ? 1 : 0;
		if($options['arr_transaction_extra']){
			foreach($options['arr_transaction_extra'] as $k=>$v){
				$arr_transaction[$k] = $v;
			}
		}
		$s_transaction = serialize($arr_transaction);
		$s_encrypt_transaction  = self::encrypt($s_transaction, self::encrypt_salt, self::encrypt_num);
		return $s_encrypt_transaction;
	}
	
	//Note: max len = 1024
	public function createReturnUrl(){
		$options = $this->getOptions();
		$return = ($options['return']) ? $options['return'] : get_site_url();
		$return = $return . '?transaction='.urlencode($this->createEncryptTransaction());
		return $return;
	}
	
	static function parseArrTransaction($s_encrypted_transaction){ 
		$s_decrypted_transaction = self::decrypt($s_encrypted_transaction, self::encrypt_salt, self::encrypt_num);
		return unserialize($s_decrypted_transaction);
	}
	
	static function encrypt($string, $salt, $iNum = 2) {
		$output = false;
		// initialization vector 
		$iv = md5($salt);
		for($i = 1; $i <= $iNum; $i++){
			$iv = md5($iv);
		}
		//$iv = md5(md5($salt));
		$output = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($salt), $string, MCRYPT_MODE_CBC, $iv);
		$output = base64_encode($output);
		return $output;
	}
	
	static function decrypt($encrypted, $salt, $iNum = 2) {
		$output = false;
		// initialization vector
		$iv = md5($salt);
		for($i = 1; $i <= $iNum; $i++){
			$iv = md5($iv);
		}
		//$iv = md5(md5($salt));
		$output = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($salt), base64_decode($encrypted), MCRYPT_MODE_CBC, $iv);
		$output = rtrim($output, "");
		return $output;
	}
	
	
	
	public function createForm(){
		$options = $this->getOptions(); 
		$url = ($this->isSandbox()) ? $this->sandbox_url : $this->paypal_url;
		$rm = ($options['rm']) ? $options['rm'] : '2';
		$cmd = ($options['cmd']) ? $options['cmd'] : '_xclick';
		$business = ($options['business']) ? $options['business'] : ''; 
		$currency_code = ($options['currency_code']) ? $options['currency_code'] : 'USD';
		$return = ($options['return']) ? $options['return'] : get_site_url();
		$cancel_return = ($options['cancel_return']) ? $options['cancel_return'] : get_site_url();
	?>
        <form action="<?php echo $url; ?>" method="post">
        	<input type="hidden" name="no_shipping" value="1">
        	<input type="hidden" name="charset" value="utf-8">
        	<input type="hidden" name="rm" value="<?php echo $rm; ?>">
            <input type="hidden" name="cmd" value="<?php echo $cmd; ?>">
            <input type="hidden" name="business" value="<?php echo $business; ?>">
            <input type="hidden" name="currency_code" value="<?php echo $currency_code; ?>">
            <input type="hidden" name="return" value="<?php echo $this->createReturnUrl(); ?>">
            <input type="hidden" name="cancel_return" value="<?php echo $cancel_return; ?>">
  
            <?php echo $this->createItemHidden(); ?>
            <?php echo $this->createCustomerHidden(); ?>
            
            <!-- Button -->
            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
        </form>
    <?php
	}
	
//End Class---------------------------------------------
}