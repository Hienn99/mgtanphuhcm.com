<?php
class w366_news extends WP_Widget {
    function __construct() {
		parent::__construct(
			'w366_news', esc_html_x('[w366] FAQs & News', 'widget name', 'website366com'),
			array(
				'classname' => 'w366_news',
				'description' => esc_html__('FAQs & News', 'website366com'),
				'customize_selective_refresh' => true
			)
		);
	}
    function widget($args, $instance) {
	    $defaults = array('title' => '');
		$title = $instance['title']; 
		$left_page_id = $instance['wpage1'];
		$right_page_id = $instance['wpage2'];
		
		echo $args['before_widget'];
		
		//-------------------------------
		$args = array(
				'post_type' => 'wb366_faqs',
				'posts_per_page' => 8,
				'order'   => 'ASC',
			);
		$the_faqs = new WP_Query( $args );
		?>
           <div class="w366_news">
           		<div class="container">
                    <?php 
					if($title):
					?>
                	<h2 class="text-center home-heading rm-martop renew">
                    	<span class="bg-2"><?php echo $title; ?></span>
                   	</h2>
                    <?php 
					endif;
					?>
                    
                	<div class="row">
                    	<div class="col-md-6 col-sm-12">
                        	<div class="fl100 border-style bg-w">
                            	<div class="faqs-listing faqs-cluster-bk">
                                    <h2><span class="under-line">Visa FAQs <?php if($left_page_id): ?></span><a href="<?php echo get_permalink($left_page_id); ?>" class="btn btn-danger right">Read More</a><?php endif; ?></h2>
                                    <ul>
                                    <?php 
                                        $args = array(
                                                'post_type' => 'wb366_faqs',
                                                'posts_per_page' => -1,
                                                'order'   => 'ASC',
                                            );
                                        $the_faqs = new WP_Query( $args );
                                    
                                        if ( $the_faqs->have_posts() ) :
                                                while ( $the_faqs->have_posts() ) : $the_faqs->the_post(); 
                                                ?>
                                                    <li>
                                                        <h2><a class="collapsed" rel="nofollow" title="" data-toggle="collapse" href="#for_collapse_<?php the_ID();?>" aria-expanded="false" aria-controls="collapse230"><?php the_title(); ?></a></h2>
                                                        <div class="collapse" id="for_collapse_<?php the_ID();?>">
                                                            <?php the_content(); ?>
                                                            <div style="text-align:right;">
                                                                <a class="waves-effect waves-light btn btn-danger" href="<?php the_permalink(); ?>">View</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php
                                                endwhile; 
                                                wp_reset_postdata(); 
                                        endif; 
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                        	<div class="fl100 border-style bg-w">
                                <div class="news-cluster">
                                    <h2><span class="under-line">Vietnam Visa Blog</span> <?php if($right_page_id): ?><a href="<?php echo get_permalink($right_page_id); ?>" class="btn btn-danger right">Read More</a><?php endif; ?></h2>
                                    <?php 
                                    $args = array(
                                            'post_type' => 'post',
                                            'posts_per_page' => 2,
                                            'order'   => 'desc',
                                        );
                                    $the_news = new WP_Query( $args );
                                    if ( $the_news->have_posts() ) :
                                        echo '<div class="list-news-out">';
                                            while ( $the_news->have_posts() ) : $the_news->the_post(); 
                                            ?>
                                            <div class="row clearfix item">
                                                <div class="col-xs-4 col-sm-5 clearfix">
                                                    <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
                                                        <?php the_post_thumbnail( 'thumb_360x260' ); ?>
                                                    </a>
                                                </div>
                                                <div class="col-xs-8 col-sm-7">
                                                    <h4 class="heading"><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                                    <p class="content"><?php echo wp_trim_words( get_the_content(), 25, '...' ); ?></p>
                                                    <!--<a title="Read more" href="<?php the_permalink(); ?>" class="waves-effect waves-light btn btn-danger">Read more</a>-->
                                                </div>
                                            </div>	
                                            <?php
                                            endwhile; 
                                            echo '</div>';
                                            wp_reset_postdata(); 
                                    endif; 
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
        <?php
        echo $args['after_widget'];
    }
    function update($new_instance, $old_instance) {
        $instance = array();
		if (!empty($new_instance['title'])) {
			$instance['title'] = sanitize_text_field($new_instance['title']);
		}
		
		$instance['wpage1'] = (!empty($new_instance['wpage1'])) ? $new_instance['wpage1'] : 0;
		$instance['wpage2'] = (!empty($new_instance['wpage2'])) ? $new_instance['wpage2'] : 0;
		
        return $instance;
    }
    function form($instance) {
		$arr_page = get_pages();
		
	    $defaults = array('title' => '');
	   
        $instance = wp_parse_args($instance, $defaults); ?>
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title', 'website366com'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['title']); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" id="<?php echo esc_attr($this->get_field_id('title')); ?>" />
        	<small><?php _e('Vd: Title', 'website366com')?></small>
        </p>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('wpage1')); ?>"><?php esc_html_e('Left link', 'website366com'); ?></label>
            <select id="<?php echo esc_attr($this->get_field_id('wpage1')); ?>" class="widefat" name="<?php echo esc_attr($this->get_field_name('wpage1')); ?>">
            	<?php 
				foreach($arr_page as $p){
				?>
                <option value="<?php echo $p->ID; ?>" <?php selected($p->ID, $instance['wpage1']); ?>> <?php echo $p->post_title; ?></option>
                <?php
				}
				?>
            </select>
            <small><?php esc_html_e('Left link', 'website366com'); ?></small>
		</p>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('wpage2')); ?>"><?php esc_html_e('Right link', 'website366com'); ?></label>
            <select id="<?php echo esc_attr($this->get_field_id('wpage2')); ?>" class="widefat" name="<?php echo esc_attr($this->get_field_name('wpage2')); ?>">
            	<?php 
				foreach($arr_page as $p){
				?>
                <option value="<?php echo $p->ID; ?>" <?php selected($p->ID, $instance['wpage2']); ?>> <?php echo $p->post_title; ?></option>
                <?php
				}
				?>
            </select>
            <small><?php esc_html_e('Right link', 'website366com'); ?></small>
		</p>
   	<?php
    }
}

?>