<?php
function w366InitWidgets(){
	$arr_init = array(	
						'w366_news'
						//'add_new_here', //file exist : add_new_here.php
					);
	return $arr_init;
}

function web33_register_widgets(){
	$arr_init = w366InitWidgets();
	
	if($arr_init):
		foreach($arr_init as $file_name){
			$file = realpath(dirname(__FILE__)).'/'.$file_name.'.php';
			if( file_exists($file) ){
				require_once($file);
				register_widget($file_name);
			}
		}
	endif;	//end if arr_init
}
add_action('widgets_init', 'web33_register_widgets');

?>