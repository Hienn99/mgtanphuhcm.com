<?php
function checkReduxActive(){
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if ( is_plugin_active( 'redux-framework/redux-framework.php' ) ) {
		return true;
	} 
	return false;
}

function isInstalledPlugin($str_folder_plugin){
	$plugin_dir = ABSPATH . 'wp-content/plugins/'.$str_folder_plugin;
	if(file_exists($plugin_dir))
	 	return true;
	
	return false;
}

$prefix_field = 'pre_gecko_';
define('PREFIX_TEXT_DOMAIN', 'twentysixteen' );
define('PREFIX_FIELD', $prefix_field );
define('THE_FOLDER_OPTIONS', 'theme-options');
define('ROOT_THEME_OPTIONS', realpath(dirname(__FILE__)) );
define('OPTIONS_PATH', ROOT_THEME_OPTIONS.'/options' );
define('TABS_PATH', OPTIONS_PATH.'/tabs' );

if(checkReduxActive()):
	/*if(!checkReduxActive())
		require(ROOT_THEME_OPTIONS .'/ReduxCore/framework.php');*/
	
	require(OPTIONS_PATH .'/functions.php'); 
	require(OPTIONS_PATH .'/presets.php');
else:
	if(!isInstalledPlugin('redux-framework')){
		require(ROOT_THEME_OPTIONS .'/libs/class-tgm-plugin-activation.php');
		require(ROOT_THEME_OPTIONS .'/libs/reqiure-plugins.php');
	}
	
	function default_redux_register_my_custom_menu_page(){
		add_menu_page( 
			__( 'Custom Menu Title', PREFIX_TEXT_DOMAIN ),
			'Options Redux',
			'manage_options',
			'options_redux', //slug
			'options_redux_func'
		); 
	}
	add_action( 'admin_menu', 'default_redux_register_my_custom_menu_page' );
	function options_redux_func(){
		if(!isInstalledPlugin('redux-framework')){
			
			echo '<h1>Redux Framework chưa được cài.</h1>';
			echo '<a href="'.get_site_url().'/wp-admin/themes.php?page=tp_plugin_install">Cài plugin "Redux Framework"</a>';
		}else{
			echo '<h1>Redux Framework chưa được active.</h1>';
			echo '<a href="'.get_site_url().'/wp-admin/plugins.php?plugin_status=all">Tìm và active plugin "Redux Framework"</a>';
		}
	}
	
endif;

//need prefix: PREFIX_FIELD
function get_option_data($name){
	global $smof_data; //'opt_name'           => 'smof_data' (in file 'functions.php' )
	return $smof_data[$name];
}

//No prefix: PREFIX_FIELD
function get_data_name($name){
	global $smof_data; //'opt_name'           => 'smof_data' (in file 'functions.php' )
	return $smof_data[PREFIX_FIELD.$name];
}
	
/*
$header_layout =  get_option_data('header_layout');
echo $header_layout;exit;*/
