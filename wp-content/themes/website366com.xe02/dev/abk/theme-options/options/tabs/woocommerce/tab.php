<?php
if (defined('PREFIX_FIELD')) {
    $pre_tab = PREFIX_FIELD;
}

$page_options = get_list_page_options();

/*@import parent tab*/
$this->sections[] = array(
    'title' => esc_html__('Sản phẩm', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-shopping-cart', //http://elusiveicons.com/icons/
	'heading' => 'Cấu hình sản phẩm',
    'desc'    => 'Cấu hình thông số liên quan đến sản phẩm',
    'fields' => array(
        //woocommerce per page
		array(
			'title' => esc_html__('Số sản phẩm', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Số sản phẩm hiển thị trên 1 trang', PREFIX_TEXT_DOMAIN),
            'id' => $pre_tab.'wooc_per_page',
            'type' => 'select',
            'options' => array(
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '13' => '13',
                '14' => '14',
                '15' => '15',
				'16' => '16',
				'17' => '17',
				'18' => '18',
				'19' => '19',
				'20' => '20'
            ),
            'default' => '8'
        ),
		
		//woocommerce per page related
		array(
			'title' => esc_html__('Số sản phẩm liên quan', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Số sản phẩm liên quan đến sản phẩm đang xem', PREFIX_TEXT_DOMAIN),
            'id' => $pre_tab.'wooc_related_per_page',
            'type' => 'select',
            'options' => array(
				'1' => '1',
				'2' => '2',
				'3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10'
            ),
            'default' => '4'
        ),
		
		//shiiping page
		array(
			'title' => esc_html__('Trang giao hàng', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Chọn trang chi tiết giao hàng', PREFIX_TEXT_DOMAIN),
            'id' => $pre_tab.'wooc_shipping_return',
            'type' => 'select',
            'options' => $page_options,
            'default' => '256'
        ),
    )
);


/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);
	
function get_list_page_options(){
	$pages = get_pages();
	$arr_page[0] = __('Chọn', PREFIX_TEXT_DOMAIN);
	if($pages):
		foreach ( $pages as $page ) {
			$arr_page[$page->ID] = $page->post_title;
		}
	endif;
	return $arr_page;
}