<?php
/*@import parent tab*/

if (defined('PREFIX_FIELD')) {
    $pre_tab = PREFIX_FIELD;
}


$arr_fields = array(
	//Company Name
	array(
		'id' => $pre_tab.'comany_name',
		'title' => esc_html__('Tên công ty', PREFIX_TEXT_DOMAIN),
		'subtitle' => esc_html__('Eg: Cong ty TNHH Demo', PREFIX_TEXT_DOMAIN),
		'type' => 'text',
		'default' => '',
		'placeholder' => 'Nhập tên công ty'
	),
	
	//Company address
	array(
		'id' => $pre_tab.'comany_address',
		'title' => esc_html__('Địa chỉ công ty', PREFIX_TEXT_DOMAIN),
		'subtitle' => esc_html__('Địa chỉ', PREFIX_TEXT_DOMAIN),
		'type' => 'text',
		'default' => '',
		'placeholder' => 'Nhập địa chỉ công ty'
	),
	
	//Phone
	array(
		'id' => $pre_tab.'comany_phone',
		'title' => esc_html__('Điện thoại', PREFIX_TEXT_DOMAIN),
		'subtitle' => esc_html__('Eg: 0909 888 999', PREFIX_TEXT_DOMAIN),
		'type' => 'text',
		'default' => '',
		'placeholder' => 'Nhập số điện thoại'
	),
	
	//Email
	array(
		'id' => $pre_tab.'comany_email',
		'title' => esc_html__('Email', PREFIX_TEXT_DOMAIN),
		'subtitle' => esc_html__('Eg: contact@gmail.com', PREFIX_TEXT_DOMAIN),
		'type' => 'text',
		'default' => '',
		'placeholder' => 'Nhập email liên lạc'
	),
	
	//Footer logo
	array(
		'title' => esc_html__('Footer logo', PREFIX_TEXT_DOMAIN),
		'subtitle' => esc_html__('Chọn logo', PREFIX_TEXT_DOMAIN),
		'id' => $pre_tab.'footer_logo',
		'type' => 'media',
		'url' => true,
		'default' => array(
			'url'=>get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/logo.png'
		)
	),
	
	//Copy Right at Footer
	array(
		'id' => $pre_tab.'copy_right_footer',
		'type'             => 'editor',
		'title'            => __('@copy right dưới footer', PREFIX_TEXT_DOMAIN), 
		'subtitle'         => __('@copy right dưới footer', PREFIX_TEXT_DOMAIN),
		'default'          => 'Copyright 2016 - Công ty Cổ phần Vật liệu và Keo Viên Thanh.Thiết kế website bởi Website366.com',
		'args'   => array(
			'teeny'            => true,
			'textarea_rows'    => 5
		)
	),
);
	

$this->sections[] = array(
    'title' => esc_html__('Cấu hình', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-cogs',
	'heading' => 'Cấu hình website',
    'fields' => $arr_fields
);

/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);