<?php
/**
 * Optimal Core
 * 
 * Optimal options for theme. optimal speed
 * @author Fox
 */
$this->sections[] = array(
    'title' => esc_html__('Optimal Core', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-idea',
    'fields' => array(
        array(
            'subtitle' => esc_html__('no minimize , generate css over time...', PREFIX_TEXT_DOMAIN),
            'id' => 'dev_mode',
            'type' => 'switch',
            'title' => esc_html__('Dev Mode (not recommended)', PREFIX_TEXT_DOMAIN),
            'default' => false
        ),
        array(
            'subtitle' => esc_html__('count view for single post.', PREFIX_TEXT_DOMAIN),
            'id' => 'post_view',
            'type' => 'switch',
            'title' => esc_html__('Count Views', PREFIX_TEXT_DOMAIN),
            'default' => false
        )
    )
);

/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);