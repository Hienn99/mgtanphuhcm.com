<?php
/*@import parent tab*/
/**
 * Body
 *
 * @author Fox
 */
$this->sections[] = array(
    'title' => esc_html__('Body', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-website',
    'fields' => array(
        array(
            'subtitle' => esc_html__('Set body width layout.', PREFIX_TEXT_DOMAIN),
            'id' => 'body_width',
            'type' => 'text',
            'title' => esc_html__('Boxed Layout', PREFIX_TEXT_DOMAIN),
            'default' => '1170px',
        ),
        array(
            'id'       => 'body_background',
            'type'     => 'background',
            'title'    => esc_html__( 'Background', PREFIX_TEXT_DOMAIN ),
            'subtitle' => esc_html__( 'body background with image, color, etc.', PREFIX_TEXT_DOMAIN ),
            'output'   => array('body'),
        ),
        array(
            'subtitle' => esc_html__('in pixels, top right bottom left, ex: 10px 10px 10px 10px', PREFIX_TEXT_DOMAIN),
            'id' => 'body_margin',
            'type' => 'text',
            'title' => 'Margin',
            'default' => ''
        ),
        array(
            'subtitle' => esc_html__('in pixels, top right bottom left, ex: 10px 10px 10px 10px', PREFIX_TEXT_DOMAIN),
            'id' => 'body_padding',
            'type' => 'text',
            'title' => 'Padding',
            'default' => '',
        )
    )
);

/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);