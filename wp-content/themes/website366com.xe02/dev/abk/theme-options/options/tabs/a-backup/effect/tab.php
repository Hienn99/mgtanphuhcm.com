<?php
/*@import parent tab*/
/**
 * Effect
 */
$this->sections[] = array(
    'title' => esc_html__('Effect element', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-credit-card',
);

/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);