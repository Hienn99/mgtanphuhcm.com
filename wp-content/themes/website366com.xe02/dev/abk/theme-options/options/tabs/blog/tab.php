<?php
/*@import parent tab*/

if (defined('PREFIX_FIELD')) {
    $pre_tab = PREFIX_FIELD;
}


$arr_fields = array(
	//Per Page Post On The Top
	array(
		'title' => esc_html__('Số tin tức hiển thị trên Top', PREFIX_TEXT_DOMAIN),
		'subtitle' => esc_html__('Số tin tức hiển thị trên Top của trang danh sách', PREFIX_TEXT_DOMAIN),
		'id' => $pre_tab.'blog_per_page_top',
		'type' => 'select',
		'options' => array(
			'0' => 'Không hiển thị',
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10'
		),
		'default' => '4'
	),
	
	//Show tag?
	array(
		'title' => esc_html__('Hiển thị thẻ (tag) liên quan?', PREFIX_TEXT_DOMAIN),
		'subtitle' => esc_html__('Hiển thị thẻ (tag) liên quan', PREFIX_TEXT_DOMAIN),
		'id' => $pre_tab.'blog_show_tag',
		'type' => 'switch',
		'default' => true,
	),
	
	//Show tag?
	array(
		'title' => esc_html__('Có chia sẻ bài viết qua MXH?', PREFIX_TEXT_DOMAIN),
		'subtitle' => esc_html__('Chia sẻ bài viết qua mạng xả hội (Facebook, google ...)', PREFIX_TEXT_DOMAIN),
		'id' => $pre_tab.'blog_show_share_social',
		'type' => 'switch',
		'default' => true,
	),
	
	//Number Related Blog
	array(
		'title' => esc_html__('Số tin tức liên quan', PREFIX_TEXT_DOMAIN),
		'subtitle' => esc_html__('Số tin tức liên quan muốn hiển thị', PREFIX_TEXT_DOMAIN),
		'id' => $pre_tab.'blog_per_page_related',
		'type' => 'select',
		'options' => array(
			'0' => 'Không hiển thị',
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10'
		),
		'default' => '4'
	),
	
);
	

$this->sections[] = array(
    'title' => esc_html__('Tin tức', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-th',
	'heading' => 'Cấu hình tin tức',
    'fields' => $arr_fields
);

/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);