<?php
/* Default Button */
$this->sections[] = array(
    'title' => esc_html__('Default Button', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-forward-alt',
    'subsection' => true,
    'fields' => array(
        array(
            'subtitle' => 'Default is 14px',
            'id' => 'button_font_size',
            'type' => 'text',
            'title' => 'Font Size',
            'default' => '14px'
        ),
        array(
            'subtitle' => 'Button Border Style.',
            'id' => 'button_border_style',
            'type' => 'select',
            'options' => array(
                'solid' => 'solid',
                'dotted' => 'dotted',
                'dashed' => 'dashed',
                'none' => 'none',
                'hidden' => 'hidden',
                'double' => 'double',
                'groove' => 'groove',
                'ridge' => 'ridge',
                'inset' => 'inset',
                'outset' => 'outset',
                'initial' => 'initial',
                'inherit' => 'inherit'
            ),
            'title' => 'Border Style',
            'default' => 'solid'
        ),
        array(
            'subtitle' => 'Controls the border color of the buttons.',
            'id' => 'button_border_color',
            'type' => 'color',
            'title' => 'Border Color',
            'default' => '#000000'
        ),
        array(
            'subtitle' => 'Controls the border color hover of the buttons.',
            'id' => 'button_border_color_hover',
            'type' => 'color',
            'title' => 'Border Color Hover',
            'default' => '#ffbf00'
        ),
        array(
            'subtitle' => 'Button Border Width for: Top, Right, Bottom, Left',
            'id' => 'button_border_width',
            'type' => 'text',
            'title' => 'Border Width',
            'default' => '2px'
        ),
        array(
            'subtitle' => 'Border Radius. In pixels ex: 3px',
            'id' => 'button_border_radius',
            'type' => 'text',
            'title' => 'Border Radius',
            'default' => '0px'
        ),
        array(
            'subtitle' => 'Controls the text color of buttons.',
            'id' => 'button_gradient_text_color',
            'type' => 'color',
            'title' => 'Default Text Color',
            'default' => '#000'
        ),
        array(
            'subtitle' => 'Controls the text color hover of buttons.',
            'id' => 'button_gradient_text_color_hover',
            'type' => 'color',
            'title' => 'Default Text Color Hover',
            'default' => ''
        ),
        array(
            'subtitle' => 'Controls the button background color..',
            'id' => 'button_gradient_default_background',
            'type' => 'color',
            'title' => 'Default Background Color',
            'default' => '#fff'
        ),
        array(
            'subtitle' => 'Controls the button background color hover.',
            'id' => 'button_gradient_default_background_hover',
            'type' => 'color',
            'title' => 'Default Background Hover Color',
            'default' => '#ffbf00'
        ),
    )
);
/**
 * Button Primary
 */
$this->sections[] = array(
    'icon' => 'el-icon-forward-alt',
    'title' => esc_html__('Primary Button', PREFIX_TEXT_DOMAIN),
    'subsection' => true,
    'fields' => array(
        array(
            'subtitle' => ' Font Size. Default is: 14px',
            'id' => 'button_primary_font_size',
            'type' => 'text',
            'title' => 'Button Font Size',
            'default' => '14px'
        ),

        array(
            'subtitle' => 'Border Style.',
            'id' => 'button_primary_border_style',
            'type' => 'select',
            'options' => array(
                'solid' => 'solid',
                'dotted' => 'dotted',
                'dashed' => 'dashed',
                'none' => 'none',
                'hidden' => 'hidden',
                'double' => 'double',
                'groove' => 'groove',
                'ridge' => 'ridge',
                'inset' => 'inset',
                'outset' => 'outset',
                'initial' => 'initial',
                'inherit' => 'inherit'
            ),
            'title' => 'Border Style',
            'default' => 'solid'
        ),
        array(
            'subtitle' => 'Controls the border color of the buttons.',
            'id' => 'button_primary_border_color',
            'type' => 'color',
            'title' => 'Border Color',
            'default' => '#ffbf00'
        ),
        array(
            'subtitle' => 'Controls the border color hover of the buttons.',
            'id' => 'button_primary_border_color_hover',
            'type' => 'color',
            'title' => 'Border Color Hover',
            'default' => '#000'
        ),
        array(
            'subtitle' => 'Button Primary Border Width for : Top, Right, Bottom, Left',
            'id' => 'button_primary_border_width',
            'type' => 'text',
            'title' => 'Border Width',
            'default' => '2px',
        ),
        array(
            'subtitle' => 'Ex: 3px',
            'id' => 'button_primary_border_radius',
            'type' => 'text',
            'title' => 'Border Radius',
            'default' => '0px'
        ),
        array(
            'subtitle' => 'Controls the text color of buttons.',
            'id' => 'button_primary_text_color',
            'type' => 'color',
            'title' => 'Text Color',
            'default' => '#000'
        ),
        array(
            'subtitle' => 'Controls the text color hover of buttons.',
            'id' => 'button_primary_text_color_hover',
            'type' => 'color',
            'title' => 'Text Color Hover',
            'default' => ''
        ),
        array(
            'subtitle' => 'Controls the button background color..',
            'id' => 'primary_button_background_color',
            'type' => 'color',
            'title' => 'Background Color',
            'default' => '#ffbf00'
        ),
        array(
            'subtitle' => 'Controls the button background color hover.',
            'id' => 'primary_button_background_color_hover',
            'type' => 'color',
            'title' => 'Background Hover Color',
            'default' => '#fff'
        ),
    )
);