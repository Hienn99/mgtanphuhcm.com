<?php
/*@import parent tab*/
/* Demo */
$pre_tab = 'demo_';

$this->sections[] = array(
    'title' => esc_html__('Fields demo(Ver: '.getReduxVersion().')', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-adjust', //http://elusiveicons.com/icons/
	'heading' => 'All field type demo of ReduxFramework version: '.getReduxVersion(),
    'desc'    => '<br />You can update ReduxFramework version by:<br />
					<ul>
						<li><strong>Step1:</strong> Download new version <a target="_blank" href="https://wordpress.org/plugins/redux-framework/">Download</a></li>
						<li><strong>Step2:</strong> Extract (Call : new1)</li>
						<li><strong>Step3:</strong> Find folder \'ReduxCore\' in \'new1\' and replace for \'inc\theme-options\ReduxCore\' in theme </li>
						<li>Done!!!</li>
					</ul>',
    'fields' => array(
    /*Begin list fields*/ 
	   
		//text
		array(
			'title' => esc_html__('Text field type', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Type: text', PREFIX_TEXT_DOMAIN),
            'id' => $pre_tab.'text_field_type',
            'type' => 'text',
            'default' => ''
        ),
		
		//switch
		array(
			'title' => esc_html__('Switch field type', PREFIX_TEXT_DOMAIN),
			'subtitle' => esc_html__('Type: switch', PREFIX_TEXT_DOMAIN),
			'id' => $pre_tab.'switch_field_type',
			'type' => 'switch',
			'default' => true,
		),
		
		//select
		array(
			'title' => esc_html__('Select field type', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Type: select', PREFIX_TEXT_DOMAIN),
            'id' => $pre_tab.'select_field_type',
            'type' => 'select',
            'options' => array(
                'solid' => 'solid',
                'dotted' => 'dotted',
                'dashed' => 'dashed',
                'none' => 'none',
                'hidden' => 'hidden',
                'double' => 'double',
                'groove' => 'groove',
                'ridge' => 'ridge',
                'inset' => 'inset',
                'outset' => 'outset',
                'initial' => 'initial',
                'inherit' => 'inherit'
            ),
            'default' => 'outset'
        ),
		
		//radio //https://docs.reduxframework.com/core/fields/radio/
		array(
			'id' => $pre_tab.'radio_field_type',
			'type'     => 'radio',
			'title'    => __('Radio Option', PREFIX_TEXT_DOMAIN), 
			'subtitle' => __('No validation can be done on this field type', PREFIX_TEXT_DOMAIN),
			'desc'     => __('This is the description field, again good for additional info.', PREFIX_TEXT_DOMAIN),
			//Must provide key => value pairs for radio options
			'options'  => array(
				'1' => 'Opt 1', 
				'2' => 'Opt 2', 
				'3' => 'Opt 3'
			),
			'default' => '2'
		),
		
		//textarea
		array(
            'id' => $pre_tab.'textarea_field_type',
            'type' => 'textarea',
            'title' => esc_html__('Textarea field type', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Add tag html ID or class (body,a,.class,#id)', PREFIX_TEXT_DOMAIN),
            'validate' => '', //no_html
            'default' => '.cs-highlight-style-3',
        ),
		
		//image_select
		array(
            'id' => $pre_tab.'image_select_field_type',
            'title' => esc_html__('Image Select field type', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Type: image_select', PREFIX_TEXT_DOMAIN),
            'default' => '1',
            'type' => 'image_select',
            'options' => array(
                '1' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/header/header-1.png',
				'2' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/header/header-1.png',
            )
        ),
		
		//media
		array(
            'title' => esc_html__('Media field type', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Type: media', PREFIX_TEXT_DOMAIN),
            'id' => $pre_tab.'media_field_type',
            'type' => 'media',
            'url' => true,
            'default' => array(
				'url'=>get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/logo.png'
            )
        ),
		
		//color
		array(
            'subtitle' => esc_html__('Type: color', PREFIX_TEXT_DOMAIN),
            'id' => $pre_tab.'color_field_type',
            'type' => 'color',
            'title' => esc_html__('Color field type', PREFIX_TEXT_DOMAIN),
            'default' => '#fff'
        ),
		
		//color_rgba
		array(
            'id' => $pre_tab.'color_rgba_field_type',
            'type'      => 'color_rgba',
            'title' => esc_html__('Color Rgba field type', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Type: color_rgba', PREFIX_TEXT_DOMAIN),
            'options'       => array(
                'show_input'                => true,
                'show_initial'              => true,
                'show_alpha'                => true,
                'show_palette'              => true,
                'show_palette_only'         => false,
                'show_selection_palette'    => true,
                'max_palette_size'          => 10,
                'allow_empty'               => true,
                'clickout_fires_change'     => false,
                'choose_text'               => 'Choose',
                'cancel_text'               => 'Cancel',
                'show_buttons'              => true,
                'use_extended_classes'      => true,
                'palette'                   => null,
                'input_text'                => 'Select Color'
            ),
            'default'   => array(
                'color'     => '#fff',
                'alpha'     => 1,
                'rgba'      => 'rgba(0, 0, 0, 0.5)'
            ),
            'output' => array('background-color' => '.wrap-navigation.header-fixed'),
        ),
		
		//color_gradient //https://docs.reduxframework.com/core/fields/color-gradient/
		array(
			'id' => $pre_tab.'color_gradient_field_type',
			'type'     => 'color_gradient',
			'title'    => __('Header Gradient Color Option', PREFIX_TEXT_DOMAIN),
			'subtitle' => __('Only color validation can be done on this field type', PREFIX_TEXT_DOMAIN),
			'desc'     => __('This is the description field, again good for additional info.', PREFIX_TEXT_DOMAIN),
			'validate' => 'color',
			'default'  => array(
				'from' => '#1e73be',
				'to'   => '#00897e', 
			),
		),
		
		//ace_editor
		array(
            'id' => $pre_tab.'ace_editor_field_type',
            'type' => 'ace_editor',
            'title' => esc_html__('Ace editor field type', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Type: ace_editor', PREFIX_TEXT_DOMAIN),
            'mode' => 'css',
            'theme' => 'monokai',
        ),
		
		//typography
		array(
            'id' => $pre_tab.'typography_field_type',
            'type' => 'typography',
            'title' => esc_html__('Typography field type', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Type: typography', PREFIX_TEXT_DOMAIN),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output'  => array('.page-title #page-title-text h1'),
            'units' => 'px',
            'default' => array(
                'color' => '',
            )
        ),
		
		//background
		array(
            'id' => $pre_tab.'background_field_type',
            'type'     => 'background',
            'title' => esc_html__('Background field type', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Type: background', PREFIX_TEXT_DOMAIN),
            'output'   => array('#masthead')
        ),
		
		//border : //https://docs.reduxframework.com/core/fields/border/
		array( 
			'id' => $pre_tab.'border_field_type',
			'type'     => 'border',
			'title' => esc_html__('Border field type', PREFIX_TEXT_DOMAIN),
			'subtitle' => esc_html__('Type: border', PREFIX_TEXT_DOMAIN),
			'output'   => array('.site-header'),
			'desc'     => __('This is the description field, again good for additional info.', PREFIX_TEXT_DOMAIN),
			'default'  => array(
				'border-color'  => '#1e73be', 
				'border-style'  => 'solid', 
				'border-top'    => '3px', 
				'border-right'  => '3px', 
				'border-bottom' => '3px', 
				'border-left'   => '3px'
			)
		),
		
		//button_set (single) //https://docs.reduxframework.com/core/fields/button_set/
		array(
			'id' => $pre_tab.'button_set_field_type',
			'type'     => 'button_set',
			'title'    => __('Button Set (Single)', PREFIX_TEXT_DOMAIN),
			'subtitle' => esc_html__('Type: button_set (single)', PREFIX_TEXT_DOMAIN),
			'desc'     => __('This is the description field, again good for additional info.', PREFIX_TEXT_DOMAIN),
			//Must provide key => value pairs for options
			'options' => array(
				'1' => 'Opt 1', 
				'2' => 'Opt 2', 
				'3' => 'Opt 3'
			 ), 
			'default' => '2'
		),
		
		//button_set (multiple) //https://docs.reduxframework.com/core/fields/button_set/
		array(
			'id' => $pre_tab.'button_set_multi_field_type',
			'type'     => 'button_set',
			'title'    => __('Button Set (Multi)', PREFIX_TEXT_DOMAIN),
			'subtitle' => esc_html__('Type: button_set (multiple)', PREFIX_TEXT_DOMAIN),
			'desc'     => __('This is the description field, again good for additional info.', PREFIX_TEXT_DOMAIN),
			'multi'    => true,
			//Must provide key => value pairs for options
			'options' => array(
				'1' => 'Opt 1', 
				'2' => 'Opt 2', 
				'3' => 'Opt 3'
			 ), 
			'default' => array('2', '3'),
		),
		
		//checkbox //https://docs.reduxframework.com/core/fields/checkbox/
		array(
			'id' => $pre_tab.'checkbox_field_type',
			'type'     => 'checkbox',
			'title'    => __('Checkbox', PREFIX_TEXT_DOMAIN), 
			'subtitle' => esc_html__('Type: checkbox', PREFIX_TEXT_DOMAIN),
			'desc'     => __('This is the description field, again good for additional info.', PREFIX_TEXT_DOMAIN),
		 
			//Must provide key => value pairs for multi checkbox options
			'options'  => array(
				'1' => 'Opt 1',
				'2' => 'Opt 2',
				'3' => 'Opt 3'
			),
		 
			//See how default has changed? you also don't need to specify opts that are 0.
			'default' => array(
				'1' => '1', 
				'2' => '0', 
				'3' => '0'
			)
		),
		
		//date //https://docs.reduxframework.com/core/fields/date/
		array(
			'id' => $pre_tab.'date_field_type',
			'type'        => 'date',
			'title'       => __('Date Option', PREFIX_TEXT_DOMAIN), 
			'subtitle'    => __('No validation can be done on this field type', PREFIX_TEXT_DOMAIN),
			'desc'        => __('This is the description field, again good for additional info.', PREFIX_TEXT_DOMAIN),
			'placeholder' => 'Click to enter a date'
		),
		
		//dimensions //https://docs.reduxframework.com/core/fields/dimensions/
		array(
			'id' => $pre_tab.'dimensions_field_type',
			'type'     => 'dimensions',
			'units'    => array('em','px','%'),
			'title'    => __('Dimensions (Width/Height) Option', PREFIX_TEXT_DOMAIN),
			'subtitle' => __('Allow your users to choose width, height, and/or unit.', PREFIX_TEXT_DOMAIN),
			'desc'     => __('Enable or disable any piece of this field. Width, Height, or Units.', PREFIX_TEXT_DOMAIN),
			'default'  => array(
				'Width'   => '200', 
				'Height'  => '100'
			),
		),
		
		//divide //https://docs.reduxframework.com/core/fields/divide/
		array(
			'id' => $pre_tab.'divide_field_type',
			'title' => esc_html__('Divide field type', PREFIX_TEXT_DOMAIN),
			'subtitle' => esc_html__('Type: divide', PREFIX_TEXT_DOMAIN),
			'desc' => __('This is the description field, again good for additional info.', PREFIX_TEXT_DOMAIN),
			'type' => 'divide'
		),
		
		//editor //https://docs.reduxframework.com/core/fields/editor/
		array(
			'id' => $pre_tab.'editor_text_field_type',
			'type'             => 'editor',
			'title'            => __('Editor Text', PREFIX_TEXT_DOMAIN), 
			'subtitle'         => __('Subtitle text would go here.', PREFIX_TEXT_DOMAIN),
			'default'          => 'Powered by Redux.',
			'args'   => array(
				'teeny'            => true,
				'textarea_rows'    => 10
			)
		),
		
		//gallery //https://docs.reduxframework.com/core/fields/gallery/
		array(
			'id' => $pre_tab.'gallery_field_type',
			'type'     => 'gallery',
			'title'    => __('Add/Edit Gallery', PREFIX_TEXT_DOMAIN),
			'subtitle' => __('Create a new Gallery by selecting existing or uploading new images using the WordPress native uploader', PREFIX_TEXT_DOMAIN),
			'desc'     => __('This is the description field, again good for additional info.', PREFIX_TEXT_DOMAIN),
		),
		
		//info //https://docs.reduxframework.com/core/fields/info/
		array(
			'id' => $pre_tab.'info_normal_field_type',
			'type' => 'info',
			'desc' => __('This is the info field, if you want to break sections up.', PREFIX_TEXT_DOMAIN)
		),
		array(
			'id' => $pre_tab.'info_warning_field_type',
			'type'  => 'info',
			'title' => __('Danger, Will Robinson!', PREFIX_TEXT_DOMAIN),
			'style' => 'warning',
			'desc'  => __('This is an info field with the warning style applied and a header.', PREFIX_TEXT_DOMAIN)
		),
		array(
			'id' => $pre_tab.'info_success_field_type',
			'type'  => 'info',
			'style' => 'success',
			'title' => __('Success!', PREFIX_TEXT_DOMAIN),
			'icon'  => 'el-icon-info-sign',
			'desc'  => __( 'This is an info field with the success style applied, a header and an icon.', PREFIX_TEXT_DOMAIN)
		),
		array(
			'id' => $pre_tab.'info_critical_field_type',
			'type' => 'info',
			'style' => 'critical',
			'icon' => 'el-icon-info-sign',
			'title' => __('This is a title.', PREFIX_TEXT_DOMAIN),
			'desc' => __('This is an info field with the critical style applied, a header and an icon.', PREFIX_TEXT_DOMAIN)
		),
		
		//link_color //https://docs.reduxframework.com/core/fields/link-color/
		array(
			'id' => $pre_tab.'link_color_field_type',
			'type'     => 'link_color',
			'title'    => __('Links Color Option', PREFIX_TEXT_DOMAIN),
			'subtitle' => __('Only color validation can be done on this field type', PREFIX_TEXT_DOMAIN),
			'desc'     => __('This is the description field, again good for additional info.', PREFIX_TEXT_DOMAIN),
			'default'  => array(
				'regular'  => '#1e73be', // blue
				'hover'    => '#dd3333', // red
				'active'   => '#8224e3',  // purple
				'visited'  => '#8224e3',  // purple
			)
		),
		
		//multi_text //https://docs.reduxframework.com/core/fields/multi_text/
		array(
			'id' => $pre_tab.'multi_text_field_type',
			'type' => 'multi_text',
			'title' => __('Multi Text Option - Color Validated', PREFIX_TEXT_DOMAIN),
			'validate' => 'html', //https://docs.reduxframework.com/core/the-basics/validation/
			'subtitle' => __('If you enter an invalid color it will be removed. Try using the text "blue" as a color.<a href="https://docs.reduxframework.com/core/the-basics/validation/" target="_blank">Validation</a>', PREFIX_TEXT_DOMAIN),
			'desc' => __('This is the description field, again good for additional info.', PREFIX_TEXT_DOMAIN)
		),
		
		//password //https://docs.reduxframework.com/core/fields/password/
		array(
			'id' => $pre_tab.'password_field_type',
			'type'        => 'password',
			'username'    => true,
			'title'       => __('SMTP Account', PREFIX_TEXT_DOMAIN),
			'placeholder' => array(	'username'   => __('Enter your Username', PREFIX_TEXT_DOMAIN), 
									'password'   => __('Enter your Password', PREFIX_TEXT_DOMAIN))
		),
		
		//raw //https://docs.reduxframework.com/core/fields/raw/
		array( 
			'id' => $pre_tab.'raw_field_type',
			'type'     => 'raw',
			'title'    => __('Raw output', PREFIX_TEXT_DOMAIN),
			'subtitle' => __('Subtitle text goes here.', PREFIX_TEXT_DOMAIN),
			'desc'     => __('This is the description field for additional info.', PREFIX_TEXT_DOMAIN),
			//'content'  => file_get_contents(dirname(__FILE__) . '/myfile.txt')
			'content'  => 'This is content'
		),
		
		//section //https://docs.reduxframework.com/core/fields/section/
		array(
		   'id' => $pre_tab.'section1_start_field_type',
		   'type' => 'section',
		   'title' => __('Indented Options', PREFIX_TEXT_DOMAIN),
		   'subtitle' => __('With the "section" field you can create indent option sections.', PREFIX_TEXT_DOMAIN),
		   'indent' => true 
	   ),
	   array(
			'id' => $pre_tab.'section1_end_field_type',
			'type'   => 'section',
			'indent' => false,
		),
		
		//select_image //https://docs.reduxframework.com/core/fields/select-image/
		array( 
			'id' => $pre_tab.'select_image_field_type',
			'type'     => 'select_image',
			'title'    => __('Select Image', PREFIX_TEXT_DOMAIN),
			'subtitle' => __('A preview of the selected image will appear underneath the select box.', PREFIX_TEXT_DOMAIN),
			'desc'     => __('This is the description field, again good for additional info.', PREFIX_TEXT_DOMAIN),
			'options'  => Array(
				Array (
					 'alt'  => 'Image Name 1',
					 'img'  => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/header/header-1.png',
				),
				Array (
					 'alt'  => 'Image Name 2',
					 'img'  => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/header/header-2.png',
				)
			),
			'default'  => 'Image Name 1',
		),
		
		//slider //https://docs.reduxframework.com/core/fields/slider/
		array(
			'id' => $pre_tab.'slider_1_field_type',
			'type'      => 'slider',
			'title'     => __('Slider Example 1', PREFIX_TEXT_DOMAIN),
			'subtitle'  => __('This slider displays the value as a label.', PREFIX_TEXT_DOMAIN),
			'desc'      => __('Slider description. Min: 1, max: 500, step: 1, default value: 250', PREFIX_TEXT_DOMAIN),
			"default"   => 250,
			"min"       => 1,
			"step"      => 1,
			"max"       => 500,
			'display_value' => 'label'
		),
		array(
			'id' => $pre_tab.'slider_2_field_type',
			'type' => 'slider',
			'title' => __('Slider Example 2 with Steps (5)', PREFIX_TEXT_DOMAIN),
			'subtitle' => __('This example displays the value in a text box', PREFIX_TEXT_DOMAIN),
			'desc' => __('Slider description. Min: 0, max: 300, step: 5, default value: 75', PREFIX_TEXT_DOMAIN),
			"default" => 75,
			"min" => 0,
			"step" => 5,
			"max" => 300,
			'display_value' => 'text'
		),
		array(
			'id' => $pre_tab.'slider_3_field_type',
			'type' => 'slider',
			'title' => __('Slider Example 3 with two sliders', PREFIX_TEXT_DOMAIN),
			'subtitle' => __('This example displays the values in select boxes', PREFIX_TEXT_DOMAIN),
			'desc' => __('Slider description. Min: 0, max: 500, step: 5, slider 1 default value: 100, slider 2 default value: 300', PREFIX_TEXT_DOMAIN),
			"default" => array(
				1 => 100,
				2 => 300,
			),
			"min" => 0,
			"step" => 5,
			"max" => "500",
			'display_value' => 'select',
			'handles' => 2,
		 
		), 
		array(
			'id' => $pre_tab.'slider_4_field_type',
			'type' => 'slider',
			'title' => __('Slider Example 4 with float values', PREFIX_TEXT_DOMAIN),
			'subtitle' => __('This example displays float values', PREFIX_TEXT_DOMAIN),
			'desc' => __('Slider description. Min: 0, max: 1, step: .1, default value: .5', PREFIX_TEXT_DOMAIN),
			"default" => .5,
			"min" => 0,
			"step" => .1,
			"max" => 1,
			'resolution' => 0.1,
			'display_value' => 'text'
		), //slider end
		
		//slides //https://docs.reduxframework.com/core/fields/slides/
		array(
			'id' => $pre_tab.'slides_field_type',
			'type'        => 'slides',
			'title'       => __('Slides Options', PREFIX_TEXT_DOMAIN),
			'subtitle'    => __('Unlimited slides with drag and drop sortings.', PREFIX_TEXT_DOMAIN),
			'desc'        => __('This field will store all slides values into a multidimensional array to use into a foreach loop.', PREFIX_TEXT_DOMAIN),
			'placeholder' => array( 
				'title'           => __('This is a title', PREFIX_TEXT_DOMAIN),
				'description'     => __('Description Here', PREFIX_TEXT_DOMAIN),
				'url'             => __('Give us a link!', PREFIX_TEXT_DOMAIN),
			),
		),
		
		//sortable =>'mode'     => 'checkbox' //https://docs.reduxframework.com/core/fields/sortable/
		array(
			'id' => $pre_tab.'sortable_checkbox_field_type',
			'type'     => 'sortable',
			'title'    => __('Sortable Text Option', PREFIX_TEXT_DOMAIN),
			'subtitle' => __('Define and reorder these however you want.', PREFIX_TEXT_DOMAIN),
			'desc'     => __('This is the description field, again good for additional info.', PREFIX_TEXT_DOMAIN),
			'mode'     => 'checkbox',
			'options'  => array(
				'1'     => 'Item 1',
				'2'     => 'Item 2',
				'3'     => 'Item 3',
			),
			// For checkbox mode
			'default' => array(
				'1' => false,
				'2' => true,
				'3' => false
			),
		),
		//sortable =>'mode'     => 'text'
		array(
			'id' => $pre_tab.'sortable_text_field_type',
			'type'     => 'sortable',
			'title'    => __('Sortable Text Option', PREFIX_TEXT_DOMAIN),
			'subtitle' => __('Define and reorder these however you want.', PREFIX_TEXT_DOMAIN),
			'desc'     => __('This is the description field, again good for additional info.', PREFIX_TEXT_DOMAIN),
			'mode'     => 'text',
			'options' => array(
				 '1' => 'Item number one',
				 '2' => 'Number two here',
				 '3' => 'Three strikes, yer out!',
			),
		),
		
		//sorter //https://docs.reduxframework.com/core/fields/sorter/
		array(
			'id' => $pre_tab.'sorter_field_type',
			'type'    => 'sorter',
			'title'   => __('Homepage Layout Manager', PREFIX_TEXT_DOMAIN),
			'desc'    => __('Organize how you want the layout to appear on the homepage', PREFIX_TEXT_DOMAIN),
			'options' => array(
				'enabled'  => array(
					'highlights' => __('Highlights', PREFIX_TEXT_DOMAIN),
					'slider'     => __('Slider', PREFIX_TEXT_DOMAIN),
					'staticpage' => __('Static Page', PREFIX_TEXT_DOMAIN),
					'services'   => __('Services', PREFIX_TEXT_DOMAIN)
				),
				'disabled' => array(
				)
			),
		),
		
		//spacing //https://docs.reduxframework.com/core/fields/spacing/
		array(
			'id' => $pre_tab.'spacing_field_type',
			'type'           => 'spacing',
			'output'         => array('.site-header'),
			'mode'           => 'margin', //absolute, padding or margin.
			'units'          => array('em', 'px'),
			'units_extended' => 'false',
			'title'          => __('Padding/Margin Option', PREFIX_TEXT_DOMAIN),
			'subtitle'       => __('Allow your users to choose the spacing or margin they want.', PREFIX_TEXT_DOMAIN),
			'desc'           => __('You can enable or disable any piece of this field. Top, Right, Bottom, Left, or Units.', PREFIX_TEXT_DOMAIN),
			'default'            => array(
				'margin-top'     => '1px', 
				'margin-right'   => '2px', 
				'margin-bottom'  => '3px', 
				'margin-left'    => '4px',
				'units'          => 'em', 
			)
		),
		
		//spinner //https://docs.reduxframework.com/core/fields/spinner/
		array(
			'id' => $pre_tab.'spinner_field_type',
			'type'     => 'spinner', 
			'title'    => __('JQuery UI Spinner Example 1', PREFIX_TEXT_DOMAIN),
			'subtitle' => __('No validation can be done on this field type',PREFIX_TEXT_DOMAIN),
			'desc'     => __('JQuery UI spinner description. Min:20, max: 100, step:20, default value: 40', PREFIX_TEXT_DOMAIN),
			'default'  => '40',
			'min'      => '20',
			'step'     => '20',
			'max'      => '100',
		),
					
	/*End list fields*/ 	
    )
);

/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);