<?php
/**
 * Styling
 * 
 * css color.
 * @author Fox
 */
$this->sections[] = array(
    'title' => esc_html__('Styling', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-adjust',
    'fields' => array(
        array(
            'subtitle' => esc_html__('set color main color.', PREFIX_TEXT_DOMAIN),
            'id' => 'primary_color',
            'type' => 'color',
            'title' => esc_html__('Primary Color', PREFIX_TEXT_DOMAIN),
            'default' => '#ffbf00'
        ),
        array(
        'id' => 'secondary_color',
        'type' => 'color',
        'title' => esc_html__('Secondary Color', PREFIX_TEXT_DOMAIN),
        'default' => '#010101'
        ),
        array(
            'subtitle' => esc_html__('set color for tags <a></a>.', PREFIX_TEXT_DOMAIN),
            'id' => 'link_color',
            'type' => 'color',
            'title' => esc_html__('Link Color', PREFIX_TEXT_DOMAIN),
            'output'  => array('a'),
            'default' => '#000000'
        )
    )
);

/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);