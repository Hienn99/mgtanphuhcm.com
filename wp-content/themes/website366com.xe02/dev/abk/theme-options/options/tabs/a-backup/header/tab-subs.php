<?php

/* Logo */
$this->sections[] = array(
    'title' => esc_html__('Logo', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-picture',
    'subsection' => true,
    'fields' => array(
        array(
            'title' => esc_html__('Logo', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Select an image file for your logo.', PREFIX_TEXT_DOMAIN),
            'id' => 'main_logo',
            'type' => 'media',
            'url' => true,
            'default' => array(
				'url'=>get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/logo.png'
            )
        ),
        array(
            'subtitle' => esc_html__('in pixels.', PREFIX_TEXT_DOMAIN),
            'id' => 'main_logo_height',
            'type' => 'text',
            'title' => 'Height',
            'default' => ''
        ),
        array(
            'title' => esc_html__('Logo sticky', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Select an image file for your logo sticky.', PREFIX_TEXT_DOMAIN),
            'id' => 'logo_sticky',
            'type' => 'media',
            'url' => true,
            'default' => array(
                'url'=>get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/logo_sticky.png'
            )
        ),
        array(
            'subtitle' => esc_html__('in pixels.', PREFIX_TEXT_DOMAIN),
            'id' => 'logo_sticky_height',
            'type' => 'text',
            'title' => 'Height logo sticky',
            'default' => ''
        ),
    )
);

/* Menu */
$this->sections[] = array(
    'title' => esc_html__('Menu', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-tasks',
    'subsection' => true,
    'fields' => array(

        array(
            'subtitle' => esc_html__('enable sticky mode for menu.', PREFIX_TEXT_DOMAIN),
            'id' => 'menu_sticky',
            'type' => 'switch',
            'title' => esc_html__('Sticky Menu', PREFIX_TEXT_DOMAIN),
            'default' => false,
        ),
        array(
            'subtitle' => esc_html__('in pixels ex: 60px', PREFIX_TEXT_DOMAIN),
            'id' => 'menu_height',
            'type' => 'text',
            'title' => 'Menu height',
            'default' => '88px'
        ),
        array(
            'subtitle' => esc_html__('Background color in menu', PREFIX_TEXT_DOMAIN),
            'id' => 'menu_background_color',
            'type' => 'color',
            'title' => 'Menu background color',
            'default' => '#fff'
        ),
        array(
            'subtitle' => esc_html__('in pixels ex: 13px', PREFIX_TEXT_DOMAIN),
            'id' => 'menu_font_size',
            'type' => 'text',
            'title' => 'Font size',
            'default' => '15px'
        ),
        array(
            'subtitle' => esc_html__('in pixels ex: 0 5px 0 5px', PREFIX_TEXT_DOMAIN),
            'id' => 'menu_item_space',
            'type' => 'text',
            'title' => 'Menu item space',
            'default' => '0 20px 0 0'
        ),
        array(
            'subtitle' => esc_html__('Uppercase text', PREFIX_TEXT_DOMAIN),
            'id' => 'menu_text_uppercase',
            'type' => 'switch',
            'title' => 'Text uppercase',
            'default' => 'true'
        ),
        array(
            'subtitle' => esc_html__('Change text color', PREFIX_TEXT_DOMAIN),
            'id' => 'menu_text_color',
            'type' => 'color',
            'title' => 'Text color',
            'default' => '#000000'
        ),
        array(
            'subtitle' => esc_html__('Change text color hover', PREFIX_TEXT_DOMAIN),
            'id' => 'menu_text_color_hover',
            'type' => 'color',
            'title' => 'Text color hover',
            'default' => ''
        ),
        array(
            'subtitle' => esc_html__('in pixels ex: 200px', PREFIX_TEXT_DOMAIN),
            'id' => 'sub_menu_width',
            'type' => 'text',
            'title' => 'Sub menu width',
            'default' => '250px'
        ),
        array(
            'id'        => 'background_color_sub_menu',
            'type'      => 'color_rgba',
            'title'     => esc_html__('Sub menu background color',PREFIX_TEXT_DOMAIN),
            'subtitle'  => esc_html__('Change background color sub menu',PREFIX_TEXT_DOMAIN),
            'options'       => array(
                'show_input'                => true,
                'show_initial'              => true,
                'show_alpha'                => true,
                'show_palette'              => true,
                'show_palette_only'         => false,
                'show_selection_palette'    => true,
                'max_palette_size'          => 10,
                'allow_empty'               => true,
                'clickout_fires_change'     => false,
                'choose_text'               => 'Choose',
                'cancel_text'               => 'Cancel',
                'show_buttons'              => true,
                'use_extended_classes'      => true,
                'palette'                   => null,
                'input_text'                => 'Select Color'
            ),
            'default'   => array(
                'color'     => '#fff',
                'alpha'     => 1,
                'rgba'      => 'rgba(255, 255, 255, 0.95)'
            ),
        ),
        array(
            'subtitle' => esc_html__('in pixels ex: 13px', PREFIX_TEXT_DOMAIN),
            'id' => 'sub_menu_font_size',
            'type' => 'text',
            'title' => 'Sub menu font size',
            'default' => '15px'
        ),
        array(
            'subtitle' => esc_html__('Change sub menu text color', PREFIX_TEXT_DOMAIN),
            'id' => 'sub_menu_text_color',
            'type' => 'color',
            'title' => 'Sub menu text color',
            'default' => '#000'
        ),
        array(
            'subtitle' => esc_html__('Change sub menu text color hover', PREFIX_TEXT_DOMAIN),
            'id' => 'sub_menu_text_color_hover',
            'type' => 'color',
            'title' => 'Sub menu text color hover',
            'default' => '#FFBF00'
        ),
        array(
            'subtitle' => esc_html__('Sub menu uppercase text', PREFIX_TEXT_DOMAIN),
            'id' => 'sub_menu_text_uppercase',
            'type' => 'switch',
            'title' => 'Sub menu text uppercase',
            'default' => 'false'
        ),
        array(
            'subtitle' => esc_html__('enable sticky mode for menu Tablets.', PREFIX_TEXT_DOMAIN),
            'id' => 'menu_sticky_tablets',
            'type' => 'switch',
            'title' => esc_html__('Sticky Tablets', PREFIX_TEXT_DOMAIN),
            'default' => false,
            'required' => array( 0 => 'menu_sticky', 1 => '=', 2 => 1 )
        ),
        array(
            'subtitle' => esc_html__('enable sticky mode for menu Mobile.', PREFIX_TEXT_DOMAIN),
            'id' => 'menu_sticky_mobile',
            'type' => 'switch',
            'title' => esc_html__('Sticky Mobile', PREFIX_TEXT_DOMAIN),
            'default' => false,
            'required' => array( 0 => 'menu_sticky', 1 => '=', 2 => 1 )
        ),
        array(
            'subtitle' => esc_html__('Change color menu sticky', PREFIX_TEXT_DOMAIN),
            'id' => 'color_menu_sticky',
            'type' => 'color',
            'title' => 'Color menu sticky',
            'default' => '#fff',
            'output' => array('color' => '#cshero-header .wrap-navigation.header-fixed .main-navigation .nav-menu > li > a'),
        ),
        array(
            'id'        => 'background_sticky',
            'type'      => 'color_rgba',
            'title'     => esc_html__('Background sticky',PREFIX_TEXT_DOMAIN),
            'subtitle'  => esc_html__('Set color background sticky',PREFIX_TEXT_DOMAIN),
            'options'       => array(
                'show_input'                => true,
                'show_initial'              => true,
                'show_alpha'                => true,
                'show_palette'              => true,
                'show_palette_only'         => false,
                'show_selection_palette'    => true,
                'max_palette_size'          => 10,
                'allow_empty'               => true,
                'clickout_fires_change'     => false,
                'choose_text'               => 'Choose',
                'cancel_text'               => 'Cancel',
                'show_buttons'              => true,
                'use_extended_classes'      => true,
                'palette'                   => null,
                'input_text'                => 'Select Color'
            ),
            'default'   => array(
                'color'     => '#fff',
                'alpha'     => 1,
                'rgba'      => 'rgba(0, 0, 0, 0.5)'
            ),
            'output' => array('background-color' => '.wrap-navigation.header-fixed'),
        ),
        array(
            'id'        => 'background_menu_canvas',
            'type'      => 'color_rgba',
            'title'     => esc_html__('Background menu canvas',PREFIX_TEXT_DOMAIN),
            'subtitle'  => esc_html__('Set color background menu canvas',PREFIX_TEXT_DOMAIN),
            'options'       => array(
                'show_input'                => true,
                'show_initial'              => true,
                'show_alpha'                => true,
                'show_palette'              => true,
                'show_palette_only'         => false,
                'show_selection_palette'    => true,
                'max_palette_size'          => 10,
                'allow_empty'               => true,
                'clickout_fires_change'     => false,
                'choose_text'               => 'Choose',
                'cancel_text'               => 'Cancel',
                'show_buttons'              => true,
                'use_extended_classes'      => true,
                'palette'                   => null,
                'input_text'                => 'Select Color'
            ),
            'default'   => array(
                'color'     => '#000',
                'alpha'     => 1,
                'rgba'      => 'rgba(0, 0, 0, 0.9)'
            ),
            'output' => array('background-color' => '.menu-canvas .main-canvas'),
        ),
        array(
            'subtitle' => esc_html__('Change color menu canvas', PREFIX_TEXT_DOMAIN),
            'id' => 'color_menu_canvas',
            'type' => 'color',
            'title' => 'Color menu canvas',
            'default' => '#fff',
            'output' => array('color' => '.menu-canvas .main-canvas a'),
        ),
    )
);
