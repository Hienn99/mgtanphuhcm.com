<?php
/**
 * Custom CSS
 * 
 * extra css for customer.
 * @author Fox
 */
$this->sections[] = array(
    'title' => esc_html__('Custom CSS', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-bulb',
    'fields' => array(
        array(
            'id' => 'custom_css',
            'type' => 'ace_editor',
            'title' => esc_html__('CSS Code', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('create your css code here.', PREFIX_TEXT_DOMAIN),
            'mode' => 'css',
            'theme' => 'monokai',
        )
    )
);

/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);