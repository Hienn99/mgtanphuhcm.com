<?php
/**
 * Animations
 *
 * Animations options for theme. libs css, js.
 * @author Fox
 */
$this->sections[] = array(
    'title' => esc_html__('Animations', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-magic',
    'fields' => array(
        array(
            'subtitle' => esc_html__('Enable animation parallax for images...', PREFIX_TEXT_DOMAIN),
            'id' => 'paralax',
            'type' => 'switch',
            'title' => esc_html__('Images Paralax', PREFIX_TEXT_DOMAIN),
            'default' => true
        ),
    )
);

/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);