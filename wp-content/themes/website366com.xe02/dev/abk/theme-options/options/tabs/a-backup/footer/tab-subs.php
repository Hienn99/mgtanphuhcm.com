<?php
/* Footer top */
$this->sections[] = array(
    'title' => esc_html__('Top', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-fork',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'footer_background',
            'type'     => 'background',
            'title'    => esc_html__( 'Background', PREFIX_TEXT_DOMAIN ),
            'subtitle' => esc_html__( 'footer background with image, color, etc.', PREFIX_TEXT_DOMAIN ),
            'output'   => array('footer.footer-content'),
            'default'   => array(
                'background-color' => '#111111',
                'background-image'=>'',
                'background size' => 'cover',
                'background-repeat' => 'no-repeat',
                'background-position' =>'top center',
                'background-attachment' =>'scroll'
            )
        ),
        array(
            'subtitle' => 'Footer top color',
            'id' => 'footer_top_color',
            'type' => 'color',
            'title' => 'Footer top color',
            'default' => '#fff',
            'output'   => array('footer .widget'),
        ),
        array(
            'subtitle' => esc_html__('in pixels, top right bottom left, ex: 10px 10px 10px 10px', PREFIX_TEXT_DOMAIN),
            'id' => 'footer_margin',
            'type' => 'text',
            'title' => 'Margin',
            'default' => ''
        ),
        array(
            'subtitle' => esc_html__('in pixels, top right bottom left, ex: 10px 10px 10px 10px', PREFIX_TEXT_DOMAIN),
            'id' => 'footer_padding',
            'type' => 'text',
            'title' => 'Padding',
            'default' => '0px'
        ),
        array(
            'subtitle' => esc_html__('Size class boostrap', PREFIX_TEXT_DOMAIN),
            'id' => 'footer_top_left_class',
            'type' => 'text',
            'title' => 'Footer top left size',
            'default' => 'col-xs-12 col-sm-4'
        ),
        array(
            'subtitle' => esc_html__('Size class boostrap', PREFIX_TEXT_DOMAIN),
            'id' => 'footer_top_center_class',
            'type' => 'text',
            'title' => 'Footer top center size',
            'default' => 'col-xs-12 col-sm-4'
        ),
        array(
            'subtitle' => esc_html__('Size class boostrap', PREFIX_TEXT_DOMAIN),
            'id' => 'footer_top_right_class',
            'type' => 'text',
            'title' => 'Footer top right size',
            'default' => 'col-xs-12 col-sm-4'
        )
    )
);

/* footer bottom */
$this->sections[] = array(
    'title' => esc_html__('Bottom', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-bookmark',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'footer_bottom_background',
            'type'     => 'background',
            'title'    => esc_html__( 'Background', PREFIX_TEXT_DOMAIN ),
            'subtitle' => esc_html__( 'background with image, color, etc.', PREFIX_TEXT_DOMAIN ),
            'output'   => array('footer #footer-bottom'),
            'default'   => array()
        ),
        array(
            'subtitle' => esc_html__('in pixels, top right bottom left, ex: 10px 10px 10px 10px', PREFIX_TEXT_DOMAIN),
            'id' => 'footer_bottom_margin',
            'type' => 'text',
            'title' => 'Margin',
            'default' => ''
        ),
        array(
            'subtitle' => esc_html__('in pixels, top right bottom left, ex: 10px 10px 10px 10px', PREFIX_TEXT_DOMAIN),
            'id' => 'footer_bottom_padding',
            'type' => 'text',
            'title' => 'Padding',
            'default' => ''
        ),
        array(
            'subtitle' => esc_html__('enable button back to top.', PREFIX_TEXT_DOMAIN),
            'id' => 'footer_bottom_back_to_top',
            'type' => 'switch',
            'title' => esc_html__('Back To Top', PREFIX_TEXT_DOMAIN),
            'default' => true,
        )
    )
);