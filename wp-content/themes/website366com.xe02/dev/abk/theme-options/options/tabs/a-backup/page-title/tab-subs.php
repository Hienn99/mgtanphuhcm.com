<?php

/* Page Title */
$this->sections[] = array(
    'icon' => 'el-icon-podcast',
    'title' => esc_html__('Page Title', PREFIX_TEXT_DOMAIN),
    'subsection' => true,
    'fields' => array(
        array(
            'id' => 'page_title_typography',
            'type' => 'typography',
            'title' => esc_html__('Typography', PREFIX_TEXT_DOMAIN),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output'  => array('.page-title #page-title-text h1'),
            'units' => 'px',
            'subtitle' => esc_html__('Typography option with title text.', PREFIX_TEXT_DOMAIN),
            'default' => array(
                'color' => '',
            )
        ),
    )
);


/* Breadcrumb */
$this->sections[] = array(
    'icon' => 'el-icon-random',
    'title' => esc_html__('Breadcrumb', PREFIX_TEXT_DOMAIN),
    'subsection' => true,
    'fields' => array(
        array(
            'subtitle' => 'Show Breadcrumb',
            'id' => 'show_breadcrumb',
            'type' => 'switch',
            'title' => 'Show Breadcrumb',
            'default' => false
        ),
        array(
            'subtitle' => esc_html__('The text before the breadcrumb home.', PREFIX_TEXT_DOMAIN),
            'id' => 'breacrumb_home_prefix',
            'type' => 'text',
            'title' => esc_html__('Breadcrumb Home Prefix', PREFIX_TEXT_DOMAIN),
            'default' => 'You are here:  Home'
        ),
        array(
            'id' => 'breacrumb_typography',
            'type' => 'typography',
            'title' => esc_html__('Typography', PREFIX_TEXT_DOMAIN),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output'  => array('.page-title #breadcrumb-text','.page-title #breadcrumb-text ul li a'),
            'units' => 'px',
            'subtitle' => esc_html__('Typography option with title text.', PREFIX_TEXT_DOMAIN),
            'default' => array(
                'color' => '',
            )
        ),
    )
);