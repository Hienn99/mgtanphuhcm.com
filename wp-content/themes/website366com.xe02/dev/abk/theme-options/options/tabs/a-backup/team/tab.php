<?php
/*@import parent tab*/
/* Team */
$this->sections[] = array(
    'title' => esc_html__('Teams', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-adjust',
    'fields' => array(
        array(
            'id' => 'team_title_layout',
            'title' => esc_html__('Single Page Title', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('select a layout for single team page title', PREFIX_TEXT_DOMAIN),
            'default' => '1',
            'type' => 'image_select',
            'options' => array(
                '' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/pagetitle/pt-s-0.png',
                '1' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/pagetitle/pt-s-1.png',
                '2' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/pagetitle/pt-s-2.png',
                '3' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/pagetitle/pt-s-3.png',
                '4' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/pagetitle/pt-s-4.png',
                '5' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/pagetitle/pt-s-5.png',
                '6' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/pagetitle/pt-s-6.png',
            )
        )
    )
);

/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);