<?php
/* Page */
$this->sections[] = array(
    'icon' => 'el el-pencil',
    'title' => esc_html__('Page', PREFIX_TEXT_DOMAIN),
    'subsection' => true,
    'fields' => array(
        array(
            'subtitle' => 'Show comment on page',
            'id' => 'show_comment_page',
            'type' => 'switch',
            'title' => 'Show comment',
            'default' => true
        ),
        array(
            'subtitle' => 'Page full width',
            'id' => 'page_full_width',
            'type' => 'switch',
            'title' => 'Page full width',
            'default' => true
        ),
    )
);

/* Post */
$this->sections[] = array(
    'icon' => 'el el-pencil',
    'title' => esc_html__('Post', PREFIX_TEXT_DOMAIN),
    'subsection' => true,
    'fields' => array(
        array(
            'title' => esc_html__('Logo post', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Select an image file for your logo post.', PREFIX_TEXT_DOMAIN),
            'id' => 'logo_post',
            'type' => 'media',
            'url' => true,
            'default' => array(
				'url'=>get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/logo-post.png'
            )
        ),
        array(
            'subtitle' => 'Navigation',
            'id' => 'show_navigaiton',
            'type' => 'switch',
            'title' => 'Show navigation',
            'default' => true
        ),
        array(
            'subtitle' => 'Comment',
            'id' => 'show_comment',
            'type' => 'switch',
            'title' => 'Show comment',
            'default' => true
        ),
    )
);