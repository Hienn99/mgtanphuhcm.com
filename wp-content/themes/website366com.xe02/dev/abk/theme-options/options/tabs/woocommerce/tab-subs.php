<?php
if (defined('PREFIX_FIELD')) {
    $pre_tab = PREFIX_FIELD;
}

function getListSocialShare(){
	//Config list social share
	$arr_social = array(
					'Facebook',
					'Google Plus', 
					'Twitter', 
					'Pinterest',
					'Tumblr'
					);
	return $arr_social;
}

$arr_social = getListSocialShare();

if($arr_social){
	$arr_options = array();
	foreach($arr_social as $soc){
		$idsoc = strtolower( str_replace(' ','_', $soc ) );
		$arr_options[$idsoc] = $soc;
	}
}else{
	$arr_options = array(
						'facebook' => 'Facebook',
						'google_plus' => 'Google Plus',
						'twitter' => 'Twitter'
						);
}

$this->sections[] = array(
    'title' => esc_html__('Chia sẻ MXH', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-network',
    'subsection' => true,
    'fields' => array(
        array(
			'id' => $pre_tab.'share_social_network',
			'type'     => 'checkbox',
			'title'    => __('Chia sẻ lên MXH', PREFIX_TEXT_DOMAIN), 
			'subtitle' => esc_html__('Chia sẻ sản phẩm lên mạng xã hội', PREFIX_TEXT_DOMAIN),
		 
			//Must provide key => value pairs for multi checkbox options
			'options'  => $arr_options,
		 
			//See how default has changed? you also don't need to specify opts that are 0.
			'default' => array(
				'facebook' => '0', 
				'google_plus' => '0', 
				'twitter' => '0'
			)
		),
    )
);