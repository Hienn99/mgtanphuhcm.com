<?php
/*@import parent tab*/
/**
 * Post & Page
 */
$this->sections[] = array(
    'title' => esc_html__('Post & Page', PREFIX_TEXT_DOMAIN),
    'icon' => 'el el-file',
);

/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);