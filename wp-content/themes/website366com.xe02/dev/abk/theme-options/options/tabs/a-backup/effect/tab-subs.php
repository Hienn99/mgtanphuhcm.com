<?php
/**
 * Sroll Effects
 **/
$this->sections[] = array(
    'icon' => 'el-icon-forward-alt',
    'title' => esc_html__('Scroll Effects Element', PREFIX_TEXT_DOMAIN),
    'subsection' => true,
    'fields' => array(
        array(
            'subtitle' => 'Make the scroll effects',
            'id' => 'scrolleffect',
            'type' => 'switch',
            'title' => 'Scroll Effect Element',
            'default' => false
        ),
    )
);
/**
 * Effect button click
**/
$this->sections[] = array(
    'icon' => 'el-icon-forward-alt',
    'title' => esc_html__('Button effect click', PREFIX_TEXT_DOMAIN),
    'subsection' => true,
    'fields' => array(
        array(
            'subtitle' => 'Make the button effect click',
            'id' => 'enable_button_effect',
            'type' => 'switch',
            'title' => 'Button Effect Click',
            'default' => false
        ),
    )
);
/**
 * Text Input Effects
 **/
$this->sections[] = array(
    'icon' => 'el-icon-forward-alt',
    'title' => esc_html__('Text Input Effects', PREFIX_TEXT_DOMAIN),
    'subsection' => true,
    'fields' => array(
        array(
            'subtitle' => 'Make the text input effects',
            'id' => 'enable_text_input_effects',
            'type' => 'switch',
            'title' => 'Text Effect Click',
            'default' => false
        ),
    )
);