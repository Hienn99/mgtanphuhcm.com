<?php
/*@import parent tab*/
/**
 * Header Options
 * 
 * @author Fox
 */
$this->sections[] = array(
    'title' => esc_html__('Header', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-credit-card',
    'fields' => array(
        array(
            'id' => 'header_layout',
            'title' => esc_html__('Layouts', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('select a layout for header', PREFIX_TEXT_DOMAIN),
            'default' => '1',
            'type' => 'image_select',
            'options' => array(
                '1' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/header/header-1.png',
				'2' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/header/header-1.png',
            )
        ),
        array(
            'subtitle' => esc_html__('Enable top header.', PREFIX_TEXT_DOMAIN),
            'id' => 'enable_header_top',
            'type' => 'switch',
            'title' => esc_html__('Top header', PREFIX_TEXT_DOMAIN),
            'default' => false,
        ),
        array(
            'id'       => 'header_background',
            'type'     => 'background',
            'title'    => esc_html__( 'Background', PREFIX_TEXT_DOMAIN ),
            'subtitle' => esc_html__( 'header background with image, color, etc.', PREFIX_TEXT_DOMAIN ),
            'output'   => array('#masthead')
        ),
        array(
            'subtitle' => esc_html__('in pixels, top right bottom left, ex: 10px 10px 10px 10px', PREFIX_TEXT_DOMAIN),
            'id' => 'header_margin',
            'type' => 'text',
            'title' => 'Margin',
            'default' => ''
        ),
        array(
            'subtitle' => esc_html__('in pixels, top right bottom left, ex: 10px 10px 10px 10px', PREFIX_TEXT_DOMAIN),
            'id' => 'header_padding',
            'type' => 'text',
            'title' => 'Padding',
            'default' => ''
        ),
    )
);

/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);