<?php
/*@import parent tab*/
/**
 * Page Title
 *
 * @author Fox
 */
$this->sections[] = array(
    'title' => esc_html__('Page Title & BC', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-map-marker',
    'fields' => array(
        array(
            'id' => 'page_title_layout',
            'title' => esc_html__('Layouts', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('select a layout for page title', PREFIX_TEXT_DOMAIN),
            'default' => '1',
            'type' => 'image_select',
            'options' => array(
                '0' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/pagetitle/pt-s-0.png',
                '1' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/pagetitle/pt-s-1.png',
                '2' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/pagetitle/pt-s-2.png',
                '3' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/pagetitle/pt-s-3.png',
                '4' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/pagetitle/pt-s-4.png',
                '5' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/pagetitle/pt-s-5.png',
                '6' => get_template_directory_uri().'/inc/'.THE_FOLDER_OPTIONS.'/options/images/pagetitle/pt-s-6.png',
            )
        ),
        array(
            'id'       => 'page_title_background',
            'type'     => 'background',
            'title'    => esc_html__( 'Background', PREFIX_TEXT_DOMAIN ),
            'subtitle' => esc_html__( 'page title background with image, color, etc.', PREFIX_TEXT_DOMAIN ),
            'output'   => array('.page-title'),
            'default'   => array(
                'background-color'=>'',
                'background-image'=>'',
                'background-repeat'=>'',
                'background-size'=>'',
                'background-attachment'=>'',
                'background-position'=>''
            )
        ),
        array(
            'id'        => 'page_title_overlay',
            'type'      => 'color_rgba',
            'title'     => esc_html__('Page title background overlay',PREFIX_TEXT_DOMAIN),
            'subtitle'  => esc_html__('Change page title background overlay',PREFIX_TEXT_DOMAIN),
            'options'       => array(
                'show_input'                => true,
                'show_initial'              => true,
                'show_alpha'                => true,
                'show_palette'              => true,
                'show_palette_only'         => false,
                'show_selection_palette'    => true,
                'max_palette_size'          => 10,
                'allow_empty'               => true,
                'clickout_fires_change'     => false,
                'choose_text'               => 'Choose',
                'cancel_text'               => 'Cancel',
                'show_buttons'              => true,
                'use_extended_classes'      => true,
                'palette'                   => null,
                'input_text'                => 'Select Color'
            ),
            'default'   => array(
                'color'     => '#FFBF00',
                'alpha'     => 0.7,
                'rgba'      => 'rgba(255,191,0,0.6)'
            ),
        ),
        array(
            'id'        => 'single_pagle_title_overlay',
            'type'      => 'color_rgba',
            'title'     => esc_html__('Single page title background overlay',PREFIX_TEXT_DOMAIN),
            'subtitle'  => esc_html__('Change single page title background overlay',PREFIX_TEXT_DOMAIN),
            'options'       => array(
                'show_input'                => true,
                'show_initial'              => true,
                'show_alpha'                => true,
                'show_palette'              => true,
                'show_palette_only'         => false,
                'show_selection_palette'    => true,
                'max_palette_size'          => 10,
                'allow_empty'               => true,
                'clickout_fires_change'     => false,
                'choose_text'               => 'Choose',
                'cancel_text'               => 'Cancel',
                'show_buttons'              => true,
                'use_extended_classes'      => true,
                'palette'                   => null,
                'input_text'                => 'Select Color'
            ),
            'default'   => array(
                'color'     => '#000',
                'alpha'     => 0.5,
                'rgba'      => 'rgba(255,191,0,0.6)'
            ),
        ),
        array(
            'subtitle' => 'Single pagle title color',
            'id' => 'single_pagle_title_color',
            'type' => 'color',
            'title' => 'Page title single Color',
            'default' => '#fff'
        ),
        array(
            'subtitle' => 'Single pagle title bottom line color',
            'id' => 'single_title_bottom_line_color',
            'type' => 'color',
            'title' => 'Single page title bottom line color',
            'default' => '#FFBF00'
        ),
        array(
            'subtitle' => esc_html__('in pixels', PREFIX_TEXT_DOMAIN),
            'id' => 'page_title_height',
            'type' => 'text',
            'title' => 'Height',
            'default' => '290px'
        ),
        array(
            'subtitle' => esc_html__('in pixels', PREFIX_TEXT_DOMAIN),
            'id' => 'page_title_margin',
            'type' => 'text',
            'title' => 'Margin',
            'default' => ''
        ),
        array(
            'subtitle' => esc_html__('in pixels', PREFIX_TEXT_DOMAIN),
            'id' => 'page_title_padding',
            'type' => 'text',
            'title' => 'Padding',
            'default' => ''
        )
    )
);

/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);