<?php
/*@import parent tab*/

if (defined('PREFIX_FIELD')) {
    $pre_tab = PREFIX_FIELD;
}

function getListSocial(){
	//Config list social
	$arr_social = array(
					'Facebook',
					'Google Plus', 
					'Twitter', 
					'LinkedIn', 
					'YouTube',
					'Instagram',
					'Pinterest',
					'Flickr',
					'Tumblr'
					);
	return $arr_social;
}

$arr_social = getListSocial();


if($arr_social){
	$arr_fields = array();
	foreach($arr_social as $soc){
		$idsoc = strtolower( str_replace(' ','_', $soc ) );
		
		$arr_it = array(
					'id' => $pre_tab.'social_'.$idsoc,
					'title' => esc_html__($soc, PREFIX_TEXT_DOMAIN),
					'subtitle' => esc_html__($soc.' link', PREFIX_TEXT_DOMAIN),
					'type' => 'text',
					'default' => '',
					'placeholder' => 'Enter your '.$soc.' link here'
				);
		$arr_fields[] = $arr_it;
	}
}else{
	$arr_fields = array(
		//Facebook
        array(
			'id' => $pre_tab.'social_facebook',
			'title' => esc_html__('Facebook', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Facebook link', PREFIX_TEXT_DOMAIN),
            'type' => 'text',
            'default' => ''
        ),
		
		//Google Plus
        array(
			'id' => $pre_tab.'social_google_plus',
			'title' => esc_html__('Google Plus', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Google Plus link', PREFIX_TEXT_DOMAIN),
            'type' => 'text',
            'default' => ''
        ),
		
		//Twitter
        array(
			'id' => $pre_tab.'social_twitter',
			'title' => esc_html__('Twitter', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('Twitter link', PREFIX_TEXT_DOMAIN),
            'type' => 'text',
            'default' => ''
        ),
		
		//LinkedIn
        array(
			'id' => $pre_tab.'social_linkedin',
			'title' => esc_html__('LinkedIn', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('LinkedIn link', PREFIX_TEXT_DOMAIN),
            'type' => 'text',
            'default' => ''
        ),
		
		//YouTube
        array(
			'id' => $pre_tab.'social_youtube',
			'title' => esc_html__('YouTube', PREFIX_TEXT_DOMAIN),
            'subtitle' => esc_html__('YouTube link', PREFIX_TEXT_DOMAIN),
            'type' => 'text',
            'default' => ''
        ),
    );
}


$this->sections[] = array(
    'title' => esc_html__('Mạng xã hội', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-network',
    'fields' => $arr_fields
);

/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);