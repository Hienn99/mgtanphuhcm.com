<?php
/*@import parent tab*/
/**
 * Button Options
 */
$this->sections[] = array(
    'title' => esc_html__('Button Options', PREFIX_TEXT_DOMAIN),
    'icon' => 'el-icon-play',
    'fields' => array(

        array(
            'subtitle' => 'Make the text in button is uppercase or not',
            'id' => 'button_uppercase',
            'type' => 'switch',
            'title' => 'Button Text Uppercase',
            'default' => true
        ),
        array(
            'subtitle' => 'Button font weight (300,400,600,700)',
            'id' => 'button_font_weight',
            'type' => 'text',
            'title' => 'Button font-weight',
            'default' => '600'
        ),
        array(
            'subtitle' => 'In pixels, ex: 10px 10px 10px 10px',
            'id' => 'button_padding',
            'type' => 'text',
            'title' => 'Button Padding',
            'default' => '11px 25px'
        ),
        array(
            'subtitle' => 'In pixels, top left botton right, ex: 10px 10px 10px 10px',
            'id' => 'button_margin',
            'type' => 'text',
            'title' => 'Button Margin',
            'default' => '0'
        )
    )
);
/*@import list sub tabs children*/
$sub_path = realpath(dirname(__FILE__)).'/tab-subs.php';
if( file_exists( $sub_path ) )
	require($sub_path);