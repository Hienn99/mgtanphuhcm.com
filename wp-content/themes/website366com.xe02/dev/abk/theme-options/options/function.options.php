<?php
/**
 * Home Options
 * 
 * @author Fox
 */
/* Start Dummy Data*/
$msg = $disabled = '';
if (!class_exists('WPBakeryVisualComposerAbstract') or !class_exists('CmssuperheroesCore') or !function_exists('cptui_create_custom_post_types')){
    $disabled = ' disabled ';
    $msg='You should be install visual composer, Cmssuperheroes and Custom Post Type UI plugins to import data.';
}
/* End Dummy Data*/



//Redux Framework Version
function getReduxVersion(){
	$redux_version = ReduxFramework::$_version;
	return $redux_version;
}

/*@import options*/
$arr_tabs = array(	
					'general',
					'woocommerce',
					'blog',
					'social',
					//'header',
					//'page-title',
					//'body',
					//'buttons',
					//'effect',
					//'footer',
					//'team',
					//'custom-css',
					//'animations',
					//'optimal-core',
					//'styling',
					'a-sample',
				);

if( empty($arr_tabs) ){
	$arr_tabs = array('a-sample');
}

if($arr_tabs){
	foreach($arr_tabs as $tab){
		$file_path_tab = TABS_PATH .'/'.$tab.'/tab.php'; 
		if( file_exists( $file_path_tab ) ):
			require($file_path_tab);
		endif;
	}
}
/*@import options end*/