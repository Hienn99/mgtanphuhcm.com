<?php
add_action( 'init', 'shortcode_init' );
function shortcode_init(){
	$arr_shortcode = array('w336_facebook_box',
							//'add_new_shortcode_here'
						);
	
	if($arr_shortcode):
		foreach($arr_shortcode as $file_name){
			$file_shortcode = realpath(dirname(__FILE__)).'/'.$file_name.'.php';
			if( file_exists($file_shortcode) ){
				require realpath(dirname(__FILE__)).'/'.$file_name.'.php';
				add_shortcode( $file_name, $file_name.'_func' );
			}
		}
	endif;	//end if arr_shortcode
}

function get_template_part_return($template_name, $part_name=null) {
	ob_start();
		get_template_part($template_name, $part_name);
		$var = ob_get_contents();
	ob_end_clean();
	return $var;
}
