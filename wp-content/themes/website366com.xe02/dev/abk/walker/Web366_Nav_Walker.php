<?php


class Web366_Nav_Walker extends Walker_Nav_Menu {
 
 /**
 * <ul id="container-all-child" class="sub-menu">
 **/
 public function start_lvl( &$output, $depth = 0, $args = array() )
 {
	$output .= '<ul>';
 }
 
 /**
 * </ul> // #container-all-child
 **/
 public function end_lvl( &$output, $depth = 0, $args = array() )
 {
		$output .= '</ul>';
 }
 
 /**
 * <li id="menu-item-5">
 * 
 **/
 public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )
 {
	/* $item = object(WP_Post)#3848 (40) { 
					["ID"]=> int(8) 
					["post_author"]=> string(1) "1" 
					["post_date"]=> string(19) "2017-03-26 11:09:20" 
					["post_date_gmt"]=> string(19) "2017-03-26 11:09:20" 
					["post_content"]=> string(0) "" 
					["post_title"]=> string(11) "Trang chủ" 
					["post_excerpt"]=> string(0) "" 
					["post_status"]=> string(7) "publish" 
					["comment_status"]=> string(6) "closed" 
					["ping_status"]=> string(6) "closed" 
					["post_password"]=> string(0) "" 
					["post_name"]=> string(9) "trang-chu" 
					["to_ping"]=> string(0) "" 
					["pinged"]=> string(0) "" 
					["post_modified"]=> string(19) "2017-03-26 11:09:48" 
					["post_modified_gmt"]=> string(19) "2017-03-26 11:09:48" 
					["post_content_filtered"]=> string(0) "" 
					["post_parent"]=> int(0) 
					["guid"]=> string(35) "http://wp.happyhousesaigon.com/?p=8" 
					["menu_order"]=> int(1) 
					["post_type"]=> string(13) "nav_menu_item" 
					["post_mime_type"]=> string(0) "" 
					["comment_count"]=> string(1) "0" 
					["filter"]=> string(3) "raw" 
					["db_id"]=> int(8) 
					["menu_item_parent"]=> string(1) "0" 
					["object_id"]=> string(1) "8" 
					["object"]=> string(6) "custom" 
					["type"]=> string(6) "custom" 
					["type_label"]=> string(16) "Link tùy chọn" 
					["title"]=> string(11) "Trang chủ" 
					["url"]=> string(1) "#" 
					["target"]=> string(0) "" 
					["attr_title"]=> string(0) "" 
					["description"]=> string(0) "" 
					["classes"]=> array(4) { [0]=> string(0) "" [1]=> string(9) "menu-item" [2]=> string(21) "menu-item-type-custom" [3]=> string(23) "menu-item-object-custom" } 
					["xfn"]=> string(0) "" 
					["current"]=> bool(false) 
					["current_item_ancestor"]=> bool(false) 
					["current_item_parent"]=> bool(false) 
				}
	*/
	
	//ID <li>
	$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
	$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

	$classes = empty( $item->classes ) ? array() : (array) $item->classes;
	$classes[] = 'menu-item-' . $item->ID;
	if($item->current_item_ancestor || $item->current) $classes[] = 'current-menu-ancestor';
	if($item->current_item_parent) $classes[] = 'current-menu-parent';
	
	$obj_walker = $args->walker;
	$has_child = $obj_walker->has_children;
	if($has_child){
		$classes[] = 'has-sub'; 
	}
	
	
	//Get attr class for <li>
	$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
	$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
	
	//<a></a>
	$attributes = '';
	$atts = array();
	$atts['title'] = ! empty( $item->attr_title ) ? $item->attr_title : '';
	$atts['target'] = ! empty( $item->target ) ? $item->target : '';
	$atts['rel'] = ! empty( $item->xfn ) ? $item->xfn : '';
	$atts['href'] = ! empty( $item->url ) ? $item->url : '';
	if($atts){
		foreach($atts as $att_k=>$att_v){
			if($att_v)
			$attributes .= $att_k .'="'.$att_v.'"';
		}
	}
	
	
	
	$item_output = $args->before;
	$item_output .= '<div class="cha">
						<div class="hinhmenu">
							<a '. $attributes .' class="'.$item->classes[0].'">';
	
	$item_output .= $args->link_before . apply_filters( 'the_title', '', $item->ID ) . $args->link_after;
	
	
	$item_output .= '		</a>
						</div>
						<div class="con">'.$item->title.'</div>
					</div>';
	if($has_child){
		$item_output .= '';
	}
	$item_output .= $args->after;
	
	//output <li>
 	$output .= '<li '.$id.' '.$class_names.' >';
		$output .= $item_output;

 }
 
 /**
 * </li>
 **/
 public function end_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )
 {
 	$output .= '</li>';
 }
} // end Web366_Nav_Walker