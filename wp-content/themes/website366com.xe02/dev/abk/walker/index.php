<?php
$arr_nav_walker = array(	
	'Web366_Nav_Walker',
	//'New Walker here....'
);

if($arr_nav_walker):
	foreach($arr_nav_walker as $file):
		$css_nav_walker = REGISTER_NAV_WALKER_DIR . '/'.$file.'.php';
		if( file_exists($css_nav_walker) ){
			require_once ( $css_nav_walker );
		}
	endforeach;
endif;