<?php
require_once (dirname(__FILE__) . '/inc.posttype.php');

class PosttypeCustom{
	
	var $wp_theme;
	var $name;
	var $columns;
	var $a_post_type;
	
	public function __construct() {
		$this->a_post_type = $this->getArrPostype();
		$this->columns;
		$this->wp_theme =  wp_get_theme();
		$this->name = $this->wp_theme->get( 'Name' );
		$this->init();
	}
	
	public function setPosttype($psttype){
		$this->posttype = $psttype;
	}
	
	public function getPosttype(){
		return $this->posttype;
	}
	
	public function getArrPostype(){
		global $arr_post_type;
		return $arr_post_type;
	}
	
	
	public function init(){
		add_action('admin_init', 'flush_rewrite_rules');
		add_action('init', array($this, 'registerPosttype'));
	}
	
	//Register Post Type
	public function registerPosttype(){
		$arr_post_type = $this->getArrPostype();
		if($arr_post_type):
			foreach($arr_post_type as $posttype => $val_postype):
				//Register Posttype
				$args = $val_postype['post-type'];
				if($posttype)
				register_post_type( $posttype, $args);
				
				//Register Tax
				$taxonomy = $val_postype['taxonomy'];
				$taxonomy_type = $taxonomy[0];
				$taxonomy_args = $taxonomy[1];
				if($taxonomy_type)
				register_taxonomy($taxonomy_type, $posttype, $taxonomy_args	);
				
				//Add Columns
				$columns = $val_postype['columns'];
				$newCustomPosttype = new self();
				if($columns){
					// Set Columns
					$newCustomPosttype->columns = $columns;
					$newCustomPosttype->setPosttype($posttype);
					
					//add columns
					add_filter('manage_'.$posttype.'_posts_columns', array($newCustomPosttype, 'getColumns'));
					
					// get content column
					add_action('manage_'.$posttype.'_posts_custom_column', array($newCustomPosttype, 'getColumnsContent'), 10, 2);
				}
				
				//Columns Sort (Only sort with Integer value)
				$sort_columns = $val_postype['sort_columns'];
				if($sort_columns && $newCustomPosttype){
					$newCustomPosttype->sort_columns = $sort_columns;
					add_filter( 'manage_edit-'.$posttype.'_sortable_columns', array($newCustomPosttype, 'sortColumns') );
				}
			endforeach;
		endif;
	}
	
	public function sortColumns() {
		$sort_columns = $this->sort_columns;
		if($sort_columns){
			foreach($sort_columns as $column) {
				$columns[$column] = $column;
			}
		}
		return $columns;
	}
	
	public function getColumns() {
		$columns = $this->columns;
		$columns = array('cb' => '<input type="checkbox" />') + $columns;
		$new = $columns;
		if(array_key_exists("featured_image", $columns)):
			$new = array();
			foreach($columns as $key => $title) {
				if ($key=='title')
					$new['featured_image'] = 'Featured Image';
				
				$new[$key] = $title;
			}
		endif;
		return $new;
	}
	
	public function getColumnsContent($column_name, $post_ID) {
		$arrPosttype = $this->a_post_type;
		$posttype = $this->posttype;
		$arr_var =$arrPosttype[$posttype]; 
		$taxonomy = $arr_var['taxonomy'];
		$taxonomy_type = $taxonomy[0];
		
		$s_content_col = '';
		switch ($column_name) {
			// Custom------------- http://justintadlock.com/archives/2011/06/27/custom-columns-for-custom-post-types
			case 'email':
				$s_content_col =  get_post_meta($post_ID, $column_name, true );
				echo $s_content_col;
			break;
			case 'total_amount':
				$s_content_col =  get_post_meta($post_ID, $column_name, true );
				echo $s_content_col;
			break;
			case 'payment_status':
				$s_content_col =  get_post_meta($post_ID, $column_name, true );
				if($s_content_col=="") $s_content_col = 'Pending';
				echo $s_content_col;
			break;
			case 'status':
				$s_content_col =  get_post_meta($post_ID, $column_name, true );
				if($s_content_col=="") $s_content_col = 'Received';
				echo $s_content_col;
			break;
			// End Custom-------------
			
			// LIB
			case 'featured_image':
				$post_featured_image = $this->get_featured_image($post_ID);
				if ($post_featured_image) {
					$s_content_col = '<img width="50" src="' . $post_featured_image . '" />';
				}else{
					$s_content_col = 'No IMG';
				}
				echo $s_content_col;
			break;
				
			case $taxonomy_type:
				$postterms = get_the_terms($post_ID, $taxonomy_type);
				if($postterms){
					$termlists = array();
					foreach($postterms as $postterm){
						$termlists[] = '<a href="'.admin_url('edit.php?'.$taxonomy_type.'='.$postterm->slug.'&post_type='.$posttype).'">'.$postterm->name.'</a>';
					}
					if(count($termlists)>0){
						$s_content_col = implode(", ",$termlists);
					}
				}
				echo $s_content_col;
			break;
				
			default:
				break;
		}
	}
	
	public function get_featured_image($post_ID) {
		$post_thumbnail_id = get_post_thumbnail_id($post_ID);
		if ($post_thumbnail_id) {
			$post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail');
			return $post_thumbnail_img[0];
		}
	}
	
	
	
//End Class
}

$objPosttypeCustom = new PosttypeCustom();