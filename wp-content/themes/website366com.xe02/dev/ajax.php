<?php
/*Ajax Function*/

//------------------------------------initVsgoback--------------------------------------------
function initVsgobackFunc(){
	header("Content-Type: application/json", true);
	
	$current_step = getCurrentStep();
	$prev_step = $current_step - 1;
	if($prev_step){
		setCurrentStep($prev_step);
	}
				
	//_RESPONSE
	$response = json_encode(array	(
										'prev_step' => getCurrentStep(),
									)
							);
	echo $response; 
	die();
}
add_action( 'wp_ajax_initVsgoback', 'initVsgobackFunc' );
add_action('wp_ajax_nopriv_initVsgoback', 'initVsgobackFunc');
