<div class="news" itemscope="" itemtype="http://schema.org/Article">
  <div class="img" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
      <a href="<?php the_permalink();?>">
        <?php the_post_thumbnail('full');?>
      </a>
      <div class="date"><?php echo get_the_date('d/m/Y');?></div>
  </div>
  <div class="caption">
      <div class="tend">
          <h3 itemprop="headline"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
      </div>
      <div class="des" itemprop="description"><?php echo wp_trim_words(get_the_content(), 55, '...' );?></div>
  </div>
</div>