<?php 
	$w366_acc_price = get_post_meta(get_the_ID(), 'w366_acc_price', true); 
	if($w366_acc_price){
		 $w366_acc_price = w366_format_money($w366_acc_price, true);
	}else{
		$w366_acc_price = 'Liên hệ';
	}
?>
<div class="phukien">
    <div class="img">
        <a href="<?=get_the_post_thumbnail_url(get_the_ID(), 'full')?>" ref="nofollow" class="fancybox" data-fancybox-group="new-gr">
            <?php the_post_thumbnail('full');?>
        </a>
    </div>
	<div class="tend"><h3><a href="<?=get_the_post_thumbnail_url(get_the_ID(), 'full')?>" ref="nofollow"><?php the_title();?></a></h3></div>
	<div class="price"><?=$w366_acc_price?></div>
	<div class="des"></div>
</div>