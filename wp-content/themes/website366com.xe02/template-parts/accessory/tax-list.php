<?php 
$id_tax = 0;
$current_category = get_queried_object();
if($current_category->taxonomy){
	$id_tax = $current_category->term_id;
}

$arr_accessory_tax =  w366_all_accessory_tax();
if($arr_accessory_tax){
	$accessory_html = '';
	foreach($arr_accessory_tax as $obj){
		$cls = '';
		if($obj->term_id==$id_tax){
			$cls = 'active';
		}
		$accessory_html .= '<li class="'.$cls.'"><a href="'.get_term_link($obj, $obj->taxonomy ).'">'.$obj->name.'</a></li>';
	}
?>
	<div class="menuPhukien noscript">
		<div class="mc-menu hidden-lg hidden-md">phụ kiện</div>
			<ul><?=$accessory_html?></ul>
	</div>
<?php
}
?>