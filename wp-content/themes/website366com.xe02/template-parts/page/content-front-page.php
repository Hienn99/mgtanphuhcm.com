<article id="post-<?php the_ID(); ?>" <?php post_class( 'twentyseventeen-panel ' ); ?> >

	<?php 
	$slider_id = w366_get_option('setting_hp_slider');
	if(!$slider_id) $slider_id = 48;
	$arr_img_id = get_post_meta($slider_id, '_vdw_gallery_id', true);
	if($arr_img_id): 
	?>
	<!--Slider-->
    <div class="vnt-slide-home">
    	<div id="vnt-slide-home">
        	<?php 
			foreach( $arr_img_id as $k=>$image_id ){
				$arr_img_big = wp_get_attachment_image_src($image_id, 'full' );
				$img_full_url = $arr_img_big[0];
			?>
                <div class="img">
                	<a href="#" target="_blank" tabindex="-1">
                    	<img src="<?php echo $img_full_url; ?>" alt="">
                    </a>
                </div>
            <?php
			}
			?>
        </div>
    </div>
    <?php 
	endif;
	?>
    
    <!-- Dynamic sidebar Home -->
    <div id="website366com-dynmic-sb-home">
    	<?php
		if ( is_active_sidebar( 'sidebar_home' ) ) {
			dynamic_sidebar( 'sidebar_home' );
		}
		?>
    </div>

</article><!-- #post-## -->
