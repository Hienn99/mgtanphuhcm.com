<div class="container">
    <div class="wrapper_new">
        <div class="box_mid">
            <div class="mid-title ">
                <div class="titleL">
                    <h1><?php the_title(); ?></h1>
                </div>
                <div class="titleR"></div>
            </div>
            <div class="mid-content">
            	<div class="the-content desc">
                	<?php the_content(); ?>
                </div>
            </div>          
        </div>
    </div>
</div>