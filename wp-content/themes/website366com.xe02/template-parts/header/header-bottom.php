<?php 
	$img_url = get_theme_file_uri( '/dev/enqueue/images/header-bg.jpg' );
	$title = get_the_title();
?>
<div class="vnt-main-top">
    <div id="vnt-slide" slick-init="" class="slick-initialized slick-slider">
        <div class="slick-list draggable">
            <div class="slick-track">
                <div class="item slick-slide slick-current slick-active">
                	<img src="<?=$img_url?>" alt="<?=$title?>">
                </div>
            </div>
        </div>
    </div>
  
    <div id="vnt-navation" class="breadcrumb">
        <div class="wrapper">
            <div class="navation">
            	<?php 
					if ( function_exists('yoast_breadcrumb') ) {
					 	yoast_breadcrumb( '<ul id="breadcrumbs">','</ul>' );
					}
				?>
            	<div class="clear"></div>
            </div>
        </div>
    </div>
</div>
