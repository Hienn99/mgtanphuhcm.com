<div class="headerMain">

	<div class="wrapper">

    	<div class="logo">

            <h1>

            	<a href="<?php echo get_site_url();?>" target="_self">

            		<img src="<?php echo w366_get_full_src('stt_logo');?>" alt="<?php bloginfo('name');?>" class="png">

               	</a>

        	</h1>

        </div>

        <div class="header-tools">

        	<div class="menuTop hidden-sm hidden-xs">

            	<?php 

				wp_nav_menu( array(

					'theme_location' => 'top',

					'container' => false

				) );

				?>

            </div>

            

            <div class="searchTop hidden-sm hidden-xs">

            	<div class="icon"><i class="fa fa-search"></i></div>

            </div>

        </div>

        <a id="show-menu-main" class="visible-xs visible-sm" href="#"><i class="fa fa-bars"></i><i class="fa fa-times" aria-hidden="true"></i></a>

        <script>

		jQuery(document).ready(function() {

			jQuery("#show-menu-main").click(function(){

				if( jQuery("body").hasClass("js-menu-active") ){

					jQuery("body").removeClass("js-menu-active");

				}else{

					jQuery("body").addClass("js-menu-active");

				}

				

				return false;

			});

		});

	</script>

    </div>

</div>

<!---.headerMain--->



<!-- <div id="website366-menu-second" class="wrapper hidden-sm hidden-xs">

	<div class="headerTools">

    	<div class="socialTop">

            <ul>

                <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>

                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>

                <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>

                <li><a href="#"><i class="fa fa-twitter"></i></a></li>

            </ul>

        </div>

        <div class="right">

            <div class="menuTools">

            <?php 

				wp_nav_menu( array(

					'theme_location' => '2nd',

					'container' => false

				) );

				?>

            </div>

            <div class="hotlineTop"><?php echo w366_get_option('stt_hotline');?></div>

        </div>

    </div>

</div> -->

<!-- #website366-menu-second -->

