<?php $product_id = get_the_ID(); ?>
<div id="product-single-slider">
    <div class="container">
        <div class="row">
            <div id="product-price" class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <?php $w366_car_price = get_post_meta($product_id, 'w366_car_price', true); ?>
                <p class="product-price"><?php echo w366_format_money($w366_car_price, true); ?></p>
                <div class="product-content desc"><?php the_field('car_short_desc'); ?></div>
            </div>
            <div id="product-slider-content" class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                <?php 
                    $arr_img_id = get_post_meta($product_id, '_vdw_gallery_id', true);
                    if($arr_img_id): 
                ?>
                    <!--Product Slider-->
                    <div id="product-slider">
                        <?php 
                        foreach( $arr_img_id as $k=>$image_id ){
                            $arr_img_big = wp_get_attachment_image_src($image_id, 'full' );
                            $img_full_url = $arr_img_big[0];
                        ?>
                            <div class="img">
                                <a href="#" target="_blank" tabindex="-1">
                                    <img src="<?php echo $img_full_url; ?>" alt="">
                                </a>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <script>
                        jQuery(document).ready(function() {
                            jQuery("#product-slider").slick({
                                autoplay:true,
                                arrows:false,
                                dots:true,
                            });
                        });
                    </script>
                <?php 
                endif;
                ?>
            </div>
        </div>
    </div>
</div>