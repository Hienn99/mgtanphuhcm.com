<?php $product_id = get_the_ID(); ?>
<div id="product-furniture" class="product-container">
    <div class="row">
    	<div id="product-furniture-left" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <?php 
                $str_attachment_ids = get_field('car_noi_gallery');
                $arr_attachment_ids = explode(",", $str_attachment_ids);
                $first_attachment_id = $arr_attachment_ids[0];
                $arr_attachment_id = wp_get_attachment_image_src($first_attachment_id, 'thumb_600x400' );
                $first_attachment_url = $arr_attachment_id[0];
            ?>
            <div class="img">
                <a href="<?php echo $first_attachment_url; ?>" class="fancybox" data-fancybox-group="product-furniture-<?=$product_id?>">
                    <img src="<?php echo $first_attachment_url; ?>" alt="<?php the_title(); ?>">
                </a>
            </div>
        </div>
        <div id="product-furniture-right" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="mid-title ">
                <div class="titleL ttl">
                    <h2>Nội thất</h2>
                </div>
                <div class="titleR"></div>
            </div>
            <div class="content js-content-scrollbar"><?php the_field('car_noi_that'); ?></div>
        </div>
    </div>
    <div id="product-exterior-gallery" class="row">
        <?php 
        if($arr_attachment_ids){
            foreach( $arr_attachment_ids as $k=>$image_id ){
                 $arr_img_big = wp_get_attachment_image_src($image_id, 'full' );
                 $img_full_url = $arr_img_big[0];
                 
                 $arr_img_thumb = wp_get_attachment_image_src($image_id, 'thumb_360x260' );
                 $img_thumb_url = $arr_img_thumb[0];
            ?>
             <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="img">
                    <a href="<?php echo $img_full_url; ?>" class="fancybox" data-fancybox-group="product-furniture-<?=$product_id?>">
                        <img src="<?php echo $img_thumb_url; ?>" alt="<?php the_title(); ?> ">
                    </a>
                </div>
             </div>
            <?php
            }
        }
        ?>
    </div>
</div>
<script>
	jQuery(document).ready(function() {
		jQuery(".js-content-scrollbar").mCustomScrollbar({
			scrollButtons:{enable:true},
			theme : 'dark-2'
		});
	});
</script>