<?php $w366_car_price = get_post_meta(get_the_ID(), 'w366_car_price', true); ?>
<div class="car product-content-loop product-hover-content">
    <a href="<?php the_permalink();?>"><?php the_post_thumbnail('full');?></a>
    <a href="<?php the_permalink();?>"><h3><?php the_title(); ?></h3></a>
    <div class="price">Giá: <?php echo w366_format_money($w366_car_price, true); ?><a class="js-put-title-form fr fancybox" data-title="<?php the_title(); ?>" href="#form_drive_try">Lái thử</a> <a class="fr" href="<?php the_permalink();?>">Chi tiết</a></div>
</div>