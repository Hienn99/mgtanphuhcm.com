<?php 
//Get $obj_car_term from template
if($obj_car_term){
	$term_tax = $obj_car_term->taxonomy;
	$term_id = $obj_car_term->term_id;
	$term_link = get_term_link($obj_car_term, $term_tax);
	$arr_image = get_field('hinh', $obj_car_term); 
	$url_img = $arr_image['url'];
	
	//Query car from term
	$args_q = array(  	'post_type' => 'w366_car',
						'posts_per_page' =>  -1 ,
						'order'     => 'desc',
						'tax_query' => array(
											array(
												'taxonomy'  => $term_tax,
												'field'     => 'id', 
												'terms'     => $term_id
											)
										)
	);
	$the_query = new WP_Query( $args_q );
?>
    <div id="car_tax_<?=$term_id?>" class="car product-tax-loop product-hover-content">
        <a href="<?=$term_link;?>">
            <img src="<?=$url_img;?>" alt="<?=$obj_car_term->name ?>" />
        </a>
        <a href="<?=$term_link;?>"><h3 class="tt"><?=$obj_car_term->name ?></h3></a>
        <?php 
		if ( $the_query->have_posts() ) {
		?>
        <!--Product hover-->
        <div class="car-of-cat">
        	<?php 
			while ( $the_query->have_posts() ) { $the_query->the_post();
				$w366_car_price = get_post_meta(get_the_ID(), 'w366_car_price', true);
			?>
				<div class="car-item">
					<div class="car-name"><a href="<?php the_permalink();?>"><?php the_title(); ?></a></div>
					<div class="car-price"><a href="<?php the_permalink();?>"><?php echo w366_format_money($w366_car_price, true); ?></a></div>
				</div>
			<?php
			}
			wp_reset_postdata();
			?>
        </div>
        <!--Product hover-->
        <?php
		}
		?>
    </div>
<?php
}