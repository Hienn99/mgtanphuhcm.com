<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="the-title"><h1><?php the_title(); ?></h1></div>
    <div class="the-date"><span><?php the_date(); ?></span></div>

	<div class="the-content desc">
		<?php
		/* translators: %s: Name of current post */
		the_content( sprintf(
			__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'website366com' ),
			get_the_title()
		) );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
