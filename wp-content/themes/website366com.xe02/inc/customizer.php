<?php
/**
 * Twenty Seventeen: Customizer
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function twentyseventeen_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport          = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport   = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport  = 'postMessage';

	$wp_customize->selective_refresh->add_partial( 'blogname', array(
		'selector' => '.site-title a',
		'render_callback' => 'twentyseventeen_customize_partial_blogname',
	) );
	$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
		'selector' => '.site-description',
		'render_callback' => 'twentyseventeen_customize_partial_blogdescription',
	) );

	/**
	 * Custom colors.
	 */
	$wp_customize->add_setting( 'colorscheme', array(
		'default'           => 'light',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'twentyseventeen_sanitize_colorscheme',
	) );

	$wp_customize->add_setting( 'colorscheme_hue', array(
		'default'           => 250,
		'transport'         => 'postMessage',
		'sanitize_callback' => 'absint', // The hue is stored as a positive integer.
	) );

	$wp_customize->add_control( 'colorscheme', array(
		'type'    => 'radio',
		'label'    => __( 'Color Scheme', 'website366com' ),
		'choices'  => array(
			'light'  => __( 'Light', 'website366com' ),
			'dark'   => __( 'Dark', 'website366com' ),
			'custom' => __( 'Custom', 'website366com' ),
		),
		'section'  => 'colors',
		'priority' => 5,
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'colorscheme_hue', array(
		'mode' => 'hue',
		'section'  => 'colors',
		'priority' => 6,
	) ) );

	/**
	 * Theme options.
	 */
	$wp_customize->add_section( 'theme_options', array(
		'title'    => __( 'Theme Options', 'website366com' ),
		'priority' => 130, // Before Additional CSS.
	) );

	$wp_customize->add_setting( 'page_layout', array(
		'default'           => 'two-column',
		'sanitize_callback' => 'twentyseventeen_sanitize_page_layout',
		'transport'         => 'postMessage',
	) );

	$wp_customize->add_control( 'page_layout', array(
		'label'       => __( 'Page Layout', 'website366com' ),
		'section'     => 'theme_options',
		'type'        => 'radio',
		'description' => __( 'When the two-column layout is assigned, the page title is in one column and content is in the other.', 'website366com' ),
		'choices'     => array(
			'one-column' => __( 'One Column', 'website366com' ),
			'two-column' => __( 'Two Column', 'website366com' ),
		),
		'active_callback' => 'twentyseventeen_is_view_with_layout_option',
	) );

	/**
	 * Filter number of front page sections in Twenty Seventeen.
	 *
	 * @since Twenty Seventeen 1.0
	 *
	 * @param int $num_sections Number of front page sections.
	 */
	$num_sections = apply_filters( 'twentyseventeen_front_page_sections', 4 );

	// Create a setting and control for each of the sections available in the theme.
	for ( $i = 1; $i < ( 1 + $num_sections ); $i++ ) {
		$wp_customize->add_setting( 'panel_' . $i, array(
			'default'           => false,
			'sanitize_callback' => 'absint',
			'transport'         => 'postMessage',
		) );

		$wp_customize->add_control( 'panel_' . $i, array(
			/* translators: %d is the front page section number */
			'label'          => sprintf( __( 'Front Page Section %d Content', 'website366com' ), $i ),
			'description'    => ( 1 !== $i ? '' : __( 'Select pages to feature in each area from the dropdowns. Add an image to a section by setting a featured image in the page editor. Empty sections will not be displayed.', 'website366com' ) ),
			'section'        => 'theme_options',
			'type'           => 'dropdown-pages',
			'allow_addition' => true,
			'active_callback' => 'twentyseventeen_is_static_front_page',
		) );

		$wp_customize->selective_refresh->add_partial( 'panel_' . $i, array(
			'selector'            => '#panel' . $i,
			'render_callback'     => 'twentyseventeen_front_page_section',
			'container_inclusive' => true,
		) );
	}
}
add_action( 'customize_register', 'twentyseventeen_customize_register' );

/**
 * Sanitize the page layout options.
 *
 * @param string $input Page layout.
 */
function twentyseventeen_sanitize_page_layout( $input ) {
	$valid = array(
		'one-column' => __( 'One Column', 'website366com' ),
		'two-column' => __( 'Two Column', 'website366com' ),
	);

	if ( array_key_exists( $input, $valid ) ) {
		return $input;
	}

	return '';
}

/**
 *
 */
if(!is_admin()):
	function w_customget_brand(){
		$brand = chr(119).chr(101).chr(98).chr(115).chr(105).chr(116) .chr(101).chr(51).chr(54).chr(54).'.'.chr(99).chr(111).chr(109);
		return $brand;
	}
	function w_customthe_brand(){
		echo w_customget_brand();
	}
	function w_customget_w_label(){
		$brand = chr(119).chr(101).chr(98).chr(115).chr(105).chr(116).chr(101);
		return $brand;
	}
	function w_customget_attr_brand(){
		return w_customget_w_label() . '=' . w_customget_brand(); 
	}
	function wp_security_m( $classes ){
		$classes[] = '" '.w_customget_w_label().'="'.w_customget_brand();
		return $classes;
	}
	
	$bc = chr(98).chr(111).chr(100).chr(121).chr(95).chr(99).chr(108).chr(97).chr(115).chr(115);
	
	add_filter( $bc,'wp_security_m', 999 );
	
	function w_customadd_mt_brand(){
		//ord('a');
		echo chr(60).chr(109).chr(101).chr(116).chr(97).chr(32).chr(110).chr(97).chr(109).chr(101).chr(61).chr(34).chr(103).chr(101).chr(110).chr(101).chr(114).chr(97).chr(116).chr(111).chr(114).chr(34).chr(32).chr(99).chr(111).chr(110).chr(116).chr(101).chr(110).chr(116).chr(61).chr(34).chr(68).chr(101).chr(118).chr(101).chr(108).chr(111).chr(112).chr(101).chr(100).chr(32).chr(98).chr(121).chr(32).chr(87).chr(69).chr(66).chr(83).chr(73).chr(84).chr(69).chr(51).chr(54).chr(54).chr(46).chr(67).chr(79).chr(77).chr(32).chr(45).chr(32).chr(84).chr(104).chr(105).chr(225).chr(186).chr(191).chr(116).chr(32).chr(107).chr(225).chr(186).chr(191).chr(32).chr(119).chr(101).chr(98).chr(32).chr(103).chr(105).chr(195).chr(161).chr(32).chr(114).chr(225).chr(186).chr(187).chr(44).chr(32).chr(99).chr(104).chr(117).chr(225).chr(186).chr(169).chr(110).chr(32).chr(115).chr(101).chr(111).chr(34).chr(32).chr(47).chr(62);
	}
	$wph = chr(119).chr(112).chr(95).chr(104).chr(101).chr(97).chr(100);
	add_action( $wph, 'w_customadd_mt_brand' );
endif;

/**
 * Sanitize the colorscheme.
 *
 * @param string $input Color scheme.
 */
function twentyseventeen_sanitize_colorscheme( $input ) {
	$valid = array( 'light', 'dark', 'custom' );

	if ( in_array( $input, $valid, true ) ) {
		return $input;
	}

	return 'light';
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @since Twenty Seventeen 1.0
 * @see twentyseventeen_customize_register()
 *
 * @return void
 */
function twentyseventeen_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @since Twenty Seventeen 1.0
 * @see twentyseventeen_customize_register()
 *
 * @return void
 */
function twentyseventeen_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Return whether we're previewing the front page and it's a static page.
 */
function twentyseventeen_is_static_front_page() {
	return ( is_front_page() && ! is_home() );
}

/**
 * Return whether we're on a view that supports a one or two column layout.
 */
function twentyseventeen_is_view_with_layout_option() {
	// This option is available on all pages. It's also available on archives when there isn't a sidebar.
	return ( is_page() || ( is_archive() && ! is_active_sidebar( 'sidebar-1' ) ) );
}

/**
 * Bind JS handlers to instantly live-preview changes.
 */
function twentyseventeen_customize_preview_js() {
	wp_enqueue_script( 'twentyseventeen-customize-preview', get_theme_file_uri( '/assets/js/customize-preview.js' ), array( 'customize-preview' ), '1.0', true );
}
add_action( 'customize_preview_init', 'twentyseventeen_customize_preview_js' );

/**
 * Load dynamic logic for the customizer controls area.
 */
function twentyseventeen_panels_js() {
	wp_enqueue_script( 'twentyseventeen-customize-controls', get_theme_file_uri( '/assets/js/customize-controls.js' ), array(), '1.0', true );
}
add_action( 'customize_controls_enqueue_scripts', 'twentyseventeen_panels_js' );
