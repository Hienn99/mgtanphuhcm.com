        </div><!-- #vnt-content -->	
        <div id="vnt-footer">
        	<footer>
			<div class="container">
				<div class="row">
                	<?php
						if ( is_active_sidebar( 'sidebar_footer' ) ) {
							dynamic_sidebar( 'sidebar_footer' );
						}
						?>
				</div>
				<div class="copyright">
                	<a rel="nofollow" href="<?php echo get_site_url();?>"><?php echo w366_get_option('ft_cr_lelf');?></a> <span class="right"><?php echo w366_get_option('ft_cr_right');?></span>
                </div>
			</div>
		</footer>
        </div><!-- #vnt-footer-->
    </div><!-- #vnt-container -->
</div><!-- #vnt-wrapper -->
<?php wp_footer(); ?>

<!-- Form lái thử -->
<?php 
$try_cf7_id = w366_get_option('setting_try_cf7_id');
if($try_cf7_id){
?>
<a style="display:none;" id="show-form-cft7" class="js-put-title-form fr fancybox" data-title="Lái thử xe" href="#form_drive_try">Lái thử</a>

<div id="form_drive_try" style="display:none;">
	<h2 id="h-title"></h2>
	<?php echo do_shortcode('[contact-form-7 id="'.$try_cf7_id.'" title="Lái thử"]'); ?>
</div>
<script>
	jQuery(document).ready(function() {
		jQuery(".menu .js-put-title-form a").click(function(){
			jQuery("#show-form-cft7").click();
			return false;
		});
		
		jQuery("a.js-put-title-form").click(function(){
			var title = jQuery(this).data('title'); //alert(title);
			jQuery("#form_drive_try input[name='your-car']").val(title);
			jQuery("#h-title").html(title);
		});
	});
</script>
<?php 
}
?>
</body>
</html>
