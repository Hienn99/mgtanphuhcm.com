<?php
get_header(); ?>

<div class="wrap">
	<?php
		get_template_part( 'template-parts/header/header', 'bottom' );
	?>
    
    <div class="container">
        <div class="wrapper_new">
            <div class="box_mid">
                <div class="mid-title ">
                    <div class="titleL">
                        <h1><?php single_term_title(); ?></h1>
                    </div>
                    <div class="titleR"></div>
                </div>
                <div class="mid-content">
                    <div class="the-content-bk">
                         <div class="row">
                            <?php 
							while ( have_posts() ) : the_post();
							?>
								<div class="col-md-4">
									<?php 
										get_template_part( 'template-parts/product/content', 'loop' );
									?>
								</div>
							 <?php
							endwhile;
                         	?>
                         </div>
                         <div class="w366-pagination row">
                            <div class="col-md-12">
                                <?php 
                                the_posts_pagination( array(
                                        'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'website366com' ) . '</span>',
                                        'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'website366com' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
                                        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'website366com' ) . ' </span>',
                                    ) );

                                ?>
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
        </div>
    </div>
</div>


<?php get_footer();
