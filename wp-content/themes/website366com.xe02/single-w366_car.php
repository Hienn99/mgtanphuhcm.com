<?php
get_header(); ?>

<div class="wrap">
	<?php get_template_part( 'template-parts/header/header', 'bottom' ); ?>
    <div class="container-bk">
        <div class="wrapper_new">
			<?php
                /* Start the Loop */
                while ( have_posts() ) : the_post();
            ?>
                	<div class="box_mid">
                        <div class="mid-title ">
                            <div class="titleL">
                                <h1><?php the_title(); ?></h1>
                            </div>
                            <div class="titleR"></div>
                        </div>
                        <div class="mid-content">
                        	<!-- Price - Silder-->
                            <?php get_template_part( 'template-parts/product/single', 'slider' ); ?>
                            
                            <!-- Ngoại thất-->
                            <?php get_template_part( 'template-parts/product/single', 'exterior' ); ?>
                            
                            <!-- Nội thất-->
                            <?php get_template_part( 'template-parts/product/single', 'furniture' ); ?>
                            
                            <!-- Chi tiết -->
                            <div id="product-detail">
                            	<div class="container">
                            		<?php the_content(); ?>
                                </div>
                            </div>
                            
                        </div>          
                    </div>
            <?php
                endwhile; // End of the loop.
            ?>
        </div>
    </div>
</div><!-- .wrap -->

<?php get_footer();
