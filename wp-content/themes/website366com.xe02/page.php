<?php
get_header(); ?>

<div class="wrap">
	<?php
		get_template_part( 'template-parts/header/header', 'bottom' );
		
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/page/content', 'page' );
		endwhile; // End of the loop.
	?>
</div><!-- .wrap -->

<?php get_footer();
