<?php
get_header(); ?>

<div class="wrap">
	<?php get_template_part( 'template-parts/header/header', 'bottom' ); ?>
    <div class="container">
        <div class="wrapper_new">
            <div class="mod-content row">
            	<div id="vnt-main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                	<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/post/content', get_post_format() );
						endwhile; // End of the loop.
					?>
                </div>
                <div id="vnt-msidebar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 hidden-sm hidden-xs">
                	<?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </div>
</div><!-- .wrap -->

<?php get_footer();
